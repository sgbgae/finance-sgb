<?php
class Connect {
	protected static $_connection;
	public static function getConnection($db_name){
		if(!self::$_connection){
			$dbhost = ':/cloudsql/database-sgb:us-central1:database-sgb';
			$dbuser = 'root';
			$dbpassword = 'pass@word1';
			
			$dbname = $db_name;
			self::$_connection = @mysql_connect($dbhost, $dbuser, $dbpassword);
			if(!self::$_connection){
				throw new Exception('Gagal melalukan koneksi ke database. '.mysql_error());
			}
			$result = @mysql_select_db($dbname, self::$_connection);
			if(!$result){
				throw new Exception('Koneksi gagal: '.mysql_error());
			}
		}
		return self::$_connection;
	}
	
	public static function close(){
		if(self::$_connection){
			mysql_close(self::$_connection);
		}
	}
} 
