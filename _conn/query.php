<?php

//VERSION 1.0
//LAST_UPDATE 13/01/2015 - RICKY P.d

include_once dirname(__FILE__).DIRECTORY_SEPARATOR.'connect.php';

class Query {
	
	protected $_tableName;
	protected $_dbName;
	function __construct($dbName,$tableName){
		$this->_tableName = $tableName;
		$this->_dbName = $dbName;
	}
	
	public function connect(){
		return Connect::getConnection($this->_dbName);
	}
	
	public function close(){
		Connect::close();
	}
	
	function save(array $data){
		$sql = "INSERT INTO ".$this->_tableName." SET";
		foreach($data as $field => $value){
			$sql .= " `".$field."`='".mysql_real_escape_string($value, Connect::getConnection($this->_dbName))."',";
		}
		$sql = rtrim($sql, ',');
		$result = mysql_query($sql, Connect::getConnection($this->_dbName));
		if(!$result){
			throw new Exception('Gagal menyimpan data ke table '.$this->_tableName.': '.mysql_error());
		}
	}
	
	function update(array $data, $where = ''){
		$sql = "UPDATE ".$this->_tableName." SET";
		foreach($data as $field => $value){
			$sql .= " ".$field."='".mysql_real_escape_string($value, Connect::getConnection($this->_dbName))."',";
		}
		$sql = rtrim($sql, ',');
		if($where){
			$sql .= " WHERE ".$where;
		}
		$result = mysql_query($sql, Connect::getConnection($this->_dbName));
		if(!$result){
			throw new Exception('Gagal mengupdate data table '.$this->_tableName.': '.mysql_error());
		}
	}
	function updateBy($field, $value, array $data){
		$where = "`".$field."`='".mysql_real_escape_string($value, Connect::getConnection($this->_dbName))."'";
		$this->update($data, $where);
	}
	
	function delete($where = ''){
		$sql = "DELETE FROM ".$this->_tableName."";
		if($where){
			$sql .= " WHERE ".$where;
		}
		$result = mysql_query($sql, Connect::getConnection($this->_dbName));
		if(!$result){
			throw new Exception('Gagal menghapus data dari table '.$this->_tableName.': '.mysql_error());
		}
	}
	function deleteBy($field, $value){
		$where = "`".$field."`='".mysql_real_escape_string($value, Connect::getConnection($this->_dbName))."'";
		$this->delete($where);
	}
	
	function selectBy($field,$clause){
		include_once dirname(__FILE__).DIRECTORY_SEPARATOR.'select.php';
		$sql = "SELECT ".$field." FROM ".$this->_tableName ." WHERE ".$clause;
		
		return new select($this->_dbName,$sql);
	}
	function select($field,$option){
		include_once dirname(__FILE__).DIRECTORY_SEPARATOR.'select.php';
		$sql = "SELECT ".$field." FROM ".$this->_tableName ." ".$option;
		
		return new select($this->_dbName,$sql);
	}

	function findBy($select_field,$where_field,$value){
		include_once dirname(__FILE__).DIRECTORY_SEPARATOR.'select.php';
		$sql = "SELECT ".$select_field." FROM ".$this->_tableName;
		$sql .=" WHERE ".$where_field."='".$value."'";
		return new select($this->_dbName,$sql);
	}
	
}
