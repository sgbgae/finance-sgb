<?php
require_once 'google/appengine/api/mail/Message.php';
use google\appengine\api\mail\Message;

include('generator.php');
class email{
	private $_subyek;
	private $_to;
	private $_cc;
	private $_bcc;
	private $_bodyemail;
	function __construct($subyek, $to, $cc, $bcc, $bodyemail){
		$this->_subyek = $subyek;
		$this->_to = $to;
		$this->_cc = $cc;
		$this->_bcc = $bcc;
		$this->_bodyemail = $bodyemail;
	}
	
	function sendemail(){
		$mailsubject = $this->_subyek;
		$sendto = $this->_to;
		$sendcc = $this->_cc;
		$sendbcc = $this->_bcc;
		$bodyemail = $this->_bodyemail;
		
		$to = '';
		for($i=0;$i<count($sendto);$i++) $to .= $sendto[$i].",";
		
		$cc = '';
		for($i=0;$i<count($sendcc);$i++) $cc .= $sendcc[$i].",";
		
		$bcc = '';
		for($i=0;$i<count($sendbcc);$i++) $bcc .= $sendbcc[$i].",";
		$mail_options = [   
		 "sender" => "serversgb@saligadingbersama.com", 
		 "to" => $sendto,   
		 //"bcc" => $sendbcc,   
		 "subject" => $mailsubject, 
		 "textBody" => $bodyemail
		];	 
						
		IF ($sendto !== '' ) { 
			try {    
				$emaildikirim = new Message($mail_options);    
				$emaildikirim->send();
				//echo "<br>Selesai mengirim ke ".$to; 
			} 
			catch (InvalidArgumentException $e) {
				echo $e;	
			}
		}
		return $mailsubject." <br>".$to." <br>".$cc." <br>".$bcc." <br>".$bodyemail;
	}
	
	function sendemailapproval(){
		if(  !class_exists('passwordgenerator') ) { //echo "1";
			for($i=count($DOZPASSGEN)+1;$i<=count($DOZPASSGEN)+1;$i++){
				$DOZPASSGEN[$i] = new passwordgenerator();
				$ya =  $DOZPASSGEN[$i]->genPassword('mix',6);									
				$tidak = $DOZPASSGEN[$i]->genPassword('mix',6);  
				while ($ya ==  $tidak){
					$tidak = $DOZPASSGEN[$i]->genPassword('mix',6);    
				}
			}
		} else{ // echo "2"; 
			$DOZPASSGEN[1] = new passwordgenerator();
			$ya =  $DOZPASSGEN[1]->genPassword('mix',6);									
			$tidak = $DOZPASSGEN[1]->genPassword('mix',6);  
			while ($ya ==  $tidak){
				$tidak = $DOZPASSGEN[1]->genPassword('mix',6);    
			}
		}

		$mailsubject = $this->_subyek;
		$sendto = $this->_to;
		$sendcc = $this->_cc;
		$sendbcc = $this->_bcc;
	
		
		
		/*$ya =  $DOZPASSGEN->genPassword('mix',6);									
		$tidak = $DOZPASSGEN->genPassword('mix',6);  
		while ($ya ==  $tidak){
			$tidak = $DOZPASSGEN->genPassword('mix',6);    
		}*/
		
		$formatApp = '

KODE:
Y:'.$ya.'
# #
T:'.$tidak.'
# #
REPLY KE serversgb@saligadingbersama.com DENGAN COPY PASTE Y:KODE atau T:KODE
JIKA DIPERLUKAN KONFIRMASI, TULISKAN KONFIRMASI DI ANTARA TANDA #';
		$bodyemail = $this->_bodyemail.$formatApp;
		
		
		$to = '';
		for($i=0;$i<count($sendto);$i++) $to .= $sendto[$i].",";
		
		$cc = '';
		for($i=0;$i<count($sendcc);$i++) $cc .= $sendcc[$i].",";
		
		$bcc = '';
		for($i=0;$i<count($sendbcc);$i++) $bcc .= $sendbcc[$i].",";
		$mail_options = [   
		 "sender" => "serversgb@saligadingbersama.com", 
		 "to" => $sendto,   
		 //"bcc" => $sendbcc,   
		 "subject" => $mailsubject, 
		 "textBody" => $bodyemail
		];	 
						
		IF ($sendto !== '' ) { 
			try {    
				$emaildikirim = new Message($mail_options);    
				$emaildikirim->send();
				//echo "<br>Selesai mengirim ke ".$to; 
			} 
			catch (InvalidArgumentException $e) {
				echo $e;	
			}
		}
		return $mailsubject." <br>".$to." <br>".$cc." <br>".$bcc." <br>".$bodyemail;
	}
}
?>