<?php
class Paging{
function cariPosisi($batas){
if(empty($_GET['halkategori'])){
	$posisi=0;
	$_GET['halkategori']=1;
}
else{
	$posisi = ($_GET['halkategori']-1) * $batas;
}
return $posisi;
}

// Fungsi untuk menghitung total halaman
function jumlahHalaman($jmldata, $batas){
$jmlhalaman = ceil($jmldata/$batas);
return $jmlhalaman;
}

// Fungsi untuk link halaman 1,2,3 
function navHalaman($URL, $halaman_aktif, $jmlhalaman){
$link_halaman = "<!--div class='btn-group'-->";

// Link ke halaman pertama (first) dan sebelumnya (prev)
if($halaman_aktif > 1){
	$prev = $halaman_aktif-1;
	$link_halaman .= "<li><a class='btn btn-primary' href=".$URL."halkategori=1 class='nextprev'> << </a></li>
                    <li><a class='btn btn-primary' href=".$URL."halkategori=$prev class='nextprev'> < </a></li>";
}
else{ 
//$link_halaman .= "<span class='next page-numbers'> << </span>
//<span class='next page-numbers'> < </span>";
}

// Link halaman 1,2,3, ...

$angka = ($halaman_aktif > 3 ? " <li><span class='btn'>...</span></li> " : " "); 

for ($i=$halaman_aktif-2; $i<$halaman_aktif; $i++){
  if ($i < 1)
  	continue;
	  $angka .= "<li><a class='btn btn-primary' href=".$URL."halkategori=$i>$i</a></li> ";
  }
	 $angka .= " <li class='active'><span class='btn'>$halaman_aktif</span></li>";
	  
    for($i=$halaman_aktif+1; $i<($halaman_aktif+3); $i++){
    if($i > $jmlhalaman)
      break;
	  $angka .= "<li><a class='btn btn-primary' href=".$URL."halkategori=$i class='active'>$i</a></li> ";
    }
	  $angka .= ($halaman_aktif+2<$jmlhalaman ? "<li><span class='btn'>...</span></li> <li><a class='btn btn-primary' href=".$URL."halkategori=$jmlhalaman>$jmlhalaman</a></li>" : " ");
	
$link_halaman .= "$angka";

// Link ke halaman berikutnya (Lanjut) dan terakhir (Akhir) 
if($halaman_aktif < $jmlhalaman){
	$next = $halaman_aktif+1;
	$link_halaman .= " 
                     <li><a class='btn btn-primary' href=".$URL."halkategori=$next class='nextprev' > > </a></li>
					 <li><a class='btn btn-primary' href=".$URL."halkategori=$jmlhalaman class='nextprev'> >> </a></li>";
}
else{
	//$link_halaman .= "<span class='next page-numbers'> > </span>
	//<span class='next page-numbers'> >> </span>";
}
$link_halaman .= "<!--/div-->";


return $link_halaman;
}
}
?>