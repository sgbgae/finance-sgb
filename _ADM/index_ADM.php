<?php
session_start();

if(!empty($_SESSION['username'])){


?>
<!DOCTYPE html>
<html lang="en">
	<?php include '_template/head.php';?>
	<body id="page-top"><meta http-equiv="Refresh" content="1;URL=dashboard.php">
<?php include '_template/navbar_head.php';?>
<?php //include '_template/navbar_sub.php';?>

<!-- CSS YANG DIBUTUHKAN DI PAGE INI SAJA -->
<link rel="stylesheet" href="/js/DataTables/media/css/DT_bootstrap.css" />

<!------------------------------------------------------------------------->


<!--main-->
<div class="navbar">
	
	<div class="row">   
	
	<!-- BREADCRUMB UNTUK MEMPERLIHATKAN SEDANG BERADA DI MENU APA -->
			<!--<ol class="breadcrumb">
			<li class="active"><a href="#">Home</a></li>
			
			</ol>-->
	<!------------------------------------------------------------------------->
	<br><br>
	
     <div class="col-md-12 col-sm-12"> <!-- lg = large, md = medium, sm = small untuk melihat width nya, ada pada bootstrap.css -->

    	<div class="panelblue">
		<div id="view_data">
           <div class="panel-heading" style="margin-bottom: 0px;"> <h3>Selamat datang <?php echo ucwords(strtolower($_SESSION['nama_adm']));?></h3></div>
		   
   			<div class="panel-body" style="margin-bottom: 0px;">
                     
					  
					
       
       
    </div><!--playground-->
    
    <br>
    
    <div class="clearfix"></div>
    </div>
	
  </div>
</div>
</div>

<?php include "_template/navbar_footer.php"; ?>
</div><!--/main-->



	<!-- javascript yang dibutuhkan untuk halaman ini saja -->
		
		
		
		<script type="text/javascript" src="/js/DataTables/media/js/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="/js/DataTables/media/js/DT_bootstrap.js"></script>
		
		
		
		<!-- ----------------------------------------------------------------------------- -->
		
		
		<!-- ajax untuk insert data menggunakan _modal-and-datatable.js -->
		
		
		
		<script  src="/js/ajax/_modal-and-datatable.js"></script>
	
		
	</body>
</html>
<?php
}
else{
header('location:/lock.php?logingagal');
}
?>