<?php

session_start();

if(!empty($_SESSION['username'])){




?>
<?php 
include_once '_conn/query.php';
$table_KOTA = new query('HRD','MASTER_KOTA'); //('NAMA DATABASE','NAMA TABEL')
//FORMAT SEPERTI PADA CLASS QUERY, FUNCTION SELECT ('FIELD','WHERE CLAUSE')
$KOTA = $table_KOTA->selectBy("KOTA_ID,PROVINSI_ID,KOTA,KETERANGAN,AKTIF","SEMBUNYI = 'T' ORDER BY KOTA ASC");
$table_Prov= new query('HRD','MASTER_PROVINSI'); //('NAMA DATABASE','NAMA TABEL')
$PROVINSI = $table_Prov->selectBy("PROVINSI_ID,PROVINSI","AKTIF = 'Y' AND SEMBUNYI = 'T' ORDER BY PROVINSI ASC");
$table_karyawan= new query('HRD','KARYAWAN'); //('NAMA DATABASE','NAMA TABEL')


?>
<!DOCTYPE html>
<html lang="en">
	<?php include '../_template/head.php';?>
	<body id="page-top">
<?php include '../_template/navbar_head.php';?>
<?php include '../_template/navbar_sub.php';?>

<!-- CSS YANG DIBUTUHKAN DI PAGE INI SAJA -->
<link rel="stylesheet" href="/js/DataTables/media/css/DT_bootstrap.css" />

<!------------------------------------------------------------------------->


<!--main-->

<div id="tambah_agen" class="modal fade" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
      		<div class="modal-header">
          		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
          		<h3 class="text-center">Tambah Tagihan</h3>
      		</div>
      		<div class="modal-body">
			<form action="#" role="form" id="data_kota" style="font-size:12px;" class="form-horizontal col-md-12 center-block" method="POST">
				<div class="form-group">
					<label class="col-sm-3 control-label">PROVINSI</label>
					<div class="col-sm-5">
						<input type="hidden" class="form-control" id="id" name="id" autocomplete="off">
						<select class="form-control" id="select_agen" name="select_agen" autocomplete="off">
									<option value="">PILIH</option>
									<option value="1">TEST</option>
									</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Nama Kota</label>
					<div class="col-sm-5">
						<input type="text" class="form-control" id="select_hari" name="select_hari" placeholder="Nama Kota" autocomplete="off">
										
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Keterangan</label>
					<div class="col-sm-5">
						<input type="text" class="form-control" id="periode_add" name="periode_add" placeholder="Keterangan" autocomplete="off" required>
									</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Status</label>
					<div class="col-sm-5"><select class="form-control " id="komposisi_add" name="komposisi_add" >
											<option value="Y">AKTIF</option>
											<option value="T">TIDAK AKTIF</option>
											</select></div>
				</div>
				<div class="form-group"> 
					<button class="btn btn-primary btn-lg btn-block" data-dismiss="modal" type="button" id="submit_kota">Save</button>
				</div>
			</form>
			</div>
			<div class="modal-footer">
				<div class="col-md-12">
					<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
				</div>
			</div>
  		</div>
	</div>
</div>
<div class="container" id="main">
	
	<div class="row">   
	
	<!-- BREADCRUMB UNTUK MEMPERLIHATKAN SEDANG BERADA DI MENU APA -->
			<ol class="breadcrumb">
			<li><a href="#">Home</a></li>
			<li><a href="#">Master</a></li>
			<li><a href="#">Location</a></li>
			<li class="active">Kota</li>
			</ol>
	<!------------------------------------------------------------------------->
	
	
     <div class="col-md-12 col-sm-12"> <!-- lg = large, md = medium, sm = small untuk melihat width nya, ada pada bootstrap.css -->

    	<div class="panelblue">
		<div id="view_data" style="display:none;">
           <div class="panel-heading" style="margin-bottom: 0px;"> <h3>Master Kota 
		   <!--<a href="#" class="tambah_data" style="font-size:15px;"><b style="background-color: #eee;padding: 5px 5px;border-radius: 14px;">+ Tambah data </b></a>-->
		   <a href="#tambah_agen" style="font-size:15px;" data-toggle="modal"><strong>+ Tambah data</strong></a></h3></div>	</h3></div>
		   			
		   
   			<div class="panel-body" style="margin-bottom: 0px;">
                     
					  
						<div class="panel panel-default" style="margin-bottom: 0px;">
								
								<div class="panel-body">
								
									<table class="table table-striped" id="data_table">
										<thead>
											<tr style="background-color: #4B8DF8;font-size: 12px;">
												<th class="center"><font color= "white">Provinsi</font></th>
												<th class="center"><font color= "white">Kota</font></th>
												<th class="center"><font color= "white">Keterangan</font></th>
												<th class="center"><font color= "white">Aktif</font></th>
				
												<th class="center"><font color= "white">Opsi</font></th>
											</tr>
										</thead>
										<tbody>
										<?php foreach($KOTA as $KOTA){?>
										<tr style="font-size: 12px;">
										<td>
										
										<?php   
									
										
										$currentProv= $table_Prov->findBy('PROVINSI','PROVINSI_ID',$KOTA->PROVINSI_ID);
										$currentProv = $currentProv->current();
										echo $currentProv->PROVINSI;
										?>
										
										</td>
										<td>
										
										<?php echo $KOTA->KOTA;  ?>
										
										</td>
										<td>
										
											<?php echo $KOTA->KETERANGAN;  ?>
										
										</td>
										<td>
										
										<?php
										if($KOTA->AKTIF=='Y'){
										echo "Ya";
										}else{
										echo "Tidak";
										}
										?>
										
										</td>
										
										<td>
										<?php
										///CEK ID TELAH DITRANSAKSIKAN BELUM
									
										
										$currentKaryawan= $table_karyawan->findBy('TEMPATLAHIR_ID','TEMPATLAHIR_ID',$KOTA->KOTA_ID);
										$currentKaryawan = $currentKaryawan->current();
										
											if($currentKaryawan == '' ){ ?>
										<a href="#" class="edit_data" provinsi_id="<?php echo $KOTA->PROVINSI_ID; ?>" provinsi_nama="<?php echo $currentProv->PROVINSI; ?>" onClick="document.getElementById('id').value=<?php echo "'".$KOTA->KOTA_ID ."'"; ?>;document.getElementById('select_hari').value=<?php echo "'".$KOTA->KOTA ."'"; ?>;document.getElementById('periode_add').value=<?php echo "'".$KOTA->KETERANGAN ."'"; ?>;" ><i class='glyphicon glyphicon-edit'></i></a>	
										<a href="#" class="hapus_data" id=<?php echo "'".$KOTA->KOTA_ID ."'"; ?> ><font color='red'><i class='glyphicon glyphicon-trash'></i></font></a>	
										
										
										
										<?php }else{ 
										echo "&nbsp;";
										}?>
										
										
										
										</td>
										</tr>
										<?php }?>
										</tbody>
									</table>
									
								</div>
							</div>			 
       
       
    </div><!--playground-->
    
    <br>
    
    <div class="clearfix"></div>
    </div>
	
  </div>
</div>
</div>

<?php include "../_template/navbar_footer.php"; ?>
</div><!--/main-->



	<!-- javascript yang dibutuhkan untuk halaman ini saja -->
		
		
		
		<script type="text/javascript" src="/js/DataTables/media/js/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="/js/DataTables/media/js/DT_bootstrap.js"></script>
		
		
		
		<!-- ----------------------------------------------------------------------------- -->
		
		
		<!-- ajax untuk insert data menggunakan _modal-and-datatable.js -->
		
		
		
		<script  src="/js/ajax/_modal-and-datatable.js"></script>
		<!--script>
		var submit = <?php //echo "'"."button#".$submit."'" ;?>;
		var data_form = <?php //echo "'"."#".$data_form."'" ;?>;
		var act = 'ADD_ASURANSI';
		var peringatan = 'Penyimpanan gagal, nama kode / jenis sudah ada';
		</script-->
		
		<!-- ----------------------------------------------------------------------------- -->
		
		<script>
	
	$(function() {
	
	//untuk menampilkan div view_data saat awal load
	$('#view_data').show();
	act = '';
	action = '';
	peringatan = '';
	 //untuk menampilkan div add_data
	$(".tambah_data").click(function(){
	$('#view_data').hide();
	$('.select_komposisi_add').hide();
	$('#add_data').show();
	$('#data_kota')[0].reset();
	$("#select_agen").html("<option value=''>Pilihan</option><?php foreach($PROVINSI as $prov){?><option value='<?php echo $prov->PROVINSI_ID;?>'><?php echo $prov->PROVINSI;?></option><?php } ?>");
	act = 'KOTA';
	action = 'ADD';
	peringatan = 'Update gagal, nama sudah ada';
	$('.notification_select_hari').hide();
	$('.notification_periode_add').hide();	
	});
	
	$('#data_table').on('click','.edit_data',function (){
	$('#view_data').hide();
	$('#add_data').show();
	$('.select_komposisi_add').show();
	act = 'KOTA';
	action = 'UPDATE';
	peringatan = 'Update gagal, nama sudah ada';
	$('.notification_select_hari').hide();
	$('.notification_periode_add').hide();
	var provinsi_id=$(this).attr('provinsi_id');
	var provinsi_nama=$(this).attr('provinsi_nama');
	$("#select_agen").html("<option value="+"'"+provinsi_id+"'"+">"+provinsi_nama+"</option><?php foreach($PROVINSI as $prov2){?><option value='<?php echo $prov2->PROVINSI_ID;?>'><?php echo $prov2->PROVINSI;?></option><?php } ?>");
	});
	
	$('#data_table').on('click','.hapus_data',function (){
		var del_id= $(this).attr('id');
		act = 'KOTA';
		action = 'DELETE';
		if (confirm('Anda yakin ?')) {
		   	jQuery.ajax({
			
    		   	type: "POST",
			url: "_ADM/crud_master.php?act="+act+"&action="+action,
			 data:{'id':del_id},
        		success: function(msg){
 	          		 location.reload();
 		        },
			error: function(){
				alert("failure");
				
				}
      			});
		} else {
		return false;
		}			
	});
	
	 $(".lihat_data").click(function(){
		   $('#view_data').show();
		   $('#add_data').hide();
		    $(data_form)[0].reset();
	});	

	/** Ajax untuk ADD, DELETE DAN UPDATE DATA **/
	
	$('#submit_kota').click(function(){
			
			//Validasi field			
			if($("#select_agen").val()=="") {
						//alert('Field Asuransi tidak boleh kosong!');
						$('.notification_select_agen').show();
						$('.notification_select_agen').html('Pilihan provinsi tidak boleh kosong');
						$("#select_agen").focus();
						return false;
					}
			else{
			$('.notification_select_agen').hide();
			}	
			//
			
			//Validasi field			
			if($("#select_hari").val()=="") {
						//alert('Field Asuransi tidak boleh kosong!');
						$('.notification_select_hari').show();
						$('.notification_select_hari').html('Field Nama Kota tidak boleh kosong');
						$("#select_hari").focus();
						return false;
					}
			else{
			$('.notification_select_hari').hide();
			}	
			//
			
			//Validasi field
			if($("#periode_add").val()=="") {
						//alert('Field periode_add tidak boleh kosong!');
						$('.notification_periode_add').show();
						$('.notification_periode_add').html('Field Keterangan tidak boleh kosong');
						$("#periode_add").focus();
						return false;
					}
			else{
			$('.notification_periode_add').hide();
			}	
			//
			if (confirm('Anda yakin ?')) {
		   	$.ajax({
    		   	type: "POST",
			url: "_ADM/crud_master.php?act="+act+"&action="+action,
			data: $('#data_kota').serialize(),
        		success: function(msg){
 		        	if(msg==1){
					alert(peringatan);
					$('.notification_select_hari').show();
					$('.notification_select_hari').html('Update gagal, nama sudah ada');
					}else{
					window.location.replace(msg);
					
					}
					
 		        },
			error: function(){
				alert("koneksi bermasalah, silahkan reload halaman");
				
				}
      			});
    
			} else {
		return false;
		}
	
		});
		
	
	});
		</script>
		
	</body>
</html>
<?php
}
else{
header('location:/lock.php?logingagal');
}
?>