<?php

session_start();

if(!empty($_SESSION['username']) //&& !empty($_SESSION['action']) && ( ($_SESSION['action']) == 'SR' || ($_SESSION['action']) == 'SUPERADMIN' ) 
){
$alamat_email = $_SESSION['alamat_email']; 
$agen_id = '';
$namaagen = '';
$notagihan = '';
$namapelanggan = '';
$queryagen = "";
$halkategori = "";
$judul = "";
$date1 = date("Y-m-d");
$date2 = date("Y-m-d");
$date3 = date("Y-m-d");

IF(isset($_REQUEST['srch-term'])){
	$queryagen = "AND (K.KELOMPOK  LIKE '%".trim($_REQUEST['srch-term'])."%' OR M.NAMA_BARANG  LIKE '%".trim($_REQUEST['srch-term'])."%' )";
	$judul = "Pencarian berdasarkan '".trim($_REQUEST['srch-term'])."'";
}

if(isset($_GET['halkategori'])){
	$halkategori = "&halkategori=".$_GET['halkategori'];
}
?><?php 
include_once '_conn/query.php';
include_once 'modul/class_paging.php'; //CLASS UNTUK PAGING
include_once "../_crud/function.php";
?>
<!DOCTYPE html>
<html lang="en">
	<?php include '../_template/head.php'; //TEMPLATE HEADER DAN BERISI CSS GENERAL ?>
	<body id="page-top">
<?php include '../_template/navbar_head.php'; //TEMPLATE MENU YANG ADA PADA HEADER ?>
<?php //include '../_template/navbar_sub.php'; //TEMPLATE MENU YANG ADA PADA NAVIGASI BAR?>
<?php
//akses halaman
$queryfinance = new queryfinance();
$batas  = 20;
$arrayproduk = $queryfinance->produk();

$table_produk = new query('MASTER_GUDANG',$arrayproduk['from']);
$queryproduk = $table_produk->selectBy("DISTINCT M.MASTERBARANG_ID, CONCAT(ME.MERK, ' - ',M.NAMA_BARANG) AS NAMA_BARANG"
," 1 ".$arrayproduk['where']." ORDER BY ME.MERK,M.NAMA_BARANG "); //ECHO $queryproduk->printquery();
$optionproduk = '';
foreach($queryproduk as $currentproduk){
	$produk = $currentproduk->NAMA_BARANG;
	$produk_id = "'".$currentproduk->MASTERBARANG_ID."'";
	$optionproduk .='<option value='.$produk_id.'>'.$produk.'</option>';
}

$table_kelompok = new query('FINANCE','FINANCE.KELOMPOK');
$querykelompok = $table_kelompok->selectBy("KELOMPOK_ID, KELOMPOK"
," 1 "); //ECHO $querykelompok->printquery();
$optionkelompok = '';
foreach($querykelompok as $currentkelompok){
	$kelompok = $currentkelompok->KELOMPOK;
	$kelompok_id = "'".$currentkelompok->KELOMPOK_ID."'";
	$optionkelompok .='<option value='.$kelompok_id.'>'.$kelompok.'</option>';
}

?>
<!-- CSS YANG DIBUTUHKAN DI PAGE INI SAJA -->
<link rel="stylesheet" href="/js/DataTables/media/css/DT_bootstrap.css" />
<link rel="stylesheet" href="/css/jquery-ui/jquery-ui.css"> <?php //Datepicker ?>
<script src="/js/jquery/jquery.min.js"></script>
		<script src="/js/jquery-ui/jquery-ui.js"></script>
<!--script>

$(document).ready(function(){
   $('#srch-term').autocomplete({  
			source: '/ajax/autocompletetagihan.php',
			minLength: 1,
			max: 5,
			select: function( event, ui ) {
			document.getElementById('nama').value = ui.item.NIK;
			document.getElementById('id').value = ui.item.KARYAWAN_ID;
			}
		});
		
});
</script!-->
<style>
<?php //AGAR TABEL BISA DI SCROLL HORIZONTAL  ?>
.dataTables_wrapper {
	   overflow-x: auto;
	   display:block;
	   
}
</style>

<!------------------------------------------------------------------------->

<!--main-->
<div id="tambah_bg" class="modal fade" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
      		<div class="modal-header">
          		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
          		<h3 class="text-center">
          		  <div id="judul">Kelompok</div>
          		</h3>
      		</div>
      		<div class="modal-body">	
			<form class="form-horizontal col-md-12 center-block" method="POST" role="form" id="data_bg" style="font-size:12px;">
				<div class="form-group"> 
					<label class="col-sm-3 control-label">Nama Produk</label>
					<div class="col-sm-5 control-label">
						<input type="hidden" class="form-control" id="id" name="id" autocomplete="off">
						<strong><div id="select_produk2"  align="left" style="display:none">Nama Agen</div></strong>
						<select class="form-control" id="select_produk" name="select_produk" autocomplete="off" size="1">
						</select> 
						
						<font color = "red"><div class="notification_select_produk" align="left" style="display:none;"></div></font>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Kelompok</label>
					<div class="col-sm-5"> 
						<select class="form-control" id="select_kelompok" name="select_kelompok" autocomplete="off" size="1">
						</select>
						<font color = "red"><div class="notification_select_kelompok" style="display:none;"></div></font>
					</div>
				</div>
				<div class="form-group">
					  <button class="btn btn-primary btn-lg btn-block" data-dismiss="modal" type="button" id="submit_kelompok">Save</button>
				</div>
			</form>
			</div>
			<div class="modal-footer">
				<div class="col-md-12">
					<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
				</div>
			</div>
  		</div>
	</div>
</div>
<div class="navbar">
	<div class="row">   
	<!-- <?php //BREADCRUMB UNTUK MEMPERLIHATKAN SEDANG BERADA DI MENU APA ?>-->
			<!--<ol class="breadcrumb">
			<li><a href="#">Home</a></li>
			<li><a href="#">Master</a></li>
			<li class="active">Bank Garansi</li>
			</ol>-->
	<!------------------------------------------------------------------------->
	<br><br>
		<div class="col-md-12 col-sm-12"> <!-- <?php //lg = large, md = medium, sm = small untuk melihat width nya, ada pada bootstrap.css ?>-->
		  <div class="panelblue"> <?php //style card yang bisa di custom di styles.css ?> 
		  <span style="float:left;"><a href="/index_ADM.php">Home</a>
			> Master
			> <b>Kelompok</b>
</span><span style="float:right;">
<!--form method="GET" action="m_bg.php" >
									<input  align="right" type="text" 
									placeholder="Pencarian"  size="35" name="srch-term" id="srch-term"
								autocomplete="off">
                                   <input type="hidden" name="nama" id="nama" value="">
									<input type="hidden" name="id" id="id" value="">
										<button  type="submit"><i class="glyphicon glyphicon-search"></i></button>
										
										<?php
	 if($hasilcari != ''){ ?>
	 <!--<button  type="submit"><i class="glyphicon glyphicon-search"></i></button>
								  <a href="m_artikel.php" title="Kembali"><i class="fa fa-undo"></i></a>-->
								  <button onClick="m_bg.php"><i class="fa fa-undo"></i></button>
	<?php  } ?>
									</form--></span>
									<font size="4"><br><b> Kelompok</b></font>
								<a href="#tambah_bg" class="tambah_data" style="font-size:15px;" data-toggle="modal" produk_id="" produk_nama="Pilih Produk" kelompok_id="" kelompok_nama="Pilih Kelompok" onClick="document.getElementById('judul').innerHTML='Tambah Kelompok';document.getElementById('id').value='';"><strong>+ Tambah data</strong></a>
				
<!--
				<div id="view_data" >
          			<div class="panel-heading" style="margin-bottom: 0px;"> <h3>Master Bank Garansi 
						<a href="#tambah_bg" class="tambah_data" style="font-size:15px;" data-toggle="modal" agen_id="" agen_nama="Pilih Pelanggan - Area" onClick="document.getElementById('judul').innerHTML='Tambah Bank Garansi';document.getElementById('id').value='';document.getElementById('pelangganid').value='';document.getElementById('bank').value='';document.getElementById('cabang').value='';document.getElementById('nominal').value='';document.getElementById('nomor').value='';document.getElementById('tanggal1').value='<?php echo $date1; ?>';document.getElementById('tanggal2').value='<?php echo $date2; ?>';document.getElementById('tanggal3').value='<?php echo $date3; ?>';"><strong>+ Tambah data</strong></a>
					</h3></div>	</h3></div>
		   			<div class="panel-body" style="margin-bottom: 0px;">
						<div class="panel panel-default" style="margin-bottom: 0px;">
							<div class="panel-body">

								<form method="GET" action="m_bg.php" >
								<div style="padding-bottom:5px;border-bottom:1px solid #dadada;">
									<table class="table table-condensed table-hover borderless tableedit" id="biodata">
										<tbody>
											<tr>
												<td class="col-md-4" colspan="4"><strong>Search:</strong></td>
											</tr>
										</tbody>
									</table>
									
								</div>
								
								<div class="input-group" style="padding-bottom:5px;border-bottom:1px solid #dadada;">
									<input type="text" class="form-control hint" style="z-index:1;" placeholder="pencarian berdasarkan Nama Pelanggan, Nama Bank, Cabang, Nominal" name="srch-term" id="srch-term" data-container="body" data-placement="bottom" data-content="Untuk pencarian berdasarkan Nama Pelanggan, Nama Bank, Cabang, Nominal">
									<input type="hidden" name="nama" id="nama" value="">
									<input type="hidden" name="id" id="id" value="">
									<div class="input-group-btn">
										<button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
										<?php// if($queryagen != ''){ ?>
										<a class="btn btn-default" href="m_bg.php" title="Kembali"><i class="fa fa-undo"></i></a>
										<?php //} ?>
									</div>
								</div>
							
								</form>
						-->
						<form method="GET" action="m_kelompok.php" >
								<div class="input-group" style="padding-bottom:5px;border-bottom:1px solid #dadada;" >
									<input type="text" class="form-control hint"  placeholder="pencarian berdasarkan Nama Produk dan kelompok" name="srch-term" id="srch-term" data-container="body" data-placement="bottom" data-content="Untuk pencarian data Pelanggan berdasarkan Nama">
									<div class="input-group-btn">
										<button class="btn btn-default" type="submit" name="submitserch"><i class="glyphicon glyphicon-search"></i></button>
										<?php if($queryagen != '' and $queryagenx == '1'){ ?>
										<a class="btn btn-default" href="t_sj.php" title="Kembali"><i class="fa fa-undo"></i></a>
										<?php } ?>
									</div>
								</div>
							
				  </form>
			<?php //-----------------------------------------------------------//?>
			
								<?php //----------------------Table view--------------------------//?>	
								<div style="font-size: 15px;"><font><?php echo $judul;?></font></div>
								<?php
									include_once '../class.eyemysqladap.inc.php';
									include_once '../class.MultiGrid.inc.php';
									
									if (!isset($dbtask)) {
										$dbtask = new EyeMySQLAdap(':/cloudsql/database-sm:database-sm', 'root', '', 'FINANCE');
									}	
									$order = '';
									if(!isset($_REQUEST['order1'])){ $order = ' ORDER BY M.NAMA_BARANG'; 
									}
									
									$select = "K.KELOMPOK, K.KELOMPOK_ID,P.PRODUK_ID, SUBSTRING(M.NAMA_BARANG,1,LOCATE(' ISI',M.NAMA_BARANG)) AS NAMA_BARANG, 'Opsi' as Opsi";
									$from = "FINANCE.PRODUK P
										INNER JOIN MASTERGUDANG.MASTER_BARANG M ON M.MASTERBARANG_ID = P.PRODUK_ID
										INNER JOIN FINANCE.KELOMPOK K ON K.KELOMPOK_ID = P.KELOMPOK_ID";
									$filter = " AKTIF = 'Y' ".$queryagen.$order;
									
									$grid_lpb1 = new MultiGridcls($dbtask,'1');				
									$grid_lpb1->setQuery($select
									, $from
									, "kelompok",$filter);
									
									
									$grid_lpb1->setResultsPerPage($batas);
									$grid_lpb1->showRowNumber(); 
									$grid_lpb1->hideColumn('KELOMPOK_ID');
									$grid_lpb1->hideColumn('PRODUK_ID');
									
									$grid_lpb1->setColumnHeader('NAMA_BARANG', 'Nama Produk');
									$grid_lpb1->setColumnHeader('KELOMPOK', 'Kelompok');
									$grid_lpb1->setColumnType('Opsi', MultiGridcls::TYPE_FUNCTION, 'returnopsi', '1=>%PRODUK_ID% 2=>%NAMA_BARANG% 3=>%KELOMPOK_ID% 4=>%KELOMPOK% 5=>'); 
									
									
									function returnopsi($lastname) 
									{ 	
										$produk_id = trim(substr($lastname,strpos($lastname, "1=>")+3,strpos($lastname, "2=>")-3));
										$namabarang = trim(substr($lastname,strpos($lastname, "2=>")+3,strpos($lastname, "3=>")-strpos($lastname, "2=>")-3));
										$kelompokid =   trim(substr($lastname,strpos($lastname, "3=>")+3,strpos($lastname, "4=>")-strpos($lastname, "3=>")-3));	
										$kelompok =   trim(substr($lastname,strpos($lastname, "4=>")+3,strpos($lastname, "5=>")-strpos($lastname, "4=>")-3));				
										//return 		$kelompok;	
										return  "<a href=\"#tambah_bg\" class=\"edit_data\" style=\"font-size:15px;\" data-toggle=\"modal\" produk_id=\"".$produk_id."\" produk_nama=\"".$namabarang."\" kelompok_id=\"".$kelompokid."\" kelompok_nama= \"".$kelompok."\" onClick=\"document.getElementById('judul').innerHTML='Edit Kelompok';document.getElementById('id').value='".$produk_id."';\"><i class='glyphicon glyphicon-edit'></i></a>";
									}
									
									$grid_lpb1->printTable('table_custom2', '');
								?>
								
								    					<br>
    
    					<div class="clearfix"></div>
   					</div>
	
	<?php //Tambah data maupun edit data, di hide terlebih dahulu untuk nanti ditampilkan dengan menggunakan javascript dibawah?>
  			</div>
		</div>
	</div>
<?php include "../_template/navbar_footer.php"; ?>
</div><!--/main-->



	<?php //javascript yang dibutuhkan untuk halaman ini saja ?>
		
		
		
		<script src="/js/jquery-ui/jquery-ui.js"></script>
		<script type="text/javascript" src="/js/DataTables/media/js/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="/js/DataTables/media/js/DT_bootstrap.js"></script>
		
		
		
		<!-- ----------------------------------------------------------------------------- -->
		
		
		<?php //ajax untuk insert data menggunakan _modal-and-datatable.js ?>
		
		
		
		<script  src="/js/ajax/_modal-and-datatable.js"></script>
		
		
		<!-- ----------------------------------------------------------------------------- -->
		
		
		<script>
	
	$(function() {
	
	// Menjalankan plugin DataTables pada id contoh. id contoh merupakan tabel yang kita gunakan untuk menampilkan data
	$('#table_custom1').dataTable({"bSort": false,"bFilter": false,"bInfo": false,"bLengthChange": false,"bPaginate": false,
});
	$('#table_custom2').dataTable({"bSort": false,"bFilter": false,"bInfo": false,"bLengthChange": false,"bPaginate": false,
});
	$('#table_custom3').dataTable({"bSort": false,"bFilter": false,"bInfo": false,"bLengthChange": false,"bPaginate": false,
});
	
	<?php //Datepicker ?>
	$( "#tanggal1" ).datepicker({changeMonth: true, changeYear: true, yearRange: "-60:+10", dateFormat:"yy-mm-dd"});
	$( "#tanggal2" ).datepicker({changeMonth: true, changeYear: true, yearRange: "-60:+10", dateFormat:"yy-mm-dd"});
	$( "#tanggal3" ).datepicker({changeMonth: true, changeYear: true, yearRange: "-60:+10", dateFormat:"yy-mm-dd"});
	
	<?php //untuk menampilkan div view_data saat awal load ?>
	//$('#view_data').show();
	act = 'KELOMPOK';
	action = 'ADD';
	<?php //peringatan = ''; Digunakan untuk alert ?>
	
	<?php //untuk menampilkan div add_data  //   ?>
	$(".tambah_data").click(function(){
	//$('#view_data').hide(); <?php //Div untuk view data di hide ?>
	//$('.select_aktif').hide();
	//$('#add_data').show();
	//$('#data_token')[0].reset(); <?php //Data form direset ketika user kembali ke halaman tersebut ?>
		act = 'KELOMPOK';
		action = 'ADD';
		peringatan = 'Gagal memasukkan data, refresh halaman / hubungi MIS';
		var produk_id=$(this).attr('produk_id');
		var produk_nama=$(this).attr('produk_nama');
		$("#select_produk").html("<option value="+"'"+produk_id+"'"+">"+produk_nama+"</option><?php echo $optionproduk;?>");
				
		var kelompok_id=$(this).attr('kelompok_id');
		var kelompok_nama=$(this).attr('kelompok_nama');
		$("#select_kelompok").html("<option value="+"'"+kelompok_id+"'"+">"+kelompok_nama+"</option><?php echo $optionkelompok; ?>");
		 
		$('#select_produk2').hide();
		$('.notification_select_produk').hide();
		$('.notification_select_kelompok').hide();
		/*$('.notification_select_agen').hide();
		$('.notification_bank').hide();
		$('.notification_cabang').hide();
		$('.notification_nominal').hide();
		$('.notification_nomor').hide();
		$('.notification_tanggal1').hide();
		$('.notification_tanggal2').hide();
		$('.notification_tanggal3').hide();*/
	});
	
	<?php //Edit data ?>
	$('#table_custom2').on('click','.edit_data',function (){
		//$('#view_data').hide();
		//$('#add_data').show();
		$('.select_aktif').show(); <?php //Untuk pilihan token aktif atau tidak ?>
		act = 'KELOMPOK';
		action = 'UPDATE'; 
		peringatan = 'Update gagal, Data sudah ada';
		
		var produk_nama=$(this).attr('produk_nama');	
		var kelompok_id=$(this).attr('kelompok_id');
		var kelompok_nama=$(this).attr('kelompok_nama');
		$("#select_kelompok").html("<option value="+"'"+kelompok_id+"'"+">"+kelompok_nama+"</option><?php echo $optionkelompok; ?>");
		
		$('#select_produk').hide();
		$('#select_produk2').show();
		$('#select_produk2').html(produk_nama);
		//$('.notification_select_agen').hide();
		//$('.notification_select_posisi').hide();	 
	});
	
	 $(".lihat_data").click(function(){
		   $('#view_data').show();
		   $('#add_data').hide();
		    $(data_form)[0].reset();
	});	

	<?php //Ajax untuk ADD, DELETE DAN UPDATE DATA  ?>
	
	$('#submit_kelompok').click(function(){
			
			<?php //Validasi field ?>	
			if($("#select_produk").val()=="") {
						$('.notification_select_produk').show();
						$('.notification_select_produk').html('Pilihan Produk tidak boleh kosong');
						$("#select_produk").focus();
						return false;
					}
			else{
			$('.notification_select_produk').hide();
			}	
			
			if($("#select_kelompok").val()=="") {
						$('.notification_select_kelompok').show();
						$('.notification_select_kelompok').html('Pilihan Kelompok tidak boleh kosong');
						$("#select_kelompok").focus();
						return false;
					}
			else{
			$('.notification_select_kelompok').hide();
			}	
			
			if (confirm('Anda yakin ?')) {
		   	$.ajax({
    		   	type: "POST",
			url: "_ADM/crud_master.php?act="+act+"&action="+action,
			data: $('#data_bg').serialize(),
        		success: function(msg){
 		        	if(msg==1){ <?php //jika dari crud_master tidak sesuai kondisi makan akan di echo 1 yang berarti gagal menyimpan ?>
					alert(peringatan);
					
					
					}else{
					alert("Penyimpanan Sukses");
					window.location.replace("m_kelompok.php"); <?php //reload halaman sesuai url yang di echo di crud_master ?>
					}
					
 		        },
			error: function(){
				alert("koneksi bermasalah, silahkan reload halaman");
				
				}
      			});
    
			} else {
		return false;
		}
	
		});
		
	
	});
		</script>
		
	</body>
</html>
<?php
}
else{
header('location:/lock.php?logingagal');
}
?>