<?php

session_start();

if(!empty($_SESSION['username'])){ ?>
<?php 
include_once '_conn/query.php';
$table_metode = new query('FINANCE','METODE'); //('NAMA DATABASE','NAMA TABEL')
//FORMAT SEPERTI PADA CLASS QUERY, FUNCTION SELECT ('FIELD','WHERE CLAUSE')
$querymetode = $table_metode->selectBy("KDMETODE, METODE","1=1 ORDER BY METODE ASC");
//$table_karyawan= new query('HRD','KARYAWAN'); //('NAMA DATABASE','NAMA TABEL')
?>
<!DOCTYPE html>
<html lang="en">
	<?php include '../_template/head.php';?>
	<body id="page-top">
<?php include '../_template/navbar_head.php';?>
<?php include '../_template/navbar_sub.php';?>

<!-- CSS YANG DIBUTUHKAN DI PAGE INI SAJA -->
<link rel="stylesheet" href="/js/DataTables/media/css/DT_bootstrap.css" />

<!------------------------------------------------------------------------->
<!--main-->
<div id="tambah_metode" class="modal fade" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
      		<div class="modal-header">
          		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
          		<h3 class="text-center">Tambah Metode</h3>
      		</div>
      		<div class="modal-body">
			<form class="form-horizontal col-md-12 center-block" method="POST" role="form" id="data_karyawan" style="font-size:12px;">
				<div class="form-group">
					<label class="col-sm-3 control-label">NIK</label>
					<div class="col-sm-5">
						<input type="text" placeholder="Kode Metode" id="kdmetode" name="kdmetode" class="form-control input-sm nik" value="" autocomplete="off" >
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">sdsdf</label>
					<div class="col-sm-5">
						<input type="text" placeholder="Metode" id="metode" name="metode" class="form-control input-sm nik" value="" autocomplete="off" >
					</div>
				</div>
				<div class="form-group">
					  <button class="btn btn-primary btn-lg btn-block" data-dismiss="modal" type="button" id="submit_pekerjaan">Save</button>
				</div>
			</form>
      	</div>
    <div class="modal-footer">
          <div class="col-md-12">
			  	<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
			  </div>
   </div>
  </div>
  </div>
</div>
<div class="container" id="main">
	<div class="row">   
	<!-- BREADCRUMB UNTUK MEMPERLIHATKAN SEDANG BERADA DI MENU APA -->
			<ol class="breadcrumb">
			<li><a href="#">Home</a></li>
			<li><a href="#">Master</a></li>
			<li class="active">Metode</li>
			</ol>
	<!------------------------------------------------------------------------->
     <div class="col-md-12 col-sm-12"> <!-- lg = large, md = medium, sm = small untuk melihat width nya, ada pada bootstrap.css -->
    	<div class="panelblue">
			<div id="view_data">
           		<div class="panel-heading" style="margin-bottom: 0px;"> <h3>Data Metode 
					<!--<a href="#" class="tambah_data" style="font-size:15px;"><b style="background-color: #eee;padding: 5px 5px;border-radius: 14px;">+ Tambah data </b></a>-->
					<a href="#tambah_metode" style="font-size:15px;" data-toggle="modal"><strong>+ Tambah data</strong></a></h3></div>
   					<div class="panel-body" style="margin-bottom: 0px;">
						<div class="panel panel-default" style="margin-bottom: 0px;">
								<div class="panel-body">
									<table class="table table-striped" id="data_table">
										<thead>
											<tr style="background-color: #4B8DF8;font-size: 12px;">
											
												<th class="center"><font color= "white">Metode</font></th>
												<th class="center"><font color= "white">Kode Metode</font></th>
												<th class="center"><font color= "white" style="display:none">Opsi</font></th>
											</tr>
										</thead>
										<tbody>
										<?php foreach($querymetode as $metodecurrent){?>
										<tr style="font-size: 12px;">
										<td>
										<?php echo $metodecurrent->METODE;  ?>
										</td>
										<td>
											<?php echo $metodecurrent->KDMETODE;  ?>
										</td>
										<td style="display:none">
										<?php
										///CEK ID TELAH DITRANSAKSIKAN BELUM
										/*
										$currentKaryawan= $table_karyawan->findBy('BANK_ID','BANK_ID',$bank->BANK_ID);
										$currentKaryawan = $currentKaryawan->current();
										
											if($currentKaryawan == '' ){ ?>
										<a href="#" class="edit_data" onClick="document.getElementById('id').value=<?php echo "'".$bank->BANK_ID ."'"; ?>;document.getElementById('nama_bank').value=<?php echo "'".$bank->BANK ."'"; ?>;document.getElementById('keterangan').value=<?php echo "'".$bank->KETERANGAN ."'"; ?>;" ><i class='glyphicon glyphicon-edit'></i></a>	
										<a href="#" class="hapus_data" id=<?php echo "'".$bank->BANK_ID ."'"; ?> ><font color='red'><i class='glyphicon glyphicon-trash'></i></font></a>	
										
										
										
										<?php }else{ 
										echo "&nbsp;";
										}*/?>				
										</td>
										</tr>
										<?php }?>
										</tbody>
									</table>
								</div>
							</div>	
    					</div><!--playground-->
    					<br>
    					<div class="clearfix"></div>
   					</div>
					<!--<div id="add_data" style="display:none;">
           				<div class="panel-heading"> <h3>Data Metode <a href="#" class="lihat_data" style="font-size:15px;"><b style="background-color: #eee;padding: 5px 5px;border-radius: 14px;">&nbsp; Lihat data </b></a></h3></div>
                      	<div class="panel-body" style="margin-bottom: 0px;">
							<div class="col-sm-5">
								<!--<form action="#" role="form" id="data_bank" >
									<input type="hidden" class="form-control" id="id" name="id" autocomplete="off">
									
									<div class="form-group">
										<label>Kode Metode</label>
										<input type="text" class="form-control" id="nama_bank" name="nama_bank" placeholder="Nama bank" autocomplete="off">
										<font color = "red"><div class="notification_nama_bank" style="display:none;"></div></font>
									</div>										
									<div class="form-group">
										<label>Metode</label>
									  	<input type="text" class="form-control" id="keterangan" name="keterangan" placeholder="Keterangan" autocomplete="off" required>
										<font color = "red"><div class="notification_keterangan" style="display:none;"></div></font>
									</div>
									<div class="form-group select_aktif" style="display:none;">
										<label>Status</label>
										<div class="form-group">
											<div class="row">
												<div class="col-sm-3">		
													<select class="form-control " id="aktif" name="aktif" >
													<option value="Y">AKTIF</option>
													<option value="T">TIDAK AKTIF</option>
													</select>
												</div>
											</div>
										</div>
									</div>
									<div class="form-group">
										<div class="col-sm-12">
											<label class="col-sm-3">&nbsp;</label>
											<div class="row">
												<div class="pull-right">		
									  				<button class="btn btn-primary btn-md" type="button" id="submit_bank">Simpan</button>
													<button class="btn btn-danger btn-md lihat_data" type="button">Kembali</button>
									  			</div>
									 		</div>
									 	</div>
									</div>
								</form>
							</div>
    					</div>
  					<br>
    				<div class="clearfix"></div>
    				</div>-->
  				</div>
			</div>
		</div>
	<?php include "../_template/navbar_footer.php"; ?>
	</div><!--/main-->
<!-- javascript yang dibutuhkan untuk halaman ini saja -->
<script type="text/javascript" src="/js/DataTables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/js/DataTables/media/js/DT_bootstrap.js"></script>
<!-- ----------------------------------------------------------------------------- -->
		
<!-- ajax untuk insert data menggunakan _modal-and-datatable.js -->
<script  src="/js/ajax/_modal-and-datatable.js"></script>
<!--script>
var submit = <?php //echo "'"."button#".$submit."'" ;?>;
var data_form = <?php //echo "'"."#".$data_form."'" ;?>;
var act = 'ADD_ASURANSI';
var peringatan = 'Penyimpanan gagal, nama kode / jenis sudah ada';
</script-->
	
<!-- ----------------------------------------------------------------------------- -->
		
<script>
	
	$(function() {
	
	//untuk menampilkan div view_data saat awal load
	//$('#view_data').show();
	act = '';
	action = '';
	peringatan = '';
	 //untuk menampilkan div add_data
	/*$(".tambah_data").click(function(){
	$('#view_data').hide();
	$('.select_aktif').hide();
	$('#add_data').show();
	$('#data_bank')[0].reset();
	act = 'BANK';
	action = 'ADD';
	peringatan = 'Update gagal, nama sudah ada';
	$('.notification_nama_bank').hide();
	$('.notification_keterangan').hide();		
	});
	
	$('#data_table').on('click','.edit_data',function (){
	$('#view_data').hide();
	$('#add_data').show();
	$('.select_aktif').show();
	act = 'BANK';
	action = 'UPDATE';
	peringatan = 'Update gagal, nama sudah ada';
	$('.notification_nama_bank').hide();
	$('.notification_keterangan').hide();	 
	});*/
	
	$('#data_table').on('click','.hapus_data',function (){
		var del_id= $(this).attr('id');
		act = 'BANK';
		action = 'DELETE';
		if (confirm('Anda yakin ?')) {
		   	jQuery.ajax({
			
    		   	type: "POST",
			url: "_ADM/crud_master.php?act="+act+"&action="+action,
			 data:{'id':del_id},
        		success: function(msg){
 	          		 location.reload();
 		        },
			error: function(){
				alert("failure");
				
				}
      			});
		} else {
		return false;
		}			
	});
	
	 $(".lihat_data").click(function(){
		   $('#view_data').show();
		   $('#add_data').hide();
		    $(data_form)[0].reset();
	});	

	/** Ajax untuk ADD, DELETE DAN UPDATE DATA **/
	
	$('#submit_bank').click(function(){
			
			//Validasi field			
			if($("#nama_bank").val()=="") {
						//alert('Field Asuransi tidak boleh kosong!');
						$('.notification_nama_bank').show();
						$('.notification_nama_bank').html('Field Nama bank tidak boleh kosong');
						$("#nama_bank").focus();
						return false;
					}
			else{
			$('.notification_nama_bank').hide();
			}	
			//
			
			//Validasi field
			if($("#keterangan").val()=="") {
						//alert('Field keterangan tidak boleh kosong!');
						$('.notification_keterangan').show();
						$('.notification_keterangan').html('Field Keterangan tidak boleh kosong');
						$("#keterangan").focus();
						return false;
					}
			else{
			$('.notification_keterangan').hide();
			}	
			//
			
			if (confirm('Anda yakin ?')) {
		   	$.ajax({
    		   	type: "POST",
			url: "_ADM/crud_master.php?act="+act+"&action="+action,
			data: $('#data_bank').serialize(),
        		success: function(msg){
 		        	if(msg==1){
					alert(peringatan);
					$('.notification_nama_bank').show();
					$('.notification_nama_bank').html('Update gagal, nama sudah ada');
					}else{
					window.location.replace(msg);
					
					}
					
 		        },
			error: function(){
				alert("koneksi bermasalah, silahkan reload halaman");
				
				}
      			});
    
			} else {
		return false;
		}
	
		});
});
</script>
</body>
</html>
<?php
}
else{
header('location:/lock.php?logingagal');
}
?>