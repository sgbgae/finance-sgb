<?php

session_start();

if(!empty($_SESSION['username'])){

?>
<?php 
include_once '_conn/query.php';
$hariini = date("Y-m-d");
$table_pelanggan= new query('PROFILE','PROFILE.PROFILE PP, MARKETING.PELANGGAN MP'); //('NAMA DATABASE','NAMA TABEL')


if(isset($_GET['nama']) and isset($_GET['id'])){	$GET_NAMA = $_GET['nama'];
$GET_ID_PELANGGAN = $_GET['id']; 


$pelanggan = $table_pelanggan->selectBy(
'PP.PROFILE_ID, MP.PELANGGAN_ID, MP.PELANGGAN_ALIAS, MP.TIPEKONTAK, MP.STATUSAGEN, MP.ALAMATEMAIL, MP.NPWP, MP.GPS
, PP.NAMA, PP.KODE, PP.KETERANGAN',
'PP.PROFILE_ID = "'.$GET_ID_PELANGGAN.'" AND PP.PROFILE_ID=MP.PELANGGAN_ID');
//echo $pelanggan->printquery();
$pelanggan = $pelanggan->current();
$GET_ID_PELANGGAN =  $pelanggan->PELANGGAN_ID;

$table_ketentuan = new query('FINANCE','FINANCE.AGEN A');
$queryketentuan = $table_ketentuan->selectBy("A.PELANGGAN_ID, A.MINGGUAN, A.PERIODE"
	,"A.PELANGGAN_ID = '".$GET_ID_PELANGGAN."' ");
$currentketentuan = $queryketentuan->current();
//ECHO $queryketentuan->num_rows();
$numqueryketentuan = $queryketentuan->num_rows();

$day = array('', 'Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu');
$optionhari = '';	
$daysx = '';											//echo $day[1];
for($i=0;$i<=7;$i++){
	if($day[$i] != ''){
		$optionhari .= '<option value='.$i.'>'.$day[$i].'</option>';
	} else if($day[$i] == ''){
		$optionhari .= '<option value='.$daysx.'>'.$day[$i].'</option>';
	}
}

if($queryketentuan->num_rows() > 0){
	if($currentketentuan->PERIODE>=0 and $currentketentuan->PERIODE != '-' and $currentketentuan->PERIODE != '' and $currentketentuan->MINGGUAN != 0 and $currentketentuan->MINGGUAN != ''){
		$periode = $currentketentuan->PERIODE;
		$periodename = $day[$currentketentuan->PERIODE];
		$mingguan = $currentketentuan->MINGGUAN;
		$periodename2 = $day[$currentketentuan->PERIODE+1];
		$periodenamex = $periodename2." s/d ".$periodename;
	} else { 
		$periode = '';
		$periodename = 'Pilih Hari';
		$mingguan = $currentketentuan->MINGGUAN;
	} 
} else { 
	$periode = '';
	$periodename = 'Pilih Hari';
	$mingguan = '';
}
//AGEN TIPE

$table_tipe= new query('MARKETING','MARKETING.AGEN_TIPE AT, MARKETING.AGEN_TIPE2 AT2');

$querytipe = $table_tipe->selectBy(
'AT.TIPE'
, 'AT2.PROFILE_ID = "'.$GET_ID_PELANGGAN.'"
AND AT.AGENTIPE_ID  = AT2.AGENTIPE_ID 
AND (AT2.TGL_SELESAI IS NULL OR AT2.TGL_SELESAI LIKE "%0000%")');
//echo $queryarea->printquery();
$tipecurrent = $querytipe->current();

$table_area= new query('MARKETING','MARKETING.MKT_AREA A, MARKETING.AGEN_AREA AA');

$queryarea = $table_area->selectBy(
'A.AREA, A.MKTAREA_ID'
, 'AA.KONTAK_ID = "'.$GET_ID_PELANGGAN.'" 
AND (AA.TGL_SELESAI IS NULL OR AA.TGL_SELESAI LIKE "%0000%")
AND A.MKTAREA_ID = AA.MKTAREA_ID');
//echo $queryarea->printquery();
$areacurrent = $queryarea->current();
$area = $areacurrent->AREA;
$areaid = $areacurrent->MKTAREA_ID;

$table_propinsi= new query('MARKETING','MARKETING.MKT_PROPINSI P, MARKETING.MKT_PROAREA MP');
$querypropinsi = $table_propinsi->selectBy(
'P.PROPINSI, P.MKTPROPINSI_ID'
, 'MP.MKTAREA_ID = "'.$areaid.'"
AND (MP.TGL_SELESAI IS NULL OR MP.TGL_SELESAI LIKE "%0000%")
AND P.MKTPROPINSI_ID = MP.MKTPROPINSI_ID');
//echo $querypropinsi->printquery();
$propinsicurrent = $querypropinsi->current();
$propinsi = $propinsicurrent->PROPINSI;
$propinsiid = $propinsicurrent->MKTPROPINSI_ID;

$table_wilayah = new query('MARKETING','MARKETING.MKT_WILAYAH W, MARKETING.MKT_WILPRO MW');
$querywilayah = $table_wilayah->selectBy(
'W.WILAYAH, W.MKTWILAYAH_ID'
, 'MW.MKTPROPINSI_ID = "'.$propinsiid.'"
AND (MW.TGL_SELESAI IS NULL OR MW.TGL_SELESAI LIKE "%0000%")
AND W.MKTWILAYAH_ID = MW.MKTWILAYAH_ID');
//echo $querywilayah->printquery();
$wilayahcurrent = $querywilayah->current();
$wilayah = $wilayahcurrent->WILAYAH;
$wilayahid = $wilayahcurrent->MKTWILAYAH_ID;

$table_regional = new query('MARKETING','MARKETING.MKT_REGIONAL R, MARKETING.MKT_REGWIL MR');
$queryregional = $table_regional->selectBy(
'R.REGIONAL'
, 'MR.MKTWILAYAH_ID = "'.$wilayahid.'"
AND (MR.TGL_SELESAI IS NULL OR MR.TGL_SELESAI LIKE "%0000%")
AND R.MKTREGIONAL_ID = MR.MKTREGIONAL_ID');
//echo $querywilayah->printquery();
$regionalcurrent = $queryregional->current();
$regional = $regionalcurrent->REGIONAL;

$table_alamat = new query('PROFILE', 'PROFILE.PROFILE P, PROFILE.PROFILE_ALAMAT A, PROFILE.KOTA K, PROFILE.PROPINSI PR, PROFILE.NEGARA N,  MARKETING.PELANGGAN_ALAMAT I');
$queryalamat = $table_alamat->selectBy(
'A.PROFILEALAMAT_ID, A.ALAMAT, A.JALAN, A.KODEPOS, A.KETERANGAN, K.KOTA, PR.PROPINSI, N.NEGARA, I.ALAMATKIRIM, I.HRPENGIRIMAN'
, 'P.PROFILE_ID = A.PROFILE_ID
AND A.PROFILE_ID = "'.$GET_ID_PELANGGAN.'"
AND K.KOTA_ID = A.KOTA_ID
AND PR.PROPINSI_ID = K.PROPINSI_ID
AND N.NEGARA_ID = PR.NEGARA_ID
AND I.PROFILE_ID = P.PROFILE_ID
AND I.PELANGGANALAMAT_ID = A.PROFILEALAMAT_ID
AND (A.TGL_SELESAI IS NULL OR A.TGL_SELESAI LIKE "%0000%")
ORDER BY I.ALAMATKIRIM DESC');
//$alamatcurrent = $queryalamat->current();
//echo $queryalamat->printquery();

$table_pic = new query('PROFILE', 'PROFILE.PROFILE_PIC I, MARKETING.PEGAWAI P, HRD.KARYAWAN K');
$querypic = $table_pic->selectBy(
'I.JABATAN, K.NAMA, K.NIK, I.KETERANGAN'
, 'I.NAMA = P.PEGAWAI_ID
AND P.AKTIF = "Y"
AND K.NIK = P.NIK
AND I.PROFILE_ID = "'.$GET_ID_PELANGGAN.'"');
//echo $querypic->printquery();
/*
$table_infolain = new query('PROFILE', 'PROFILE.PROFILE_INFO I, PROFILE.PROFILE P, PROFILE.FIELD F');
$queryinfolain = $table_infolain->selectBy(
'F.FIELD, I.NILAI, I.KETERANGAN'
, 'I.PROFILE_ID = P.PROFILE_ID
AND I.FIELD_ID =F.FIELD_ID
AND I.PROFILE_ID = "'.$GET_ID_PELANGGAN.'"');
//echo $queryinfolain->printquery();
*/
$table_TOP = new query('MARKETING', 'MARKETING.TOP T, MARKETING.KODETRANSAKSI K');
$queryTOP = $table_TOP->selectBy(
'T.TOP_ID, K.TRANSAKSI, T.TOP, T.TGL_MULAI, T.TGL_SELESAI'
, 'K.KODETRANSAKSI_ID = T.KODETRANSAKSI_ID
AND T.PELANGGAN_ID = "'.$GET_ID_PELANGGAN.'"
AND (T.TGL_SELESAI IS NULL OR T.TGL_SELESAI LIKE "%0000-00-00%"  or T.TGL_SELESAI>="'.$hariini.'")');
//echo $queryTOP->printquery();

$table_limit = new query('MARKETING', 'MARKETING.LIMITKREDIT L, MARKETING.CURRENCY C, MARKETING.KODETRANSAKSI K');
$querylimit = $table_limit->selectBy(
'L.LIMITKREDIT_ID, L.LIMITKREDIT, C.CURRENCY, K.TRANSAKSI, L.TGL_MULAI, L.TGL_SELESAI'
, 'K.KODETRANSAKSI_ID = L.KODETRANSAKSI_ID
AND L.PELANGGAN_ID = "'.$GET_ID_PELANGGAN.'"
AND (L.TGL_SELESAI IS NULL OR TGL_SELESAI LIKE "%0000%"  or TGL_SELESAI>="'.$hariini.'")
AND C.CURRENCY_ID = L.CURRENCY_ID');
//echo $querylimit->printquery();

$namapelanggan = $pelanggan->NAMA;
$namapelangganalias = $pelanggan->PELANGGAN_ALIAS;
$tipekontak = $pelanggan->TIPEKONTAK;
$status = $pelanggan->STATUSAGEN;
$alamatemail = $pelanggan->ALAMATEMAIL;
$npwp = $pelanggan->NPWP;
$ket = $pelanggan->KETERANGAN;
$kode = $pelanggan->KODE;
$gps = $pelanggan->GPS;
$tipe = $tipecurrent->TIPE;
	
} else{
	$GET_ID_PELANGGAN = "";
	$namapelanggan = "";
	$namapelangganalias = "";
	$tipekontak = "";
	$status = "";
	$alamatemail = "";
	$npwp = "";
	$ket = "";
	$kode = "";
	$gps = "";
	$tipe = "";
	$area = "";
	$propinsi = "";
	$wilayah = "";
	$regional = "";
	$periode = '';
	$periodename = 'Pilih Hari';
	$mingguan = '';
	$numqueryketentuan = '';
}

?>
<div id="tambah_agen" class="modal fade" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
      		<div class="modal-header">
          		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
          		<h3 class="text-center"><div id="judul">Pelanggan</div></h3>
      		</div>
      		<div class="modal-body">
			<form action="#" role="form" id="data_agen" style="font-size:12px;" class="form-horizontal col-md-12 center-block" method="POST">
				<div class="form-group">
					<label class="col-sm-3 control-label">Nama Pelanggan </label>
					<div class="col-sm-5 control-label">
						<input type="hidden" class="form-control" id="id" name="id" autocomplete="off">
						<strong><div id="select_agen"  align="left"></div></strong>
						<!--<select class="form-control" id="select_agen" name="select_agen" autocomplete="off">
							<option value="">Pilih</option>
							<option value="x">x</option>
						</select>-->
					</div>
				</div>
				<div class="form-group"> 
					<label class="col-sm-3 control-label">Periode Cut off</label>
					<div class="col-sm-5">
						<select class="form-control" id="select_hari" name="select_hari" autocomplete="off" size="1">
						</select>
						<font color = "red"><div class="notification_select_hari" style="display:none;"></div></font>
					</div>
				</div>
				<div class="form-group">
					  <button class="btn btn-primary btn-lg btn-block" data-dismiss="modal" type="button" id="submit_agen">Save</button>
				</div>
			</form>
			</div>
			<div class="modal-footer">
				<div class="col-md-12">
					<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
				</div>
			</div>
  		</div>
	</div>
</div>

<!DOCTYPE html>
<html lang="en">
	<?php include '../_template/head.php';?>
	<body id="page-top">
<?php include '../_template/navbar_head.php';?>
<?php //include '../_template/navbar_sub.php';?>

<!-- CSS YANG DIBUTUHKAN DI PAGE INI SAJA -->
<link rel="stylesheet" href="/js/DataTables/media/css/DT_bootstrap.css" />
<link rel="stylesheet" href="/js/x-editable/css/bootstrap-editable.css">
<link rel="stylesheet" href="/css/jquery-ui/jquery-ui.css">
<style>
.tableedit, .tableedit TD, .tableedit TH
{

font-size:10pt;

}
</style>
<script src="/js/jquery/jquery.min.js"></script>
		<script src="/js/jquery-ui/jquery-ui.js"></script>
<script>

$(document).ready(function(){
   $('#srch-term').autocomplete({  
			source: '/ajax/autocompletepelanggan.php',
			minLength: 1,
			max: 5,
			select: function( event, ui ) {
			document.getElementById('nama').value = ui.item.NIK;
			document.getElementById('id').value = ui.item.KARYAWAN_ID;
			}
		});
		
});
</script>
<!------------------------------------------------------------------------->
<!--main-->
<div class="navbar">
	<div class="row" >   <br><br>
	<!-- BREADCRUMB UNTUK MEMPERLIHATKAN SEDANG BERADA DI MENU APA -->
		<!--<ol class="breadcrumb">
			<li><a href="#">Home</a></li>
			<li><a href="#">Master</a></li>
			<li><a href="m_agen.php">Pelanggan</a></li>
			<li class="active"><?php //echo $GET_NAMA;?></li>
		</ol>-->
	<!------------------------------------------------------------------------->
		<div class="col-md-12 col-sm-12"> <!-- lg = large, md = medium, sm = small untuk melihat width nya, ada pada bootstrap.css -->
			<div class="col-md-3">
				<div class="panelblue">
					<div class="user-image">
						<?php if($GET_ID_PELANGGAN != ""){ ?>
						<div class="panel-heading">
							<table class="table table-condensed table-hover">
							<thead>
								<tr>
									<th colspan="3" style="font-size:15px;"><center><?php echo $namapelanggan;?> 
									<?php if(isset($_REQUEST['i'])){ ?>
									<a class="btn btn-default" title="Kembali" onClick="window.history.back(<?php echo $_REQUEST['i']; ?>)" ><i class="fa fa-undo"></i></a>
									<?php } else{ ?>
										
									<a class="btn btn-default" title="Kembali" onClick="window.history.back()" ><i class="fa fa-undo"></i></a>
									<?php }?>
									</center></th> 
								</tr>
							</thead>
							</table>
						</div>
						<?php } ?>
						<div class="fileupload-new thumbnail">
						<?php if($GET_ID_PELANGGAN != ""){ ?>
							<table class="table-condensed table-hover">
							<thead style="font-size:11px;">
								<tr>
									<td><strong>Regional</strong></td>
									<td>: <?php echo stripslashes($regional); ?></td>
								</tr>
								<tr>
									<td><strong>Wilayah</strong></td>
									<td>: <?php  echo stripslashes($wilayah); ?></td>
								</tr>
								<tr>
									<td><strong>Propinsi</strong></td>
									<td>: <?php  echo stripslashes($propinsi); ?></td>
								</tr>
								<tr>
									<td><strong>Area</strong></td>
									<td>: <?php echo stripslashes($area); ?></td>
								</tr>
							</thead>
							</table>
							<?PHP //if($gps != ''){include_once "map.php";?>
								<!-- <div id="googleMap2" style="height:380px;" class="img-responsive"></div>
							<?php //} else { ?>
								<img src="images/photo_maps.png" width="255px" >-->
							<?php //} ?><?php } ?>
						</div>
						
					</div>
				<div class="panel-body" >
					<div style="margin-left:-10px;margin-right:-10px;margin-top:-30px;margin-bottom:10px;">
						<!--<form method="GET" action="m_detailagen.php" >
							<div class="input-group"style="padding-bottom:5px;border-bottom:1px solid #dadada;">
								<input type="text" class="form-control hint" style="z-index:999;" placeholder="Search by name" name="srch-term" id="srch-term" data-container="body" data-placement="bottom" data-content="Untuk pencarian data Pelanggan berdasarkan Nama">
								<input type="hidden" name="nama" id="nama" value="TIGA EF, CV">
								<input type="hidden" name="id" id="id" value="02014FB000001">
								<div class="input-group-btn">
									<button class="btn btn-default btn-primary" type="submit"><i class="glyphicon glyphicon-search"></i></button>
								</div>
							</div>
						</form>-->
						<ul class="nav nav2 nav-pills2 nav-stacked2" style="margin-left:-5px;margin-right:-5px;margin-top:5px;">
							<li class="nav-header"></li>
							<li class="active"><a href="#panel_tab3_example1" data-toggle="tab" class="hint" data-container="body" data-placement="top" data-content="Untuk berpindah halaman"><i class="glyphicon glyphicon-list"></i> Profil Pelanggan </a></li>
							<li><a href="#panel_tab3_example2" data-toggle="tab"><i class="glyphicon glyphicon-home"></i> Alamat &amp; Kontak</a></li>
							<li><a href="#panel_tab3_example3" data-toggle="tab"><i class="glyphicon glyphicon-briefcase"></i> PIC </a></li>
							<li><a href="#panel_tab3_example6" data-toggle="tab"><i class="glyphicon glyphicon-usd"></i> Info Pembayaran</a></li>
						</ul>
					</div>					 
					<hr/>													
					<div class="panel panel-default">
						<!--<a class="btn btn-primary center-block hint" href="#addKaryawan" data-toggle="modal" data-container="body" data-placement="bottom" data-content="Untuk menambah Profil Pelanggan"><strong>Tambah Pelanggan</strong></a>-->
					</div>																															
				</div>
			</div>
		</div>
		<div class="col-sm-7 col-md-9">
			<div class="tab-content">
								<div class="tab-pane in active" id="panel_tab3_example1">
									<div class="col-sm-7 col-md-12">
										<div class="paneltosca">
											<div class="panel-heading">
												<table class="table">
													<thead>
														<tr>
															<th>Profil Pelanggan</th>
														</tr>
													</thead>
												</table>
											</div>
											<div class="panel-body" style="margin-top: -15px;">
												<table class="table table-condensed table-hover borderless tableedit" id="biodata">
													<tbody>
														<tr>
															<td class="col-md-4"><strong>Nama Pelanggan</strong></td>
															<td>: <a href="#" id="nama_lengkap" name="nama_lengkap"  data-type="text" data-pk="1"  data-original-title="Edit data">
																<?php echo stripslashes($namapelanggan) ; ?>
															</a></td>
														</tr>
														<tr>
															<td class="col-md-4"><strong>Nama Alias</strong></td>
															<td>: <a href="#" id="nama_lengkap" name="nama_lengkap"  data-type="text" data-pk="1"  data-original-title="Edit data">
																<?php echo stripslashes($namapelangganalias) ; ?>
															</a></td>
														<tr>
															<td><strong>Regional</strong></td>
															<td>: <a href="#" id="jenis_kelamin" data-type="select" data-pk="1" data-value="" data-source="/JK" data-original-title="Edit data">
																<?php echo stripslashes($regional); ?>
																</a></td>
														</tr>
														<tr>
															<td><strong>Wilayah</strong></td>
															<td>: <a href="#" id="jenis_kelamin" data-type="select" data-pk="1" data-value="" data-source="/JK" data-original-title="Edit data">
																<?php  echo stripslashes($wilayah); ?>
																</a></td>
														</tr>
														<tr>
															<td><strong>Propinsi</strong></td>
															<td>: <a href="#" id="jenis_kelamin" data-type="select" data-pk="1" data-value="" data-source="/JK" data-original-title="Edit data">
																 <?php  echo stripslashes($propinsi); ?>
															</a></td>
														</tr>
														<tr>
															<td><strong>Area</strong></td>
															<td>: <a href="#" id="jenis_kelamin" data-type="select" data-pk="1" data-value="" data-source="/JK" data-original-title="Edit data">
																<?php echo stripslashes($area); ?>
															</a></td>
														</tr>
														<tr>
															<td class="col-md-4"><strong>Kode</strong></td>
															<td>: <?php echo stripslashes($kode) ; ?></td>
														</tr>
														<tr>
															<td class="col-md-4"><strong>GPS</strong></td>
															<td>: <?php echo stripslashes($gps); ?></td>
														</tr>
														<tr>
															<td class="col-md-4"><strong>Tipe Kontak</strong></td>
															<td>: <?php echo stripslashes($tipekontak) ; ?></td>
														</tr>
														<tr>
															<td class="col-md-4"><strong>Status Agen</strong></td>
															<td>: <?php echo stripslashes($status) ; ?></td>
														</tr>
														<tr>
															<td class="col-md-4"><strong>Alamat email</strong></td>
															<td>: <?php echo stripslashes($alamatemail) ; ?></td>
														</tr>
														<tr>
															<td><strong>No NPWP</strong></td>
															<td>: <?php echo stripslashes($npwp) ; ?></td>
														</tr>
														<tr>
															<td><strong>Tipe</strong></td>
															<td>: <?php echo stripslashes($tipe) ; ?></td>
														</tr>
														
													</tbody>
												</table>
											</div>
										</div>
									</div>
									<div class="col-sm-7 col-md-12">
										<div class="paneltosca">
											<div class="panel-heading">
												<table class="table">
												<thead>
													<tr>
														<th>Cut off Tagihan</th>
													</tr>
												</thead>
												</table>
											</div>
											<div class="panel-body" style="margin-top: -15px;">	
															<?php  if($GET_ID_PELANGGAN != ""){
															?>
															<table class="table table-condensed table-hover borderless tableedit" id="cutoff">
															<tbody>
																<tr>
																	<td class="col-md-4"><strong>Periode Tagihan </strong></td>
																	<td>: <?php if($periode != '') echo $periodenamex;
																		else {echo "-";} ?></td>
																</tr>
																<tr>
																	<td class="col-md-4"><strong>Opsi</strong></td>
																	<td>: <a href="#tambah_agen" class="edit_data" style="font-size:15px;" data-toggle="modal" select_hari="<?php echo $periode; ?>" select_name="<?php echo $periodename; ?>" onClick="document.getElementById('id').value=<?php echo "'".$GET_ID_PELANGGAN."'"; ?>;document.getElementById('judul').innerHTML='Edit Pelanggan';document.getElementById('select_agen').innerHTML='<?php echo $GET_NAMA;  ?>';"><i class='glyphicon glyphicon-edit'></i></a>					</td>
																</tr>
																
															</tbody>
															</table>
															
															<hr><?php }  ?>
												<div id="pekerjaan_show_update"></div>
											</div>
										</div>
									</div>
								</div>
								<div class="tab-pane" id="panel_tab3_example2">
									<div class="col-sm-7 col-md-12">
										<div class="paneltosca">
											<div class="panel-heading">
												<table class="table">
													<thead>
														<tr>
															<th>Alamat</th>
														</tr>
													</thead>
												</table>
											</div>
											<div class="panel-body" style="margin-top: -15px;">
													<?php  if($GET_ID_PELANGGAN != ""){
													foreach($queryalamat as $alamatcurrent){ 
													$alamatid = $alamatcurrent->PROFILEALAMAT_ID;
													$alamat = $alamatcurrent->ALAMAT;
													$jalan = $alamatcurrent->JALAN;
													$kodepos = $alamatcurrent->KODEPOS;
													$ket = $alamatcurrent->KETERANGAN;
													$kota = $alamatcurrent->KOTA;
													$propinsiAlamat = $alamatcurrent->PROPINSI;
													$negara = $alamatcurrent->NEGARA;
													$alamatkirim = $alamatcurrent->ALAMATKIRIM;
													$hrpengiriman = $alamatcurrent->HRPENGIRIMAN;
													
													$table_telp = new query('PROFILE', 'PROFILE.PROFILE_ALAMAT A, PROFILE.PROFILE_PHONE P');
													$querytelp = $table_telp->selectBy(
													'P.NAMA, P.TIPE, P.KODEAREA,P.NOMER, P.EXTENTION'
													, 'A.PROFILEALAMAT_ID = P.PROFILEALAMAT_ID
													AND A.PROFILEALAMAT_ID = "'.$alamatid.'"
													AND (P.TGL_SELESAI IS NULL OR P.AKTIF = "Y" OR P.TGL_SELESAI LIKE "%0000%")');
													//echo $querytelp->printquery();
													?>
													<table class="table table-condensed table-hover borderless tableedit" id="alamat">
													<tbody>
														<tr>
															<td width="30%"><strong>Alamat</strong></td>
															<td width="70%">: <?php echo stripslashes($alamat); ?> </td>
														</tr>
														<tr>
															<td><strong>Jalan</strong></td>
															<td>: <?php echo stripslashes($jalan); ?> </td>
														</tr>
														<tr>
															<td><strong>Kota</strong></td>
															<td>: <?php echo stripslashes($kota); ?> </td>
														</tr>
														<tr>
															<td><strong>Propinsi</strong></td>
															<td>: <?php echo stripslashes($propinsiAlamat); ?> </td>
														</tr>
														<tr>
															<td><strong>Negara</strong></td>
															<td>: <?php echo stripslashes($negara); ?> </td>
														</tr>
														<tr>
															<td><strong>Kode Pos</strong></td>
															<td>: <?php echo stripslashes($kodepos); ?> </td>
														</tr>
														<tr>
															<td><strong>Alamat Kirim</strong></td>
															<td>: <?php echo stripslashes($alamatkirim); ?> </td>
														</tr>
														<tr>
															<td><strong>Brp hari Pengiriman</strong></td>
															<td>: <?php echo stripslashes($hrpengiriman)." Hari"; ?> </td>
														</tr>
														<tr>
															<td><strong>Keterangan</strong></td>
															<td>: <?php echo stripslashes($ket); ?> </td>
														</tr>
														
														<tr>
															<td><strong>Telepon</strong></td>
															<td><?PHP foreach($querytelp as $telpcurrent){ 
																	echo ": ".$telpcurrent->TIPE." ".$telpcurrent->NAMA." (".$telpcurrent->KODEAREA.") ".$telpcurrent->NOMER.", Ext. ".$telpcurrent->EXTENTION."<br>"; }?>
															</td>
														</tr>
														<tr style="display:none">
															<td><a href="#" class="hapus_alamat" id="<?php //echo $currentAlamat->ALAMAT_ID;?>"><h6><strong><font color="red">Hapus</font></strong></h6></a></td>
														</tr>
													</tbody>
													</table>
													
													<table class="table table-striped table-bordered table-hover table-full-width" id="data_table2" style="font-size: 12px;display:none" >													
														<tbody>
															<tr>
																<th style="white-space:nowrap;"><strong>Jenis alamat</strong></th>
																<th style="white-space:nowrap;"><strong>Jalan</strong></th>
																<th style="white-space:nowrap;"><strong>Kelurahan</strong></th>
																<th style="white-space:nowrap;"><strong>Kecamatan</strong></th>
																<th style="white-space:nowrap;"><strong>Kode Pos</strong></th>
																<th style="white-space:nowrap;"><strong>Kota</strong></th>
																<th style="white-space:nowrap;"><strong>No Telp</strong></th>
															</tr>
															<tr>
																<td><?php  echo "-"; ?></td>
																<td> <?php echo "-";  ?></td>
																<td><?php echo "-";  ?></td>
																<td><?php echo "-";  ?></td>
																<td><?php echo "-";  ?></td>
																<td><?php echo "-";  ?></td>
																<td><?php echo "-";  ?></td>
																<td><a href="#" class="hapus_alamat" id="<?php //echo $currentAlamat->ALAMAT_ID;?>"><h6><strong><font color="red">Hapus</font></strong></h6></a></td>
															</tr>
														</tbody>
													</table>
													<hr><?php } } ?>
										 	</div>
										</div>
									</div>
									<div class="col-sm-7 col-md-12" style="display:none">
										<div class="panelred">
											<div class="panel-heading">
												<table class="table">
													<thead>
														<tr>
															<th>Kontak</th>
														 </tr>
													</thead>
												</table>
											</div>
											<div class="panel-body">
												<div id="hp_show">
													<table class="table table-striped table-bordered table-hover table-full-width" id="data_table2" style="font-size: 12px;">													
														<tbody>
															<tr>
															<?php /*$count=0;
															foreach($currentHp as $currentHp2){
															$count++;
															}
															if($count>0){*/ ?>
																<th style="white-space:nowrap;"><strong>Kode Negara</strong></th>
																<th style="white-space:nowrap;"><strong>Kode Area</strong></th>
																<th style="white-space:nowrap;"><strong>Nomer</strong></th>
																<th style="white-space:nowrap;"><strong>Extension</strong></th>
																<th style="white-space:nowrap;"><strong>Tipe</strong></th>
																<th style="white-space:nowrap;"><strong>Aktif</strong></th>
																<th style="white-space:nowrap;"><strong>Keterangan</strong></th>
																<?php //}else{ ?>
																<?php //} ?>
															</tr>
															<?php //foreach($currentHp as $currentHp){ ?>
															<tr>
																<td><?php echo "-";  ?></td>
																<td><?php echo "-";  ?></td>
																<td><?php echo "-";  ?></td>
																<td><?php echo "-";  ?></td>
																<td><?php echo "-";  ?></td>
																<td><?php echo "-";  ?></td>
																<td><?php echo "-";  ?></td>
																<td><a href="#" class="hapus_no_hp" id="<?php //echo $currentHp->HP_ID;?>"><h6><strong><font color="red">Hapus</font></strong></h6></a></td>
																	
															</tr>
															<?php //} ?>
														</tbody>
													</table>
												</div>
												<div id="hp_show_update"></div>
											</div>
										</div>
									</div>
								</div>
								<div class="tab-pane" id="panel_tab3_example3">
									<div class="col-sm-7 col-md-12">
										<div class="paneltosca">
											<div class="panel-heading">
												<table class="table">
																<thead>
																	<tr>
																		<th>PIC
																		<?php
																		//if($GET_ID_PELANGGAN != ""){
																		?>
																		<!--<a href="#pekerjaanModal" style="font-size:15px;" class="pull-right" data-toggle="modal"><strong>+ Tambah</strong></a></th> -->
																		<?php
																		//}ELSE{}
																		?>
																		
																	</tr>
																</thead>
												</table></div>
											<div class="panel-body" style="margin-top: -15px;">
													<?php if($GET_ID_PELANGGAN != ""){
													foreach($querypic as $piccurrent){  
													$jabatan = $piccurrent->JABATAN;
													$nik = $piccurrent->NIK;
													$nama = $piccurrent->NAMA;
													$ket = $piccurrent->KETERANGAN; ?>
													<table class="table table-condensed table-hover borderless tableedit" id="pic">
													<tbody>
														<tr>
															<td width="30%"><strong>NIK</strong></td>
															<td width="70%">: <?php echo stripslashes($nik); ?> </td>
														</tr>
														<tr>
															<td width="30%"><strong>PIC</strong></td>
															<td width="70%">: <?php echo stripslashes($nama); ?> </td>
														</tr>
														<tr>
															<td width="30%"><strong>Jabatan</strong></td>
															<td width="70%">: <?php echo stripslashes($jabatan); ?> </td>
														</tr>
														<tr>
															<td width="30%"><strong>Keterangan</strong></td>
															<td width="70%">: <?php echo stripslashes($ket); ?> </td>
														</tr>
														<tr style="display:none">
															<td><a href="#" class="hapus_alamat" id="<?php //echo $currentAlamat->ALAMAT_ID;?>"><h6><strong><font color="red">Hapus</font></strong></h6></a></td>
													</tbody>
													</table>
													<table class="table table-striped table-bordered table-hover table-full-width" id="data_table2" style="font-size: 12px;display:none">	
														<tbody>
															<tr>
																<th style="white-space:nowrap;">NIK</strong></th>
																<th style="white-space:nowrap;">PIC</strong></th>
																<th style="white-space:nowrap;">Jabatan</strong></th>
																<th style="white-space:nowrap;">Tgl Mulai</strong></th>
																<th style="white-space:nowrap;">Tgl Selesai</strong></th>
																<th style="white-space:nowrap;">Keterangan</strong></th>
															</tr>
															<tr>
																<td><?php echo '-';  ?></td>
																<td>-</td>
																<td>-</td>
																<td>-</td>
																<td>-</td>
																<td>-</td>
																<td><a href="#" class="hapus_pekerjaan" id="<?php //echo $currentPekerjaan->RIWAYATPEKERJAAN_ID;?>"><h6><strong><font color="red">Hapus</font></strong></h6></a></td>
															</tr>
														</tbody>
													</table>
															<hr><?php } } ?>
												<div id="pekerjaan_show_update"></div>
											</div>
										</div>
									</div>			
								</div>
								<div class="tab-pane" id="panel_tab3_example6">
									<div class="col-sm-7 col-md-12">
										<div class="paneltosca">
											<div class="panel-heading">
												<table class="table">
													<thead>
														<tr>
															<th>TOP
																<?php //if($GET_ID_PELANGGAN != ""){ ?>
																	<!-- <a href="#tambahTOP" style="font-size:15px;" class="pull-right" data-toggle="modal"><strong>+ Tambah</strong></a></th> -->
																<?php //}ELSE{} ?>
																		
														</tr>
													</thead>
												</table></div>
											<div class="panel-body" style="margin-top: -15px;">	
													<?php if($GET_ID_PELANGGAN != ""){
													foreach($queryTOP as $currentTOP){ 
													$topid = $currentTOP->TOP_ID;
													$kodetransaksi = $currentTOP->TRANSAKSI;
													$top = $currentTOP->TOP." Hari";
													$tglmulai = date("d M Y", strtotime($currentTOP->TGL_MULAI));
													$tglselesai = $currentTOP->TGL_SELESAI;
													if($tglselesai == '' OR $tglselesai == '0000-00-00 00:00:00') $tglselesai = 'Hingga pemberitahuan lebih lanjut';
													else $tglselesai = date("d M Y", strtotime($tglselesai));
													
													/*$table_detailTOP = new query('MARKETING', 'MARKETING.DETAIL D, MARKETING.MERK M');
													$querydetailTOP = $table_detailTOP->selectBy(
													'M.MERK,D.KECUALI'
													, 'D.MASTER_ID = "'.$topid.'" 
													AND M.MERK_ID = D.MERK_ID
													order by D.KECUALI');*/
													$table_detailTOP = new query('MARKETING', 'MARKETING.DETAIL D
														LEFT JOIN GBJ.MERK M ON M.MERK_ID = D.MERK_ID
														LEFT JOIN MASTERGUDANG.MASTER_BARANG B ON B.MASTERBARANG_ID = D.PRODUK_ID');
													$querydetailTOP = $table_detailTOP->selectBy(
													'M.MERK, SUBSTRING(B.NAMA_BARANG,1,LOCATE(" ISI",B.NAMA_BARANG)) AS NAMA_BARANG ,D.KECUALI'
													, 'D.MASTER_ID = "'.$topid.'" 
													order by D.KECUALI');
													//echo $querydetailTOP->printquery();
													$numTOP = $querydetailTOP->num_rows();
													?>
													<table class="table table-condensed table-hover borderless tableedit" id="pic">
													<tbody>
														<tr>
															<td width="30%"><strong>TOP</strong></td>
															<td width="70%">: <?php echo stripslashes($top); ?> </td>
														</tr>
														<tr>
															<td width="30%"><strong>Kode Transaksi</strong></td>
															<td width="70%">: <?php echo stripslashes($kodetransaksi); ?> </td>
														</tr>
														<tr>
															<td width="30%"><strong>Tgl Mulai</strong></td>
															<td width="70%">: <?php echo stripslashes($tglmulai); ?> </td>
														</tr>
														<tr>
															<td width="30%"><strong>Tgl Selesai</strong></td>
															<td width="70%">: <?php echo stripslashes($tglselesai); ?> </td>
														</tr>
														<tr>
															<td width="30%"><strong>Detail</strong></td>
															<td width="70%">
															<?php if($numTOP>0){ 
																foreach($querydetailTOP as $currentdetailTOP){
																	$kecuali = $currentdetailTOP->KECUALI;
																	$merk = $currentdetailTOP->MERK;
																	$namabarang = $currentdetailTOP->NAMA_BARANG;
																	if($merk != ''){																	
																		if($kecuali == 'Y'){	echo "Kecuali Merk: ".$currentdetailTOP->MERK." <br>"; }
																		else if($kecuali == 'T'){echo "Berlaku Merk: ".$currentdetailTOP->MERK." <br>"; }
																	} else if($namabarang != ''){																	
																		if($kecuali == 'Y'){	echo "Kecuali Produk: ".$currentdetailTOP->NAMA_BARANG." <br>"; }
																		else if($kecuali == 'T'){echo "Berlaku Produk: ".$currentdetailTOP->NAMA_BARANG." <br>"; }
																	}
																}
															} else { echo "Berlaku semua merk"; }?>
															</td>
														</tr>
													</tbody>
													</table>
													<table class="table table-striped table-bordered table-hover table-full-width" id="data_table2" style="font-size: 12px;display:none" >
														<tbody>
															<tr>
																<th style="white-space:nowrap;">TOP (Hari)</strong></th>
																<th style="white-space:nowrap;">Kode Transaksi</strong></th>
																<th style="white-space:nowrap;">Tgl Mulai</strong></th>
																<th style="white-space:nowrap;">Tgl Selesai</strong></th>
															</tr>
															<tr>
																<td><?php echo '-';  ?></td>
																<td><?php echo '-';  ?></td>
																<td><?php echo '-';  ?></td>
																<td><?php echo '-';  ?></td>
																<td><a href="#" class="hapus_pekerjaan" id="<?php //echo $currentPekerjaan->RIWAYATPEKERJAAN_ID;?>"><h6><strong><font color="red">Hapus</font></strong></h6></a></td>
															</tr>
															</tbody>
													</table>
													<hr><?php } } ?>
												
												<div id="pekerjaan_show_update"></div>
											</div>
										</div>
									</div>
									<div class="col-sm-7 col-md-12">
										<div class="panelblue">
											<div class="panel-heading">
												<table class="table">
												<thead>
													<tr>
														<th>Limit Kredit</th>
													</tr>
												</thead>
												</table>
											</div>
											<div class="panel-body" style="margin-top: -15px;">
													<?php if($GET_ID_PELANGGAN != ""){
													foreach($querylimit as $currentlimit){ 
													$limitkreditid = $currentlimit->LIMITKREDIT_ID;
													$limitkredit = $currentlimit->LIMITKREDIT;
													if ($limitkredit=='UNLIMITED' or $limitkredit=='') $limitkredit = $limitkredit;
													else $limitkredit = number_format($limitkredit, 2, ',', '.');
													$currency = $currentlimit->CURRENCY;
													$transaksi = $currentlimit->TRANSAKSI;
													$tgl_mulai = date("d M Y", strtotime($currentlimit->TGL_MULAI));
													$tgl_selesai = $currentlimit->TGL_SELESAI;
													if($tgl_selesai == '' OR $tgl_selesai == '0000-00-00 00:00:00') $tgl_selesai = 'Hingga pemberitahuan lebih lanjut';
													else $tgl_selesai = date("d M Y", strtotime($tgl_selesai));
													
													$table_detaillimit = new query('MARKETING', 'MARKETING.DETAIL D, MARKETING.MERK M');
													$querydetaillimit = $table_detaillimit->selectBy(
													'M.MERK, D.KECUALI'
													, 'D.MASTER_ID = "'.$limitkreditid.'" 
													AND M.MERK_ID = D.MERK_ID
													order by D.KECUALI');
													$numlimit = $querydetaillimit->num_rows();
													?>
													<table class="table table-condensed table-hover borderless tableedit" id="pic">
													<tbody>
														<tr>
															<td width="30%"><strong>Limit</strong></td>
															<td width="70%">: <?php echo stripslashes($limitkredit); ?> </td>
														</tr>
														<tr>
															<td width="30%"><strong>Mata uang</strong></td>
															<td width="70%">: <?php echo stripslashes($currency); ?> </td>
														</tr>
														<tr>
															<td width="30%"><strong>Kode Transaksi</strong></td>
															<td width="70%">: <?php echo stripslashes($transaksi); ?> </td>
														</tr>
														<tr>
															<td width="30%"><strong>Tgl Mulai</strong></td>
															<td width="70%">: <?php echo stripslashes($tgl_mulai); ?> </td>
														</tr>
														<tr>
															<td width="30%"><strong>Tgl Selesai</strong></td>
															<td width="70%">: <?php echo stripslashes($tgl_selesai); ?> </td>
														</tr>
														<tr>
															<td width="30%"><strong>Detail</strong></td>
															<td width="70%">
															<?php if($numlimit > 0){
																foreach($querydetaillimit as $currentdetaillimit){
																	$kecuali = $currentdetaillimit->KECUALI;
																	if($kecuali == 'Y'){	echo "Kecuali Merk: ".$currentdetaillimit->MERK." <br>"; }
																	else if($kecuali == 'T'){echo "Berlaku Merk: ".$currentdetaillimit->MERK." <br>"; }
																}
															} else { echo "Berlaku semua merk"; }?>
															
															</td>
														</tr>
													</tbody>
													</table>
													<table class="table table-striped table-bordered table-hover table-full-width" id="data_table2" style="font-size: 12px;display:none">
														<tbody>
															<tr>
																<th style="white-space:nowrap;">Limit</strong></th>
																<th style="white-space:nowrap;">Mata uang</strong></th>
																<th style="white-space:nowrap;">Kode Transaksi</strong></th>
																<th style="white-space:nowrap;">Tgl Mulai</strong></th>
																<th style="white-space:nowrap;">Tgl Selesai</strong></th>
															</tr>
															<tr>
																<td><?php echo '-';  ?></td>
																<td><?php echo '-';  ?></td>
																<td><?php echo '-';  ?></td>
																<td><?php echo '-';  ?></td>
																<td><?php echo '-';  ?></td>
																<td><a href="#" class="hapus_pekerjaan" id="<?php //echo $currentPekerjaan->RIWAYATPEKERJAAN_ID;?>"><h6><strong><font color="red">Hapus</font></strong></h6></a></td>
															</tr>
															</tbody>
													</table>
													<hr><?php } } ?>
												
												<div id="pekerjaan_show_update"></div>
											</div>
										</div>
									</div>
									<div class="col-sm-7 col-md-12" style="display:none">
										<div class="panelred">
											<div class="panel-heading">
												<table class="table">
												<thead>
													<tr>
														<th>PEMBAYARAN</th>
													</tr>
												</thead>
												</table></div>
											<div class="panel-body">
												<div id="pekerjaan_show">	
													<?php //foreach($currentPekerjaan as $currentPekerjaan){ ?>
													<table class="table table-striped table-bordered table-hover table-full-width" id="data_table2" style="font-size: 12px;">
													<tbody>
														<tr>
															<th style="white-space:nowrap;">Cara Bayar</strong></td>
															<th style="white-space:nowrap;">Mata uang</strong></td>
															<th style="white-space:nowrap;">Tagihan</strong></td>
															<th style="white-space:nowrap;">Hari Mulai</strong></td>
															<th style="white-space:nowrap;">jumlah hari</strong></td>
															<th style="white-space:nowrap;">Tgl Mulai</strong></td>
															<th style="white-space:nowrap;">Tgl Selesai</strong></td>
														</tr>
														<tr>
															<td><?php echo '-';  ?></td>
															<td><?php echo '-';  ?></td>
															<td><?php echo '-';  ?></td>
															<td><?php echo '-';  ?></td>
															<td><?php echo '-';  ?></td>
															<td><?php echo '-';  ?></td>
															<td><?php echo '-';  ?></td>
															<td><a href="#" class="hapus_pekerjaan" id="<?php //echo $currentPekerjaan->RIWAYATPEKERJAAN_ID;?>"><h6><strong><font color="red">Hapus</font></strong></h6></a></td>
														</tr>
													</tbody>
													</table>
													<hr><?php //} ?>
												</div>
												<div id="pekerjaan_show_update"></div>
											</div>
										</div>
									</div>	
									<div class="col-sm-7 col-md-12">
										<div class="panelred">
											<div class="panel-heading">
												<table class="table">
																<thead>
																	<tr>
																		<th>Diskon</th>
																		
																	</tr>
																</thead>
												</table></div>
											<div class="panel-body">
												<div id="pekerjaan_show">	
													<?php 
													If($GET_ID_PELANGGAN != ""){
													//foreach($currentPekerjaan as $currentPekerjaan){
													$table_diskon = new query('MARKETING','MARKETING.MASTER_DISKON M,MARKETING.DETAILAREA_KETENTUAN A');
														
													$querydiskon = $table_diskon->selectBy("distinct M.NAMADISKON, M.TGL_MULAI, M.TGL_AKHIR, M.DISKON,M.MASTERDISKON_ID"
														," A.AGEN_ID = '".$GET_ID_PELANGGAN."' 
														AND M.MASTERDISKON_ID = A.MASTER_ID
														AND AKTIF = 'Y'
														AND STATUS = 'VALID'
														ORDER BY M.TGL_AKHIR");
													foreach($querydiskon as $diskoncurrent){
													$tgl_selesai = $diskoncurrent->TGL_AKHIR;
													if($tgl_selesai == '' OR $tgl_selesai == '0000-00-00 00:00:00') $tgl_selesai = 'Hingga pemberitahuan lebih lanjut';
													else $tgl_selesai = date("d M Y", strtotime($tgl_selesai));
														
													$diskon_id = $diskoncurrent->MASTERDISKON_ID;
													$table_detaildis = new query('MARKETING', 'MARKETING.DETAIL D, MARKETING.MERK M');
													$querydetaildis = $table_detaildis->selectBy(
														'M.MERK, D.KECUALI'
														, 'D.MASTER_ID = "'.$diskon_id.'" 
														AND M.MERK_ID = D.MERK_ID
														order by D.KECUALI');
													$numdiskon = $querydetaildis->num_rows();
															
													?>
													<table class="table table-condensed table-hover borderless tableedit" id="pic">
													<tbody>
														<tr>
															<td width="30%"><strong>Nama Diskon</strong></td>
															<td width="70%">: <?php echo stripslashes($diskoncurrent->NAMADISKON); ?> </td>
														</tr>
														<tr>
															<td width="30%"><strong>Diskon</strong></td>
															<td width="70%">: <?php echo stripslashes($diskoncurrent->DISKON); ?> </td>
														</tr>
														<tr>
															<td width="30%"><strong>Tgl Mulai</strong></td>
															<td width="70%">: <?php echo date("d M Y", strtotime($diskoncurrent->TGL_MULAI)); ?> </td>
														</tr>
														<tr>
															<td width="30%"><strong>Tgl Selesai</strong></td>
															<td width="70%">: <?php echo $tgl_selesai; ?> </td>
														</tr>
														<tr>
															<td width="30%"><strong>Detail</strong></td>
															<td width="70%">: 
															<?php if($numdiskon > 0){
																foreach($querydetaildis as $currentdetaildis){
																	$kecuali = $currentdetaildis->KECUALI;
																	if($kecuali = 'Y'){	echo "Kecuali Merk: ".$currentdetaildis->MERK." <br>"; }
																	else if($kecuali = 'T'){echo "Berlaku Merk: ".$currentdetaildis->MERK." <br>"; }
																}
															} else { echo "Berlaku semua merk"; }?>
															</td>
														</tr>
													</tbody>
													</table>
													
													
															<hr><?php } } ?>
													
												</div>
												<div id="pekerjaan_show_update"></div>
											</div>
										</div>
									</div>	
							</div>	
											
						</div>
											<div class="row">
											
       
       
    </div>		
													</div>
</div>
</div>
<?php include "../_template/navbar_footer.php"; ?>
</div><!--/main-->



	<!-- javascript yang dibutuhkan untuk halaman ini saja -->
		
		
		
		<script type="text/javascript" src="/js/DataTables/media/js/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="/js/DataTables/media/js/DT_bootstrap.js"></script>
		<script type="text/javascript" src="/js/jquery-mockjax/jquery.mockjax.js"></script>
		<script type="text/javascript" src="/js/x-editable/js/bootstrap-editable.min.js"></script>
		
		
		<!-- ----------------------------------------------------------------------------- -->
		
		
		<!-- ajax untuk insert data menggunakan _modal-and-datatable.js -->
		
		
		
		<script  src="/js/ajax/_modal-and-datatable.js"></script>
		<!--script>
		var submit = <?php //echo "'"."button#".$submit."'" ;?>;
		var data_form = <?php //echo "'"."#".$data_form."'" ;?>;
		var act = 'ADD_ASURANSI';
		var peringatan = 'Penyimpanan gagal, nama kode / jenis sudah ada';
		</script-->
		
		<!-- ----------------------------------------------------------------------------- -->
		<script>
	
	$(function() {
	
	// Menjalankan plugin DataTables pada id contoh. id contoh merupakan tabel yang kita gunakan untuk menampilkan data
	
	<?php //Datepicker ?>
	<?php //untuk menampilkan div view_data saat awal load ?>
	//$('#view_data').show();
	act = 'AGEN';
	action = 'ADD';
	peringatan = 'Penyimpanan Gagal, Data sudah ada';
	//action = '';
	<?php //peringatan = ''; Digunakan untuk alert ?>
	
	<?php //untuk menampilkan div add_data ?>
	$(".tambah_data").click(function(){
		//$('#view_data').hide(); <?php //Div untuk view data di hide ?>
		//$('.select_komposisi_add').hide();
		//$('#add_data').show();
		//$('#data_token')[0].reset(); <?php //Data form direset ketika user kembali ke halaman tersebut ?>
		act = 'AGEN';
		action = 'ADD';
		peringatan = 'Gagal memasukkan data, refresh halaman / hubungi MIS';
		//var agen_id=$(this).attr('agen_id');
		//var agen_nama=$(this).attr('agen_nama');
		var select_hari=$(this).attr('select_hari');
		//$("#select_agen").html("<option value="+"'"+agen_id+"'"+">"+agen_nama+"</option>"); 
		$("#select_hari").html("<option>"+select_hari+"</option><?php echo $optionhari;?>"); 
	});
	

	<?php //Edit data ?>
	$('#cutoff').on('click','.edit_data',function (){
	//$('#view_data').hide();
	//$('#add_data').show();
	//$('.select_komposisi_add').show(); <?php //Untuk pilihan token aktif atau tidak ?>
	act = 'AGEN';
	action = 'UPDATE'; 
	peringatan = 'Update gagal, Data sudah ada';
	//$('.notification_select_hari').hide();
	//$('.notification_periode_add').hide();
	//var agen_id=$(this).attr('agen_id');
	//var agen_nama=$(this).attr('agen_nama');
	var select_hari=$(this).attr('select_hari');
	var select_name=$(this).attr('select_name');
	//$("#select_agen").html("<option value="+"'"+agen_id+"'"+">"+agen_nama+"</option>"); 
	$("#select_hari").html("<option value="+select_hari+">"+select_name+"</option><?php echo $optionhari;?>"); 
	});
	
	 $(".lihat_data").click(function(){
		   $('#view_data').show();
		   $('#add_data').hide();
		    $(data_form)[0].reset();
	});	
	
	<?php //Ajax untuk ADD, DELETE DAN UPDATE DATA ?>
	
	$('#submit_agen').click(function(){
			/*if($("#select_agen").val()=="") {
						//alert('Field Asuransi tidak boleh kosong!');
						$('.notification_select_agen').show();
						$('.notification_select_agen').html('Pilihan Agen tidak boleh kosong');
						$("#select_agen").focus();
						return false;
					}
			else{
			$('.notification_select_agen').hide();
			}	*/
			//
			/*
			//Validasi field			
			if($("#select_hari").val()=="") {
						//alert('Field Asuransi tidak boleh kosong!');
						$('.notification_select_hari').show();
						$('.notification_select_hari').html('Field Nama Kota tidak boleh kosong');
						$("#select_hari").focus();
						return false;
					}
			else{
			$('.notification_select_hari').hide();
			}	
			//
			
			//Validasi field
			if($("#periode_add").val()=="") {
						//alert('Field periode_add tidak boleh kosong!');
						$('.notification_periode_add').show();
						$('.notification_periode_add').html('Field Keterangan tidak boleh kosong');
						$("#periode_add").focus();
						return false;
					}
			else{
			$('.notification_periode_add').hide();
			}	
			
			if($("#komposisi_add").val()=="") {
						//alert('Field periode_add tidak boleh kosong!');
						$('.notification_komposisi_add').show();
						$('.notification_komposisi_add').html('Field Komposisi PPN tidak boleh kosong');
						$("#komposisi_add").focus();
						return false;
					}
			else{
			$('.notification_komposisi_add').hide();
			}	*/
			//
			if (confirm('Anda yakin ?')) {
		   	$.ajax({
    		   	type: "POST",
			url: "_ADM/crud_master.php?act="+act+"&action="+action,
			data: $('#data_agen').serialize(),
        		success: function(msg){
 		        	if(msg==1){
					alert(peringatan);
					//$('.notification_select_hari').show();
					//$('.notification_select_hari').html('Update gagal, Data sudah ada');
					}else{
						alert("Penyimpanan Sukses");
						window.location.replace(msg);
					}
					
 		        },
			error: function(){
				alert("koneksi bermasalah, silahkan reload halaman");
				
				}
      			});
    
			} else { 
		return false;
		}
	
		});
		
	
	});
		</script>
		
		
	</body>
</html>
<?php
}
else{
header('location:/lock.php?logingagal');
}
?>