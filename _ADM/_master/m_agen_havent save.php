<?php

session_start();

if(!empty($_SESSION['username']) //&& !empty($_SESSION['action']) && ( ($_SESSION['action']) == 'SR' || ($_SESSION['action']) == 'SUPERADMIN' ) 
){
$agen_id = '';
$namaagen = '';
$notagihan = '';
$namapelanggan = '';
$queryagen = "";
$halkategori = "";
$date1 = date("Y-m-d");
$date2 = date("Y-m-d");
IF(isset($_REQUEST['id'])){
	IF($_REQUEST['id'] != ''){
		$agen_id = $_REQUEST['id'];
		$queryagen = "AND P.PROFILE_ID = '".$agen_id."'";
		//$namaagen = $_REQUEST['nama'];
	}
}
if(isset($_GET['halkategori'])){
	$halkategori = "&halkategori=".$_GET['halkategori'];
}
?><?php 
include_once '_conn/query.php';
include_once 'modul/class_paging.php'; //CLASS UNTUK PAGING
$paging = new Paging();
$batas  = 10;
$posisi = $paging->cariPosisi($batas);
$posisi_data = $paging->cariPosisi($batas);

/*$table_Tagihan = new query('FINANCE','TAGIHAN T, PROFILE.PROFILE A'); //('NAMA DATABASE','NAMA TABEL')

$queryTagihan = $table_Tagihan->selectBy("COUNT(T.TAGIHAN_ID) AS NUM"
,"A.PROFILE_ID = T.AGEN_ID ".$queryagen);
$queryTagihancurrent = $queryTagihan->current();
//ECHO $queryTagihan->printquery();
*/

$table_Agen = new query('FINANCE','AGEN A, PROFILE.PROFILE P'); //('NAMA DATABASE','NAMA TABEL')
$queryAgen = $table_Agen->selectBy("COUNT(A.PELANGGAN_ID) AS NUM"
,"P.PROFILE_ID = A.PELANGGAN_ID ".$queryagen);
$queryAgencurrent = $queryAgen->current();
//ECHO $queryAgen->printquery();
?>
<!DOCTYPE html>
<html lang="en">
	<?php include '../_template/head.php'; //TEMPLATE HEADER DAN BERISI CSS GENERAL ?>
	<body id="page-top">
<?php include '../_template/navbar_head.php'; //TEMPLATE MENU YANG ADA PADA HEADER ?>
<?php include '../_template/navbar_sub.php'; //TEMPLATE MENU YANG ADA PADA NAVIGASI BAR?>

<!-- CSS YANG DIBUTUHKAN DI PAGE INI SAJA -->
<link rel="stylesheet" href="/js/DataTables/media/css/DT_bootstrap.css" />
<link rel="stylesheet" href="/css/jquery-ui/jquery-ui.css"> <?php //Datepicker ?>
<script src="/js/jquery/jquery.min.js"></script>
		<script src="/js/jquery-ui/jquery-ui.js"></script>
<script>

$(document).ready(function(){
   $('#srch-term').autocomplete({  
			source: '/ajax/autocompletetagihan.php',
			minLength: 1,
			max: 5,
			select: function( event, ui ) {
			document.getElementById('nama').value = ui.item.NIK;
			document.getElementById('id').value = ui.item.KARYAWAN_ID;
			}
		});
		
});
</script>
<style>
<?php //AGAR TABEL BISA DI SCROLL HORIZONTAL  ?>
.dataTables_wrapper {
	   overflow-x: auto;
	   display:block;
	   
}
</style>

<!------------------------------------------------------------------------->

<!--main-->
<div id="tambah_agen" class="modal fade" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
      		<div class="modal-header">
          		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
          		<h3 class="text-center">Tambah Tagihan</h3>
      		</div>
      		<div class="modal-body">
			<form class="form-horizontal col-md-12 center-block" method="POST" role="form" id="data_karyawan" style="font-size:12px;">
				<div class="form-group">
					<label class="col-sm-3 control-label">Agen</label>
					<div class="col-sm-5">
						<select class="form-control input-sm jenis_kelamin" id="jenis_kelamin_add" name="agen" autocomplete="off">
							<option value="">Pilih</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Hari</label>
					<div class="col-sm-5">
						<select class="form-control input-sm jenis_kelamin" id="jenis_kelamin_add" name="jenis_kelamin_add" autocomplete="off">
							<option value="">Pilih</option>
						</select>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							<label class="col-sm-6 control-label">Periode</label>
							<div class="col-sm-6">
								<input class="form-control input-sm" type="text" name="anak_ke_add" id="anak_ke_add" value="" autocomplete="off">
								<font color = "red"><div class="notification_anak_ke_add" style="display:none;"></div></font>
							</div>
						</div>
					</div>
					<div class="col-sm-5">
						<div class="form-group">
							<label class="col-sm-5 control-label">Minggu</label>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Komposisi PPN</label>
					<div class="col-sm-5">
						<input type="text" id="nik_add" name="nik_add" class="form-control input-sm nik" autocomplete="off" >
					</div>
				</div>
				<div class="form-group">
					  <button class="btn btn-primary btn-lg btn-block" data-dismiss="modal" type="button" id="submit_agen">Save</button>
				</div>
			</form>
			</div>
			<div class="modal-footer">
				<div class="col-md-12">
					<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
				</div>
			</div>
  		</div>
	</div>
</div>
<div class="container" id="main">
	<div class="row">   
	<!-- <?php //BREADCRUMB UNTUK MEMPERLIHATKAN SEDANG BERADA DI MENU APA ?>-->
			<ol class="breadcrumb">
			<li><a href="#">Home</a></li>
			<li><a href="#">Master</a></li>
			<li class="active">Pelanggan</li>
			</ol>
	<!------------------------------------------------------------------------->
		<div class="col-md-12 col-sm-12"> <!-- <?php //lg = large, md = medium, sm = small untuk melihat width nya, ada pada bootstrap.css ?>-->
		  <div class="panelblue"> <?php //style card yang bisa di custom di styles.css ?> 
				<div id="view_data" >
          			<div class="panel-heading" style="margin-bottom: 0px;"> <h3>Master Pelanggan 
					<!--<a href="#" class="tambah_data" style="font-size:15px;"><b style="background-color: #eee;padding: 5px 5px;border-radius: 14px;">+ Tambah data </b></a>-->
					<a href="#tambah_agen" style="font-size:15px;" data-toggle="modal"><strong>+ Tambah data</strong></a></h3></div>	</h3></div>
		   			<div class="panel-body" style="margin-bottom: 0px;">
						<div class="panel panel-default" style="margin-bottom: 0px;">
							<div class="panel-body">
			<?php //----------------------Form search--------------------------//?>	
								<form method="GET" action="m_agen.php" >
								<div style="padding-bottom:5px;border-bottom:1px solid #dadada;">
									<table class="table table-condensed table-hover borderless tableedit" id="biodata">
										<tbody>
											<tr>
												<td class="col-md-4" colspan="4"><strong>Search:</strong></td>
											</tr>
										</tbody>
									</table>
									
								</div>
								<!--<div class="input-group"style="padding-bottom:5px;border-bottom:1px solid #dadada;">-->
								<div class="input-group" style="padding-bottom:5px;border-bottom:1px solid #dadada;">
									<input type="text" class="form-control hint" style="z-index:1;" placeholder="pencarian berdasarkan Nama Pelanggan, Regional, Wilayah, Propinsi, Area" name="srch-term" id="srch-term" data-container="body" data-placement="bottom" data-content="Untuk pencarian data Pelanggan berdasarkan Nama">
									<input type="hidden" name="nama" id="nama" value="TIGA EF, CV">
									<input type="hidden" name="id" id="id" value="02014FB000001">
									<div class="input-group-btn">
										<button class="btn btn-default btn-primary" type="submit"><i class="glyphicon glyphicon-search"></i></button>
									</div>
								</div>
							
								</form>
						
			<?php //-----------------------------------------------------------//?>
			
								<?php //----------------------Table view--------------------------//?>	
								
								<div class="table-responsive">
									<?php 
												/*$queryTagihanx = $table_Tagihan->selectBy("T.TAGIHAN_ID, T.NOTAGIHAN, T.NONOTA,A.NAMA, T.SUBSIDIFIX, T.PRINT, CONCAT(T.TANGGAL, ' ', T.WAKTU) AS TANGGAL"
												,"A.PROFILE_ID = T.AGEN_ID ".$queryagen."
												ORDER BY T.TANGGAL limit $posisi_data,$batas");
												*/
										$queryAgenX = $table_Agen->selectBy("A.PELANGGAN_ID, P.NAMA, A.MINGGUAN, A.PERIODE, A.KOMPOSISI_PPN"
										,"P.PROFILE_ID = A.PELANGGAN_ID ".$queryagen."
										ORDER BY P.NAMA limit $posisi_data,$batas");
										?>
											
												<table class="table table-striped" id="table_custom2">
													<thead>
														<tr style="background-color: #4B8DF8;font-size: 12px;">
															<th class="center"><font color= "white">Nama</font></th>
															<th class="center"><font color= "white">Hari</font></th>
															<th class="center"><font color= "white">Periode</font></th>
															<th class="center"><font color= "white">Komposisi PPN</font></th>
															<th class="center"><font color= "white">Detail</font></th>
														</tr>
													</thead>
													<tbody>
													<?PHP foreach($queryAgenX as $agencurrent){
													$day = array('-', 'Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu');
													//echo $day[1];
													$style = "font-size: 12px";
													if(isset($_REQUEST['idT'])){
														if($_REQUEST['idT'] == $agencurrent->PELANGGAN_ID){ $style = "color:#4B8DF8;font-size: 13px;font-weight: bold";}
													} ?>
													<tr style="<?php echo $style; ?>">
													<td height="21">
														<?php echo $agencurrent->NAMA;  ?>
													</td>
													<td height="21">
														<?php echo $day[$agencurrent->PERIODE];  ?>
													</td>
													<td height="21">
														<?php echo $agencurrent->MINGGUAN;  ?>
													</td>
													<td height="21">
														<?php echo $agencurrent->KOMPOSISI_PPN;  ?>
													</td>
													<td>
														<a href="m_agen.php?idT=<?php echo $agencurrent->PELANGGAN_ID.'&id='.$agen_id.$halkategori; ?>#detail">Detail</a>
													</td>
													</tr>
													<?php } ?>
													</tbody>
												</table>
									<?php
											  $jmldata     =  $queryAgencurrent->NUM;
											  $jmlhalaman  = $paging->jumlahHalaman($jmldata, $batas);
											  $linkHalaman = $paging->navHalaman('m_agen.php?id='.$agen_id.'&',$_GET['halkategori'], $jmlhalaman);
											  $awal = $posisi_data+1;
											  $akhir = $posisi_data+$batas;
											  if($akhir>=$jmldata) $akhir = $jmldata;
											  echo "Showing $awal to $akhir of $jmldata entries"; ?>
											  <ul class="pagination pull-right">
											  <?php
												echo $linkHalaman ; //banyak halaman yang ditampilkan jd tombol paging
											  ?> 
											  </ul>
								</div>			  
								  	<a name="detail"></a>
									<?php  //if(isset($_REQUEST['idT'])){  ?>
									<h4 id="tabs">Detail</h4>
									  <ul class="nav nav-tabs">
										<li class="active"><a href="#A" data-toggle="tab">TOP</a></li>
										<li><a href="#B" data-toggle="tab">Limit Kredit</a></li>
										<li><a href="#C" data-toggle="tab">Diskon</a></li>
									  </ul>
								<div class="tabbable">
									<div class="tab-content">
									  <div class="tab-pane active" id="A">
										<p><!--<a href="#tambah_Dtagihan" style="font-size:15px;right:" data-toggle="modal"><strong>+ Tambah Detail tagihan</strong></a>--></p>
										<p>
										<table class="table table-striped" id="table_custom1">
														<thead>
															<tr style="background-color: #4B8DF8;font-size: 12px;">
																<th class="center"><font color= "white">TOP</font></th>
																<th class="center"><font color= "white">Tgl Mulai</font></th>
																<th class="center"><font color= "white">Tgl Selesai</font></th>
																<th class="center"><font color= "white">Detail</font></th>
															</tr>
														</thead>
													<tbody>
													<?php if(isset($_REQUEST['idT'])){ 
													$idT = $_REQUEST['idT'];
													$table_TOP = new query('MARKETING','MARKETING.TOP T');
													/*$queryTOP = $table_TOP->selectBy("COUNT(T.PELANGGAN_ID) AS NUM"
													,"P.PROFILE_ID = T.PELANGGAN_ID
													AND T.PELANGGAN_ID = '".$idT."' ");
													$queryTOPcurrent = $queryTOP->current();*/
													
													$queryTOPX = $table_TOP->selectBy("T.TOP_ID, T.TOP, T.TOLERANSI, T.TGL_MULAI, T.TGL_SELESAI"
													,"T.PELANGGAN_ID = '".$idT."' ");
													
													foreach($queryTOPX as $TOPcurrent){
													$topid = $TOPcurrent->TOP_D;
													$tglselesai = $TOPcurrent->TGL_SELESAI;
													if($tglselesai == '' OR $tglselesai == '0000-00-00 00:00:00') $tglselesai = 'Hingga pemberitahuan lebih lanjut';
													else $tglselesai = date("d M Y", strtotime($tglselesai));
													
													$table_detailTOP = new query('MARKETING', 'MARKETING.DETAIL D, MARKETING.MERK M');
													$querydetailTOP = $table_detailTOP->selectBy(
													'M.MERK,D.KECUALI'
													, 'D.MASTER_ID = "'.$topid.'" 
													AND M.MERK_ID = D.MERK_ID
													order by D.KECUALI');
													$numTOP = $querydetailTOP->num_rows();
													$style = "font-size: 12px";?>
													<tr style="<?php echo $style; ?>">
													<td>
														<?php echo $TOPcurrent->TOP." Hari";  ?>
													</td>
													<td>
														<?php echo date("d M Y",strtotime($TOPcurrent->TGL_MULAI));  ?>
													</td>
													<td>
														<?php echo $tglselesai;  ?>
													</td>
													<td>
														<?php if($numTOP>0){ 
															foreach($querydetailTOP as $currentdetailTOP){
																$kecuali = $currentdetailTOP->KECUALI;
																if($kecuali = 'Y'){	echo "Kecuali Merk: ".$currentdetailTOP->MERK." <br>"; }
																else if($kecuali = 'T'){echo "Berlaku Merk: ".$currentdetailTOP->MERK." <br>"; }
															}
														}else { echo "Berlaku semua merk"; }?>
													</td>
													</tr>
													<?php } } ?>
													</tbody>
										</table>	
												
												<?php
												  	 /* $jmldata     =  $queryDTagihancurrent->NUM;
													  $jmlhalaman  = $paging->jumlahHalaman($jmldata, $batas);
													  $linkHalaman = $paging->navHalaman('m_agen.php?id='.$agen_id.'&',$_GET['halkategori'], $jmlhalaman);
													  $awal = $posisi_data+1;
													  $akhir = $posisi_data+$batas;
													  if($akhir>=$jmldata) $akhir = $jmldata;
													  
													  echo "Showing $awal to $akhir of $jmldata entries";*/
												 ?>
										</p>
									  </div>
									  <div class="tab-pane" id="B">
										<p>
										<table class="table table-striped" id="table_custom3">
														<thead>
															<tr style="background-color: #4B8DF8;font-size: 12px;">
																<th class="center"><font color= "white">Limit</font></th>
																<th class="center"><font color= "white">Mata uang</font></th>
																<th class="center"><font color= "white">Tgl Mulai</font></th>
																<th class="center"><font color= "white">Tgl Selesai</font></th>
																<th class="center"><font color= "white">Detail</font></th>
															</tr>
														</thead>
														<tbody>
														<?php 
														if(isset($_REQUEST['idT'])){ 
														$idT = $_REQUEST['idT'];
														
														$table_limitkredit = new query('MARKETING','MARKETING.LIMITKREDIT T, MARKETING.CURRENCY C');
														
														$querylimitkredit = $table_limitkredit->selectBy("T.LIMITKREDIT_ID, T.LIMITKREDIT, C.CURRENCY, T.TGL_MULAI, T.TGL_SELESAI"
														," T.PELANGGAN_ID = '".$idT."' 
														AND C.CURRENCY_ID = T.CURRENCY_ID");
														
														foreach($querylimitkredit as $limitkreditcurrent){
														$limitkredit = $limitkreditcurrent->LIMITKREDIT;
														if ($limitkredit=='UNLIMITED' or $limitkredit=='') $limitkredit = $limitkredit;
														else $limitkredit = number_format($limitkredit, 2, ',', '.');
													
														$tgl_selesai = $limitkreditcurrent->TGL_SELESAI;
														if($tgl_selesai == '' OR $tgl_selesai == '0000-00-00 00:00:00') $tgl_selesai = 'Hingga pemberitahuan lebih lanjut';
														else $tgl_selesai = date("d M Y", strtotime($tgl_selesai));
														
														$table_detaillimit = new query('MARKETING', 'MARKETING.DETAIL D, MARKETING.MERK M');
														$querydetaillimit = $table_detaillimit->selectBy(
														'M.MERK, D.KECUALI'
														, 'D.MASTER_ID = "'.$limitkreditid.'" 
														AND M.MERK_ID = D.MERK_ID
														order by D.KECUALI');
														$numlimit = $querydetaillimit->num_rows();
														$style = "font-size: 12px";?>
														<tr style="<?php echo $style; ?>">
														<td>
														<?php echo $limitkredit;  ?>
														</td>
														<td>
															<?php echo $limitkreditcurrent->CURRENCY;  ?>
														</td>
														<td>
															<?php echo date("d M Y",strtotime($limitkreditcurrent->TGL_MULAI));  ?>
														</td>
														<td>
															<?php echo stripslashes($tgl_selesai); ?> 
														</td>
														<td>
															<?php if($numlimit > 0){
																foreach($querydetaillimit as $currentdetaillimit){
																	$kecuali = $currentdetaillimit->KECUALI;
																	if($kecuali = 'Y'){	echo "Kecuali Merk: ".$currentdetaillimit->MERK." <br>"; }
																	else if($kecuali = 'T'){echo "Berlaku Merk: ".$currentdetaillimit->MERK." <br>"; }
																}
															} else { echo "Berlaku semua merk"; }?>
														</td>
														</tr>
														<?php } } ?>
														</tbody>
										</table>
												<?php
												  	  /*$jmldata     =  $queryPotcurrent->NUM;
													  $jmlhalaman  = $paging->jumlahHalaman($jmldata, $batas);
													  $linkHalaman = $paging->navHalaman('m_agen.php?id='.$agen_id.'&',$_GET['halkategori'], $jmlhalaman);
													  $awal = $posisi_data+1;
													  $akhir = $posisi_data+$batas;
													  if($akhir>=$jmldata) $akhir = $jmldata;
													  
													  echo "Showing $awal to $akhir of $jmldata entries";*/
												   ?>
												<?php //} } ?>
										</p>
									  </div>
									  <div class="tab-pane" id="C">
										<p>
											<table class="table table-striped" id="table_custom3">
												<thead>
													<tr style="background-color: #4B8DF8;font-size: 12px;">
														<th class="center"><font color= "white">Nama Diskon</font></th>
														<th class="center"><font color= "white">Diskon</font></th>
														<th class="center"><font color= "white">Tgl Mulai</font></th>
														<th class="center"><font color= "white">Tgl Selesai</font></th>
														<th class="center"><font color= "white">Detail</font></th>
													</tr>
												</thead>
												<tbody>
													<?php if(isset($_REQUEST['idT'])){ 
													$idT = $_REQUEST['idT']; 
													$table_diskon = new query('MARKETING','MARKETING.MASTER_DISKON M,MARKETING.DETAILAREA_KETENTUAN A');
														
														$querydiskon = $table_diskon->selectBy("distinct M.NAMADISKON, M.TGL_MULAI, M.TGL_AKHIR, M.DISKON,M.MASTERDISKON_ID"
														," A.AGEN_ID = '".$idT."' 
														AND M.MASTERDISKON_ID = A.MASTER_ID
														AND AKTIF = 'Y'
														AND STATUS = 'VALID'
														ORDER BY M.TGL_AKHIR");
														foreach($querydiskon as $diskoncurrent){
														$tgl_selesai = $diskoncurrent->TGL_AKHIR;
														if($tgl_selesai == '' OR $tgl_selesai == '0000-00-00 00:00:00') $tgl_selesai = 'Hingga pemberitahuan lebih lanjut';
														else $tgl_selesai = date("d M Y", strtotime($tgl_selesai));
														
														$diskon_id = $diskoncurrent->MASTERDISKON_ID;
														$table_detaildis = new query('MARKETING', 'MARKETING.DETAIL D, MARKETING.MERK M');
														$querydetaildis = $table_detaildis->selectBy(
														'M.MERK, D.KECUALI'
														, 'D.MASTER_ID = "'.$diskon_id.'" 
														AND M.MERK_ID = D.MERK_ID
														order by D.KECUALI');
														$numdiskon = $querydetaildis->num_rows();
														?>
													<TR>
														<td><?php echo $diskoncurrent->NAMADISKON; ?></td>
														<td><?php echo $diskoncurrent->DISKON; ?></td>
														<td><?php echo date("d M Y", strtotime($diskoncurrent->TGL_MULAI)); ?></td>
														<td><?php echo $tgl_selesai; ?></td>
														<td>
															<?php if($numdiskon > 0){
																foreach($querydetaildis as $currentdetaildis){
																	$kecuali = $currentdetaildis->KECUALI;
																	if($kecuali = 'Y'){	echo "Kecuali Merk: ".$currentdetaildis->MERK." <br>"; }
																	else if($kecuali = 'T'){echo "Berlaku Merk: ".$currentdetaildis->MERK." <br>"; }
																}
															} else { echo "Berlaku semua merk"; }?>
														</td>
													</TR>
													<?php } } ?>
												</tbody>
											</table>														
										</p>
									  </div>
									</div>
								</div> <!-- /tabbable --><?php //} ?>
								 			 	
									
							</div>	
    					</div><!--playground-->
    					<br>
    
    					<div class="clearfix"></div>
   					</div>
	
	<?php //Tambah data maupun edit data, di hide terlebih dahulu untuk nanti ditampilkan dengan menggunakan javascript dibawah?>
  			</div>
		</div>
	</div>
<?php include "../_template/navbar_footer.php"; ?>
</div><!--/main-->



	<?php //javascript yang dibutuhkan untuk halaman ini saja ?>
		
		
		
		<script src="/js/jquery-ui/jquery-ui.js"></script>
		<script type="text/javascript" src="/js/DataTables/media/js/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="/js/DataTables/media/js/DT_bootstrap.js"></script>
		
		
		
		<!-- ----------------------------------------------------------------------------- -->
		
		
		<?php //ajax untuk insert data menggunakan _modal-and-datatable.js ?>
		
		
		
		<script  src="/js/ajax/_modal-and-datatable.js"></script>
		
		
		<!-- ----------------------------------------------------------------------------- -->
		
		
		<script>
	
	$(function() {
	
	// Menjalankan plugin DataTables pada id contoh. id contoh merupakan tabel yang kita gunakan untuk menampilkan data
	$('#table_custom1').dataTable({"bSort": false,"bFilter": false,"bInfo": false,"bLengthChange": false,"bPaginate": false,
});
	$('#table_custom2').dataTable({"bSort": false,"bFilter": false,"bInfo": false,"bLengthChange": false,"bPaginate": false,
});
	$('#table_custom3').dataTable({"bSort": false,"bFilter": false,"bInfo": false,"bLengthChange": false,"bPaginate": false,
});
	
	<?php //Datepicker ?>
	$( "#tanggal1" ).datepicker({changeMonth: true, changeYear: true, yearRange: "-60:+10", dateFormat:"yy-mm-dd"});
	$( "#tanggal2" ).datepicker({changeMonth: true, changeYear: true, yearRange: "-60:+10", dateFormat:"yy-mm-dd"});
	$( "#tanggal3" ).datepicker({changeMonth: true, changeYear: true, yearRange: "-60:+10", dateFormat:"yy-mm-dd"});
	
	<?php //untuk menampilkan div view_data saat awal load ?>
	//$('#view_data').show();
	//act = '';
	//action = '';
	<?php //peringatan = ''; Digunakan untuk alert ?>
	
	<?php //untuk menampilkan div add_data ?>
	/*$(".tambah_data").click(function(){
	$('#view_data').hide(); <?php //Div untuk view data di hide ?>
	$('.select_aktif').hide();
	$('#add_data').show();
	$('#data_token')[0].reset(); <?php //Data form direset ketika user kembali ke halaman tersebut ?>
	act = 'TOKEN';*/
	action = 'ADD';
	peringatan = 'Gagal memasukkan data, refresh halaman / hubungi MIS';
	$('.notification_tanggal_lahir_add').hide(); <?php //Notifikasi validasi ?>
	$('.notification_select_posisi').hide();		
	});
	
	<?php //Edit data ?>
	$('#data_table').on('click','.edit_data',function (){
	$('#view_data').hide();
	$('#add_data').show();
	$('.select_aktif').show(); <?php //Untuk pilihan token aktif atau tidak ?>
	act = 'TOKEN';
	action = 'UPDATE'; 
	$('.notification_tanggal_lahir_add').hide();
	$('.notification_select_posisi').hide();	 
	});
	
	 $(".lihat_data").click(function(){
		   $('#view_data').show();
		   $('#add_data').hide();
		    $(data_form)[0].reset();
	});	

	<?php //Ajax untuk ADD, DELETE DAN UPDATE DATA ?>
	
	$('#submit_agen').click(function(){
			
			<?php //Validasi field ?>			
			if($("#agen").val()=="") {
						$('.agen').show();
						$('.agen').html('agen tidak boleh kosong');
						$("#tanggal_lahir_add").focus();
						return false;
					}
			else{
			$('.notification_tanggal_lahir_add').hide();
			}	
			//
			
			<?php //Validasi field ?>
			if($("#select_posisi").val()=="") {
						$('.notification_select_posisi').show();
						$('.notification_select_posisi').html('Posisi tidak boleh kosong');
						$("#select_posisi").focus();
						return false;
					}
			else{
			$('.notification_select_posisi').hide();
			}	
			//
			
			if (confirm('Anda yakin ?')) {
		   	$.ajax({
    		   	type: "POST",
			url: "/_SR/_ADMIN/crud_master.php?act="+act+"&action="+action,
			data: $('#data_token').serialize(),
        		success: function(msg){
 		        	if(msg==1){ <?php //jika dari crud_master tidak sesuai kondisi makan akan di echo 1 yang berarti gagal menyimpan ?>
					alert("Gagal membuat token, silahkan refresh halaman dan coba lagi");
					}else{
					window.location.replace(msg); <?php //reload halaman sesuai url yang di echo di crud_master ?>
					}
					
 		        },
			error: function(){
				alert("koneksi bermasalah, silahkan reload halaman");
				
				}
      			});
    
			} else {
		return false;
		}
	
		});
		
	
	});
		</script>
		
	</body>
</html>
<?php
}
else{
header('location:/lock.php?logingagal');
}
?>