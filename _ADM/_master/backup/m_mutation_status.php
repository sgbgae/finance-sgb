<?php

session_start();

if(!empty($_SESSION['username'])){





?>
<?php 
include_once '_conn/query.php';
$table_mutation_status = new query('HRD','MASTER_STATUS_MUTASI'); //('NAMA DATABASE','NAMA TABEL')
//FORMAT SEPERTI PADA CLASS QUERY, FUNCTION SELECT ('FIELD','WHERE CLAUSE')
$mutation_status = $table_mutation_status->selectBy("STATUS_MUTASI_ID,STATUS_MUTASI,KETERANGAN,AKTIF","SEMBUNYI = 'T' ORDER BY status_mutasi ASC");



?>
<!DOCTYPE html>
<html lang="en">
	<?php include '../_template/head.php';?>
	<body id="page-top">
<?php include '../_template/navbar_head.php';?>
<?php include '../_template/navbar_sub.php';?>

<!-- CSS YANG DIBUTUHKAN DI PAGE INI SAJA -->
<link rel="stylesheet" href="/js/DataTables/media/css/DT_bootstrap.css" />

<!------------------------------------------------------------------------->


<!--main-->
<div class="container" id="main">
	
	<div class="row">   
	
	<!-- BREADCRUMB UNTUK MEMPERLIHATKAN SEDANG BERADA DI MENU APA -->
			<ol class="breadcrumb">
			<li><a href="#">Home</a></li>
			<li><a href="#">Master</a></li>
			<li class="active">Mutation Status</li>
			</ol>
	<!------------------------------------------------------------------------->
	
	
     <div class="col-md-12 col-sm-12"> <!-- lg = large, md = medium, sm = small untuk melihat width nya, ada pada bootstrap.css -->

    	<div class="panelblue">
		<div id="view_data" style="display:none;">
           <div class="panel-heading" style="margin-bottom: 0px;"> <h3>Master Mutation Status <a href="#" class="tambah_data" style="font-size:15px;"><b style="background-color: #eee;padding: 5px 5px;border-radius: 14px;">+ Tambah data </b></a></h3></div>
		   
   			<div class="panel-body" style="margin-bottom: 0px;">
                     
					  
						<div class="panel panel-default" style="margin-bottom: 0px;">
								
								<div class="panel-body">
								
									<table class="table table-striped" id="data_table">
										<thead>
											<tr style="background-color: #4B8DF8;font-size: 12px;">
											
												<th class="center"><font color= "white">Status mutasi</font></th>
												<th class="center"><font color= "white">Keterangan</font></th>
												<th class="center"><font color= "white">Aktif</font></th>
				
												<th class="center"><font color= "white">Opsi</font></th>
											</tr>
										</thead>
										<tbody>
										<?php foreach($mutation_status as $ms){?>
										<tr style="font-size: 12px;">
				
										<td>
										
										<?php echo $ms->STATUS_MUTASI;  ?>
										
										</td>
										<td>
										
											<?php echo $ms->KETERANGAN;  ?>
										
										</td>
										<td>
										
										<?php
										if($ms->AKTIF=='Y'){
										echo "Ya";
										}else{
										echo "Tidak";
										}
										?>
										
										</td>
										
										<td>
									
										
										
										<a href="#" class="edit_data" onClick="document.getElementById('id').value=<?php echo "'".$ms->STATUS_MUTASI_ID ."'"; ?>;document.getElementById('nama_status_mutasi').value=<?php echo "'".$ms->STATUS_MUTASI ."'"; ?>;document.getElementById('keterangan').value=<?php echo "'".$ms->KETERANGAN ."'"; ?>;" ><i class='glyphicon glyphicon-edit'></i></a>	
										<a href="#" class="hapus_data" id=<?php echo "'".$ms->STATUS_MUTASI_ID ."'"; ?> ><font color='red'><i class='glyphicon glyphicon-trash'></i></font></a>	
										
										
										
										
										
										
										</td>
										</tr>
										<?php }?>
										</tbody>
									</table>
									
								</div>
							</div>			 
       
       
    </div><!--playground-->
    
    <br>
    
    <div class="clearfix"></div>
    </div>
	<div id="add_data" style="display:none;">
           <div class="panel-heading"> <h3>Master Mutation Status <a href="#" class="lihat_data" style="font-size:15px;"><b style="background-color: #eee;padding: 5px 5px;border-radius: 14px;">&nbsp; Lihat data </b></a></h3></div>
		   
   			
                      <div class="panel-body" style="margin-bottom: 0px;">
								  <div class="col-sm-5">
								
								  <form action="#" role="form" id="data_ms" >
									<input type="hidden" class="form-control" id="id" name="id" autocomplete="off">
									
									<div class="form-group">
									<label>
												Nama status mutasi
											</label>
											
									 <input type="text" class="form-control" id="nama_status_mutasi" name="nama_status_mutasi" placeholder="Status mutasi" autocomplete="off">
										<font color = "red"><div class="notification_nama_status_mutasi" style="display:none;"></div></font>
									</div>
									
											
										
									<div class="form-group">
									<label>
												Keterangan
											</label>
											
									  <input type="text" class="form-control" id="keterangan" name="keterangan" placeholder="Keterangan" autocomplete="off" required>
									<font color = "red"><div class="notification_keterangan" style="display:none;"></div></font>
									
									</div>
									
									<div class="form-group select_aktif" style="display:none;">
									<label>
												Status
											</label>
											<div class="form-group">
											<div class="row">
											<div class="col-sm-3">		
											<select class="form-control " id="aktif" name="aktif" >
											<option value="Y">AKTIF</option>
											<option value="T">TIDAK AKTIF</option>
											</select>
										</div>
										</div></div>
									</div>
									
									<div class="form-group">
									<div class="col-sm-12">
									<label class="col-sm-3">
									&nbsp;
									</label>
										<div class="row">
											<div class="pull-right">		
									  <button class="btn btn-primary btn-md" type="button" id="submit_ms">Simpan</button>
									  
									
									
									  <button class="btn btn-danger btn-md lihat_data" type="button">Kembali</button>
									  
									</div>
									  </div>
									 </div>
									  </div>
								  </form>
									
								</div>
								 
       
       
    </div><!--playground-->
  
    
    <br>
    
    <div class="clearfix"></div>
    </div>
  </div>
</div>
</div>

<?php include "../_template/navbar_footer.php"; ?>
</div><!--/main-->



	<!-- javascript yang dibutuhkan untuk halaman ini saja -->
		
		
		
		<script type="text/javascript" src="/js/DataTables/media/js/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="/js/DataTables/media/js/DT_bootstrap.js"></script>
		
		
		
		<!-- ----------------------------------------------------------------------------- -->
		
		
		<!-- ajax untuk insert data menggunakan _modal-and-datatable.js -->
		
		
		
		<script  src="/js/ajax/_modal-and-datatable.js"></script>
		<!--script>
		var submit = <?php //echo "'"."button#".$submit."'" ;?>;
		var data_form = <?php //echo "'"."#".$data_form."'" ;?>;
		var act = 'ADD_ASURANSI';
		var peringatan = 'Penyimpanan gagal, nama kode / jenis sudah ada';
		</script-->
		
		<!-- ----------------------------------------------------------------------------- -->
		
		<script>
	
	$(function() {
	
	//untuk menampilkan div view_data saat awal load
	$('#view_data').show();
	act = '';
	action = '';
	peringatan = '';
	 //untuk menampilkan div add_data
	$(".tambah_data").click(function(){
	$('#view_data').hide();
	$('.select_aktif').hide();
	$('#add_data').show();
	$('#data_ms')[0].reset();
	act = 'STATUS_MUTASI';
	action = 'ADD';
	peringatan = 'Update gagal, nama sudah ada';
	$('.notification_nama_status_mutasi').hide();
	$('.notification_keterangan').hide();		
	});
	
	$('#data_table').on('click','.edit_data',function (){
	$('#view_data').hide();
	$('#add_data').show();
	$('.select_aktif').show();
	act = 'STATUS_MUTASI';
	action = 'UPDATE';
	peringatan = 'Update gagal, nama sudah ada';
	$('.notification_nama_status_mutasi').hide();
	$('.notification_keterangan').hide();  
	});
	
	$('#data_table').on('click','.hapus_data',function (){
		var del_id= $(this).attr('id');
		act = 'STATUS_MUTASI';
		action = 'DELETE';
		if (confirm('Anda yakin ?')) {
		   	jQuery.ajax({
			
    		   	type: "POST",
			url: "_ADM/crud_master.php?act="+act+"&action="+action,
			 data:{'id':del_id},
        		success: function(msg){
 	          		 location.reload();
 		        },
			error: function(){
				alert("failure");
				
				}
      			});
			} else {
		return false;
		}	
	});
	
	 $(".lihat_data").click(function(){
		   $('#view_data').show();
		   $('#add_data').hide();
		    $(data_form)[0].reset();
	});	

	/** Ajax untuk ADD, DELETE DAN UPDATE DATA **/
	
	$('#submit_ms').click(function(){
			
			//Validasi field			
			if($("#nama_status_mutasi").val()=="") {
						//alert('Field Asuransi tidak boleh kosong!');
						$('.notification_nama_status_mutasi').show();
						$('.notification_nama_status_mutasi').html('Field Nama status mutasi tidak boleh kosong');
						$("#nama_status_mutasi").focus();
						return false;
					}
			else{
			$('.notification_nama_status_mutasi').hide();
			}	
			//
			
			//Validasi field
			if($("#keterangan").val()=="") {
						//alert('Field keterangan tidak boleh kosong!');
						$('.notification_keterangan').show();
						$('.notification_keterangan').html('Field Keterangan tidak boleh kosong');
						$("#keterangan").focus();
						return false;
					}
			else{
			$('.notification_keterangan').hide();
			}	
			//
			if (confirm('Anda yakin ?')) {
		   	$.ajax({
    		   	type: "POST",
			url: "_ADM/crud_master.php?act="+act+"&action="+action,
			data: $('#data_ms').serialize(),
        		success: function(msg){
 		        	if(msg==1){
					alert(peringatan);
					$('.notification_nama_status_mutasi').show();
					$('.notification_nama_status_mutasi').html('Update gagal, nama sudah ada');
					}else{
					window.location.replace(msg);
					
					}
					
 		        },
			error: function(){
				alert("koneksi bermasalah, silahkan reload halaman");
				
				}
      			});
    
			} else {
		return false;
		}
	
		});
		
	
	});
		</script>
		
	</body>
</html>
<?php
}
else{
header('location:/lock.php?logingagal');
}
?>