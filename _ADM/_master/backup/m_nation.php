<?php

session_start();

if(!empty($_SESSION['username'])){





?>
<?php 
include_once '_conn/query.php';
$table_NEGARA = new query('HRD','MASTER_NEGARA'); //('NAMA DATABASE','NAMA TABEL')
//FORMAT SEPERTI PADA CLASS QUERY, FUNCTION SELECT ('FIELD','WHERE CLAUSE')
$NEGARA = $table_NEGARA->selectBy("NEGARA_ID,NEGARA,KETERANGAN,AKTIF","SEMBUNYI = 'T' ORDER BY NEGARA ASC");
$table_prov= new query('HRD','MASTER_PROVINSI'); //('NAMA DATABASE','NAMA TABEL')


?>
<!DOCTYPE html>
<html lang="en">
	<?php include '../_template/head.php';?>
	<body id="page-top">
<?php include '../_template/navbar_head.php';?>
<?php include '../_template/navbar_sub.php';?>

<!-- CSS YANG DIBUTUHKAN DI PAGE INI SAJA -->
<link rel="stylesheet" href="/js/DataTables/media/css/DT_bootstrap.css" />

<!------------------------------------------------------------------------->


<!--main-->
<div class="container" id="main">
	
	<div class="row">   
	
	<!-- BREADCRUMB UNTUK MEMPERLIHATKAN SEDANG BERADA DI MENU APA -->
			<ol class="breadcrumb">
			<li><a href="#">Home</a></li>
			<li><a href="#">Master</a></li>
			<li><a href="#">Location</a></li>
			<li class="active">Negara</li>
			</ol>
	<!------------------------------------------------------------------------->
	
	
     <div class="col-md-12 col-sm-12"> <!-- lg = large, md = medium, sm = small untuk melihat width nya, ada pada bootstrap.css -->

    	<div class="panelblue">
		<div id="view_data" style="display:none;">
           <div class="panel-heading" style="margin-bottom: 0px;"> <h3>Master Negara <a href="#" class="tambah_data" style="font-size:15px;"><b style="background-color: #eee;padding: 5px 5px;border-radius: 14px;">+ Tambah data </b></a></h3></div>
		   
   			<div class="panel-body" style="margin-bottom: 0px;">
                     
					  
						<div class="panel panel-default" style="margin-bottom: 0px;">
								
								<div class="panel-body">
								
									<table class="table table-striped" id="data_table">
										<thead>
											<tr style="background-color: #4B8DF8;font-size: 12px;">
											
												<th class="center"><font color= "white">Negara</font></th>
												<th class="center"><font color= "white">Keterangan</font></th>
												<th class="center"><font color= "white">Aktif</font></th>
				
												<th class="center"><font color= "white">Opsi</font></th>
											</tr>
										</thead>
										<tbody>
										<?php foreach($NEGARA as $NEGARA){?>
										<tr style="font-size: 12px;">
				
										<td>
										
										<?php echo $NEGARA->NEGARA;  ?>
										
										</td>
										<td>
										
											<?php echo $NEGARA->KETERANGAN;  ?>
										
										</td>
										<td>
										
										<?php
										if($NEGARA->AKTIF=='Y'){
										echo "Ya";
										}else{
										echo "Tidak";
										}
										?>
										
										</td>
										
										<td>
										<?php
										///CEK ID TELAH DITRANSAKSIKAN BELUM
										
										
										$currentProv= $table_prov->findBy('NEGARA_ID','NEGARA_ID',$NEGARA->NEGARA_ID);
										$currentProv = $currentProv->current();
										
											if($currentProv == '' ){ ?>
										<a href="#" class="edit_data" onClick="document.getElementById('id').value=<?php echo "'".$NEGARA->NEGARA_ID ."'"; ?>;document.getElementById('nama_negara').value=<?php echo "'".$NEGARA->NEGARA ."'"; ?>;document.getElementById('keterangan').value=<?php echo "'".$NEGARA->KETERANGAN ."'"; ?>;" ><i class='glyphicon glyphicon-edit'></i></a>	
										<a href="#" class="hapus_data" id=<?php echo "'".$NEGARA->NEGARA_ID ."'"; ?> ><font color='red'><i class='glyphicon glyphicon-trash'></i></font></a>	
										
										
										
										<?php }else{ 
										echo "&nbsp;";
										}?>
										
										
										
										</td>
										</tr>
										<?php }?>
										</tbody>
									</table>
									
								</div>
							</div>			 
       
       
    </div><!--playground-->
    
    <br>
    
    <div class="clearfix"></div>
    </div>
	<div id="add_data" style="display:none;">
           <div class="panel-heading"> <h3>Master Negara <a href="#" class="lihat_data" style="font-size:15px;"><b style="background-color: #eee;padding: 5px 5px;border-radius: 14px;">&nbsp; Lihat data </b></a></h3></div>
		   
   			
                      <div class="panel-body" style="margin-bottom: 0px;">
								  <div class="col-sm-5">
								
								  <form action="#" role="form" id="data_negara" >
									<input type="hidden" class="form-control" id="id" name="id" autocomplete="off">
									
									<div class="form-group">
									<label>
												Nama negara
											</label>
											
									 <input type="text" class="form-control" id="nama_negara" name="nama_negara" placeholder="Nama negara" autocomplete="off">
										<font color = "red"><div class="notification_nama_negara" style="display:none;"></div></font>
									</div>
									
											
										
									<div class="form-group">
									<label>
												Keterangan
											</label>
											
									  <input type="text" class="form-control" id="keterangan" name="keterangan" placeholder="Keterangan" autocomplete="off" required>
									<font color = "red"><div class="notification_keterangan" style="display:none;"></div></font>
									
									</div>
									
									<div class="form-group select_aktif" style="display:none;">
									<label>
												Status
											</label>
											<div class="form-group">
											<div class="row">
											<div class="col-sm-3">		
											<select class="form-control " id="aktif" name="aktif" >
											<option value="Y">AKTIF</option>
											<option value="T">TIDAK AKTIF</option>
											</select>
										</div>
										</div></div>
									</div>
									
									<div class="form-group">
									<div class="col-sm-12">
									<label class="col-sm-3">
									&nbsp;
									</label>
										<div class="row">
											<div class="pull-right">		
									  <button class="btn btn-primary btn-md" type="button" id="submit_negara">Simpan</button>
									  
									
									
									  <button class="btn btn-danger btn-md lihat_data" type="button">Kembali</button>
									  
									</div>
									  </div>
									 </div>
									  </div>
								  </form>
									
								</div>
								 
       
       
    </div><!--playground-->
  
    
    <br>
    
    <div class="clearfix"></div>
    </div>
  </div>
</div>
</div>

<?php include "../_template/navbar_footer.php"; ?>
</div><!--/main-->



	<!-- javascript yang dibutuhkan untuk halaman ini saja -->
		
		
		
		<script type="text/javascript" src="/js/DataTables/media/js/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="/js/DataTables/media/js/DT_bootstrap.js"></script>
		
		
		
		<!-- ----------------------------------------------------------------------------- -->
		
		
		<!-- ajax untuk insert data menggunakan _modal-and-datatable.js -->
		
		
		
		<script  src="/js/ajax/_modal-and-datatable.js"></script>
		<!--script>
		var submit = <?php //echo "'"."button#".$submit."'" ;?>;
		var data_form = <?php //echo "'"."#".$data_form."'" ;?>;
		var act = 'ADD_ASURANSI';
		var peringatan = 'Penyimpanan gagal, nama kode / jenis sudah ada';
		</script-->
		
		<!-- ----------------------------------------------------------------------------- -->
		
		<script>
	
	$(function() {
	
	//untuk menampilkan div view_data saat awal load
	$('#view_data').show();
	act = '';
	action = '';
	peringatan = '';
	 //untuk menampilkan div add_data
	$(".tambah_data").click(function(){
	$('#view_data').hide();
	$('.select_aktif').hide();
	$('#add_data').show();
	$('#data_negara')[0].reset();
	act = 'NEGARA';
	action = 'ADD';
	peringatan = 'Update gagal, nama sudah ada';
	$('.notification_keterangan').hide();	
	$('.notification_nama_negara').hide();	
	});
	
	$('#data_table').on('click','.edit_data',function (){
	$('#view_data').hide();
	$('#add_data').show();
	$('.select_aktif').show();
	act = 'NEGARA';
	action = 'UPDATE';
	peringatan = 'Update gagal, nama sudah ada';
	$('.notification_keterangan').hide();	
	$('.notification_nama_negara').hide();	  
	});
	
	$('#data_table').on('click','.hapus_data',function (){
		var del_id= $(this).attr('id');
		act = 'NEGARA';
		action = 'DELETE';
		if (confirm('Anda yakin ?')) {
		   	jQuery.ajax({
			
    		   	type: "POST",
			url: "_ADM/crud_master.php?act="+act+"&action="+action,
			 data:{'id':del_id},
        		success: function(msg){
 	          		 location.reload();
 		        },
			error: function(){
				alert("failure");
				
				}
      			});
		} else {
		return false;
		}			
	});
	
	 $(".lihat_data").click(function(){
		   $('#view_data').show();
		   $('#add_data').hide();
		    $(data_form)[0].reset();
	});	

	/** Ajax untuk ADD, DELETE DAN UPDATE DATA **/
	
	$('#submit_negara').click(function(){
			
			//Validasi field			
			if($("#nama_negara").val()=="") {
						//alert('Field Asuransi tidak boleh kosong!');
						$('.notification_nama_negara').show();
						$('.notification_nama_negara').html('Field Nama negara tidak boleh kosong');
						$("#nama_negara").focus();
						return false;
					}
			else{
			$('.notification_nama_negara').hide();
			}	
			//
			
			//Validasi field
			if($("#keterangan").val()=="") {
						//alert('Field keterangan tidak boleh kosong!');
						$('.notification_keterangan').show();
						$('.notification_keterangan').html('Field Keterangan tidak boleh kosong');
						$("#keterangan").focus();
						return false;
					}
			else{
			$('.notification_keterangan').hide();
			}	
			//
			if (confirm('Anda yakin ?')) {
		   	$.ajax({
    		   	type: "POST",
			url: "_ADM/crud_master.php?act="+act+"&action="+action,
			data: $('#data_negara').serialize(),
        		success: function(msg){
 		        	if(msg==1){
					alert(peringatan);
					$('.notification_nama_negara').show();
					$('.notification_nama_negara').html('Update gagal, nama sudah ada');
					}else{
					window.location.replace(msg);
					
					}
					
 		        },
			error: function(){
				alert("Koneksi bermasalah, silahkan reload halaman");
				
				}
      			});
    
			} else {
		return false;
		}
	
		});
		
	
	});
		</script>
		
	</body>
</html>
<?php
}
else{
header('location:/lock.php?logingagal');
}
?>