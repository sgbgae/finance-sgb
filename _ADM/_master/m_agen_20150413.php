<?php
session_start();

if(!empty($_SESSION['username'])  //&& !empty($_SESSION['action']) && ( ($_SESSION['action']) == 'SR' || ($_SESSION['action']) == 'SUPERADMIN' ) 
){
$agen_id = '';
$namaagen = '';
$notagihan = '';
$namapelanggan = '';
$queryagen = "";
$halkategori = "";
$date1 = date("Y-m-d");
$date2 = date("Y-m-d");
IF(isset($_REQUEST['id'])){
	IF($_REQUEST['id'] != ''){
		$agen_id = $_REQUEST['id'];
		$queryagen = "AND P.PROFILE_ID = '".$agen_id."' ";
		//$namaagen = $_REQUEST['nama'];
	} else {
		$queryagen = "AND P.NAMA LIKE '%".$_REQUEST['srch-term']."%'";
	}
} 
if(isset($_GET['halkategori'])){
	$halkategori = "&halkategori=".$_GET['halkategori'];
}
?><?php 
include_once '_conn/query.php';
include_once 'modul/class_paging.php'; //CLASS UNTUK PAGING
include_once "../_crud/function.php";
$queryfinance = new queryfinance();
$paging = new Paging();
$batas  = 10;
$posisi = $paging->cariPosisi($batas);
$posisi_data = $paging->cariPosisi($batas);

$arrayagenarea = $queryfinance->agenarea();
$table_Agen = new query('FINANCE',$arrayagenarea['from']); //('NAMA DATABASE','NAMA TABEL')
$queryAgen = $table_Agen->selectBy("COUNT(P.PROFILE_ID) AS NUM"
,'1 '.$arrayagenarea['where'].' '.$queryagen);
$queryAgencurrent = $queryAgen->current();
//ECHO $queryAgen->printquery();
$day = array('', 'Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu');
$optionhari = '';	
$daysx = '';											//echo $day[1];
for($i=0;$i<=7;$i++){
	if($day[$i] != ''){
		$optionhari .= '<option value='.$i.'>'.$day[$i].'</option>';
	} else if($day[$i] == ''){
		$optionhari .= '<option value='.$daysx.'>'.$day[$i].'</option>';
	}
}
?>
<!DOCTYPE html>
<html lang="en">
	<?php include '../_template/head.php'; //TEMPLATE HEADER DAN BERISI CSS GENERAL ?>
	<body id="page-top">
<?php include '../_template/navbar_head.php'; //TEMPLATE MENU YANG ADA PADA HEADER ?>
<?php include '../_template/navbar_sub.php'; //TEMPLATE MENU YANG ADA PADA NAVIGASI BAR?>
<?php
//akses halaman
if(in_array("MASTER_PELANGGAN", $aksesuser) == false){
echo "<script>";
echo "window.location = '/index_ADM.php';";
echo "</script>";
}
?>
<!-- CSS YANG DIBUTUHKAN DI PAGE INI SAJA -->
<link rel="stylesheet" href="/js/DataTables/media/css/DT_bootstrap.css" />
<link rel="stylesheet" href="/css/jquery-ui/jquery-ui.css"> <?php //Datepicker ?>
<script src="/js/jquery/jquery.min.js"></script>
		<script src="/js/jquery-ui/jquery-ui.js"></script>
<script>

$(document).ready(function(){
   $('#srch-term').autocomplete({  
			source: '/ajax/autocompletetagihan.php',
			minLength: 1,
			max: 5,
			select: function( event, ui ) {
			document.getElementById('nama').value = ui.item.NIK;
			document.getElementById('id').value = ui.item.KARYAWAN_ID;
			}
		});
		
});
</script>
<style>
<?php //AGAR TABEL BISA DI SCROLL HORIZONTAL  ?>
.dataTables_wrapper {
	   overflow-x: auto;
	   display:block;
	   
}
</style>

<!------------------------------------------------------------------------->

<!--main-->
<div id="tambah_agen" class="modal fade" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
      		<div class="modal-header">
          		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
          		<h3 class="text-center"><div id="judul">Pelanggan</div></h3>
      		</div>
      		<div class="modal-body">
			<form action="#" role="form" id="data_agen" style="font-size:12px;" class="form-horizontal col-md-12 center-block" method="POST">
				<div class="form-group">
					<label class="col-sm-3 control-label">Nama Pelanggan </label>
					<div class="col-sm-5 control-label">
						<input type="hidden" class="form-control" id="id" name="id" autocomplete="off">
						<strong><div id="select_agen"  align="left"></div></strong>
						<!--<select class="form-control" id="select_agen" name="select_agen" autocomplete="off">
							<option value="">Pilih</option>
							<option value="x">x</option>
						</select>-->
					</div>
				</div>
				<div class="form-group"> 
					<label class="col-sm-3 control-label">Hari</label>
					<div class="col-sm-5">
						<select class="form-control" id="select_hari" name="select_hari" autocomplete="off">
						</select>
						<font color = "red"><div class="notification_select_hari" style="display:none;"></div></font>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							<label class="col-sm-6 control-label">Periode</label>
							<div class="col-sm-6">
								<input class="form-control" type="text" name="periode_add" id="periode_add" value="" autocomplete="off">
							</div>
						</div>
					</div>
					<div class="col-sm-5">
						<div class="form-group">
							<label class="col-sm-5 control-label">Minggu</label>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Komposisi PPN</label>
					<div class="col-sm-5">
						<input type="text" id="komposisi_add" name="komposisi_add" class="form-control" autocomplete="off" >
					</div>
				</div>
				<div class="form-group">
					  <button class="btn btn-primary btn-lg btn-block" data-dismiss="modal" type="button" id="submit_agen">Save</button>
				</div>
			</form>
			</div>
			<div class="modal-footer">
				<div class="col-md-12">
					<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
				</div>
			</div>
  		</div>
	</div>
</div>
<div class="container" id="main">
	<div class="row">   
	<!-- <?php //BREADCRUMB UNTUK MEMPERLIHATKAN SEDANG BERADA DI MENU APA ?>-->
			<ol class="breadcrumb">
			<li><a href="#">Home</a></li>
			<li><a href="#">Master</a></li>
			<li class="active">Pelanggan</li>
			</ol>
	<!------------------------------------------------------------------------->
		<div class="col-md-12 col-sm-12"> <!-- <?php //lg = large, md = medium, sm = small untuk melihat width nya, ada pada bootstrap.css ?>-->
		  <div class="panelblue"> <?php //style card yang bisa di custom di styles.css ?> 
				<div id="view_data" >
          			<div class="panel-heading" style="margin-bottom: 0px;"> <h3>Master Pelanggan 
					<!--<a href="#" class="tambah_data" style="font-size:15px;"><b style="background-color: #eee;padding: 5px 5px;border-radius: 14px;">+ Tambah data </b></a>
					<a href="#tambah_agen" class="tambah_data" style="font-size:15px;" data-toggle="modal" agen_id="" agen_nama="Pilih Agen" select_hari="Pilih Hari" onClick="document.getElementById('id').value='';document.getElementById('periode_add').value='';document.getElementById('komposisi_add').value='';document.getElementById('judul').innerHTML='Tambah Pelanggan';"><strong>+ Tambah data</strong></a>-->
					</h3></div>	</h3></div>
		   			<div class="panel-body" style="margin-bottom: 0px;">
						<div class="panel panel-default" style="margin-bottom: 0px;">
							<div class="panel-body">
			<?php //----------------------Form search--------------------------//?>	
								<form method="GET" action="m_agen.php" >
								<div style="padding-bottom:5px;border-bottom:1px solid #dadada;">
									<table class="table table-condensed table-hover borderless tableedit" id="biodata">
										<tbody>
											<tr>
												<td class="col-md-4" colspan="4"><strong>Search:</strong></td>
											</tr>
										</tbody>
									</table>
									
								</div>
								<!--<div class="input-group"style="padding-bottom:5px;border-bottom:1px solid #dadada;">-->
								<div class="input-group" style="padding-bottom:5px;border-bottom:1px solid #dadada;">
									<input type="text" class="form-control hint" style="z-index:1;" placeholder="pencarian berdasarkan Nama Pelanggan" name="srch-term" id="srch-term" data-container="body" data-placement="bottom" data-content="Untuk pencarian data Pelanggan berdasarkan Nama">
									<input type="hidden" name="nama" id="nama" value="">
									<input type="hidden" name="id" id="id" value="">
									<div class="input-group-btn">
										<button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
										<?php if($queryagen != ''){ ?>
										<a class="btn btn-default" href="m_agen.php" title="Kembali"><i class="fa fa-refresh"></i></a>
										<?php } ?>
									</div>
								</div>
							
								</form>
						
			<?php //-----------------------------------------------------------//?>
			
								<?php //----------------------Table view--------------------------//?>	
								
								<div class="table-responsive">
									<?php 
										//$queryAgenX = $table_Agen->selectBy("A.PELANGGAN_ID, P.NAMA, A.MINGGUAN, A.PERIODE, A.KOMPOSISI_PPN"
										$queryAgenX = $table_Agen->selectBy("P.PROFILE_ID, CONCAT(P.NAMA,' - ',M.AREA) AS NAMA"
										,"P.AKTIF = 'Y' ".$arrayagenarea['where'].' '.$queryagen."
										ORDER BY P.NAMA limit $posisi_data,$batas");
										?>
											
												<table class="table table-striped" id="table_custom2">
													<thead>
														<tr style="background-color: #4B8DF8;font-size: 12px;">
															<th class="center"><font color= "white">Nama Pelanggan - Area</font></th>
															<th class="center"><font color= "white">Hari</font></th>
															<th class="center"><font color= "white">Periode</font></th>
															<th class="center"><font color= "white">Komposisi PPN</font></th>
															<th class="center"><font color= "white">Detail</font></th>
															<th class="center"><font color= "white">Opsi</font></th>
														</tr>
													</thead>
													<tbody>
													<?PHP $namaT = '';
													foreach($queryAgenX as $agencurrent){
													$table_ketentuan = new query('FINANCE','AGEN A'); //('NAMA DATABASE','NAMA TABEL')
													$queryketentuan = $table_ketentuan->selectBy("A.PELANGGAN_ID, A.MINGGUAN, A.PERIODE, A.KOMPOSISI_PPN"
													,"A.PELANGGAN_ID = '".$agencurrent->PROFILE_ID."' ");
													$currentketentuan = $queryketentuan->current();
													if($queryketentuan->num_rows() > 0){
														if($currentketentuan->PERIODE>=0 and $currentketentuan->PERIODE != '-' and $currentketentuan->PERIODE != ''){
															$periode = $currentketentuan->PERIODE;
															$periodename = $day[$currentketentuan->PERIODE];
															$mingguan = $currentketentuan->MINGGUAN;
															$komposisippn = $currentketentuan->KOMPOSISI_PPN;
														} else { 
															$periode = '';
															$periodename = 'Pilih Hari';
															$mingguan = $currentketentuan->MINGGUAN;
															$komposisippn = $currentketentuan->KOMPOSISI_PPN;
														} 
													} else { 
														$periode = '';
														$periodename = 'Pilih Hari';
														$mingguan = '';
														$komposisippn = '';
													}
													$style = "font-size: 12px";
													
													if(isset($_REQUEST['idT'])){ 
														if($_REQUEST['idT'] == $agencurrent->PROFILE_ID){ echo "test";
															$style = "color:#4B8DF8;font-size: 13px;font-weight: bold";
															$namaT = $agencurrent->NAMA;
														} 
													} ?>
													<tr style="<?php echo $style; ?>">
													<td height="21">
														<?php echo $agencurrent->NAMA;  ?>
													</td>
													<td height="21">
														<?php if($periode != '') echo $periodename;
															else {echo "-";} ?>
													</td>
													<td height="21">
														<?php 
														if($mingguan != '') echo $mingguan;
														else {echo "-";}  ?>
													</td>
													<td height="21">
														<?php 
														if($komposisippn != '' and $komposisippn != 0) echo $komposisippn;
														else {echo "-";}  ?>
													</td>
													<td>
														<a href="m_agen.php?idT=<?php echo $agencurrent->PROFILE_ID.'&id='.$agen_id.$halkategori; ?>#detail">Detail</a>
													</td>
													<td>
														<?php
														///CEK ID TELAH DITRANSAKSIKAN BELUM
														/*$currentKaryawan= $table_karyawan->findBy('TEMPATLAHIR_ID','TEMPATLAHIR_ID',$KOTA->KOTA_ID);
														$currentKaryawan = $currentKaryawan->current();
														
															if($currentKaryawan == '' ){ */?><a href="#tambah_agen" class="edit_data" style="font-size:15px;" data-toggle="modal" select_hari="<?php echo $periode; ?>" select_name="<?php echo $periodename; ?>" onClick="document.getElementById('id').value=<?php echo "'".$agencurrent->PROFILE_ID."'"; ?>;document.getElementById('periode_add').value=<?php echo "'".$mingguan."'"; ?>;document.getElementById('komposisi_add').value=<?php echo "'".$komposisippn."'"; ?>;document.getElementById('judul').innerHTML='Edit Pelanggan';document.getElementById('select_agen').innerHTML='<?php echo $agencurrent->NAMA;  ?>';"><i class='glyphicon glyphicon-edit'></i></a>														
														<?php /* }else{ 
														echo "&nbsp;";
														}*/ ?>													</td>
													</tr>
													<?php } ?>
													</tbody>
												</table>
									<?php
											  $jmldata     =  $queryAgencurrent->NUM;
											  $jmlhalaman  = $paging->jumlahHalaman($jmldata, $batas);
											  $linkHalaman = $paging->navHalaman('m_agen.php?id='.$agen_id.'&',$_GET['halkategori'], $jmlhalaman);
											  $awal = $posisi_data+1;
											  $akhir = $posisi_data+$batas;
											  if($akhir>=$jmldata) $akhir = $jmldata;
											  echo "Showing $awal to $akhir of $jmldata entries"; ?>
											  <ul class="pagination pull-right">
											  <?php
												echo $linkHalaman ; //banyak halaman yang ditampilkan jd tombol paging
											  ?> 
											  </ul>
								</div>			  
								  	<a name="detail"></a>
									<?php  //if(isset($_REQUEST['idT'])){  ?>
									<h4 id="tabs">Detail</h4>
									<?php  if(isset($_REQUEST['idT'])){  ?>
										<h5 id="tabs"><strong>Nama Pelanggan: <?php echo $namaT; ?></strong></h5>
									<?php } ?>
									  <ul class="nav nav-tabs">
										<li class="active"><a href="#A" data-toggle="tab">TOP</a></li>
										<li><a href="#B" data-toggle="tab">Limit Kredit</a></li>
										<li><a href="#C" data-toggle="tab">Diskon</a></li>
									  </ul>
								<div class="tabbable">
									<div class="tab-content">
									  <div class="tab-pane active" id="A">
										<p><!--<a href="#tambah_Dtagihan" style="font-size:15px;right:" data-toggle="modal"><strong>+ Tambah Detail tagihan</strong></a>--></p>
										<p>
										<table class="table table-striped" id="table_custom1">
														<thead>
															<tr style="background-color: #4B8DF8;font-size: 12px;">
																<th class="center"><font color= "white">TOP</font></th>
																<th class="center"><font color= "white">Tgl Mulai</font></th>
																<th class="center"><font color= "white">Tgl Selesai</font></th>
																<th class="center"><font color= "white">Detail</font></th>
															</tr>
														</thead>
													<tbody>
													<?php if(isset($_REQUEST['idT'])){ 
													$idT = $_REQUEST['idT'];
													$table_TOP = new query('MARKETING','MARKETING.TOP T');
													/*$queryTOP = $table_TOP->selectBy("COUNT(T.PELANGGAN_ID) AS NUM"
													,"P.PROFILE_ID = T.PELANGGAN_ID
													AND T.PELANGGAN_ID = '".$idT."' ");
													$queryTOPcurrent = $queryTOP->current();*/
													
													$queryTOPX = $table_TOP->selectBy("T.TOP_ID, T.TOP, T.TOLERANSI, T.TGL_MULAI, T.TGL_SELESAI"
													,"T.PELANGGAN_ID = '".$idT."' ");
													
													foreach($queryTOPX as $TOPcurrent){
													$topid = $TOPcurrent->TOP_ID;
													$tglselesai = $TOPcurrent->TGL_SELESAI;
													if($tglselesai == '' OR $tglselesai == '0000-00-00 00:00:00') $tglselesai = 'Hingga pemberitahuan lebih lanjut';
													else $tglselesai = date("d M Y", strtotime($tglselesai));
													
													$table_detailTOP = new query('MARKETING', 'MARKETING.DETAIL D, MARKETING.MERK M');
													$querydetailTOP = $table_detailTOP->selectBy(
													'M.MERK,D.KECUALI'
													, 'D.MASTER_ID = "'.$topid.'" 
													AND M.MERK_ID = D.MERK_ID
													order by D.KECUALI');
													$numTOP = $querydetailTOP->num_rows();
													$style = "font-size: 12px";?>
													<tr style="<?php echo $style; ?>">
													<td>
														<?php echo $TOPcurrent->TOP." Hari";  ?>
													</td>
													<td>
														<?php echo date("d M Y",strtotime($TOPcurrent->TGL_MULAI));  ?>
													</td>
													<td>
														<?php echo $tglselesai;  ?>
													</td>
													<td>
														<?php if($numTOP>0){ 
															foreach($querydetailTOP as $currentdetailTOP){
																$kecuali = $currentdetailTOP->KECUALI;
																if($kecuali = 'Y'){	echo "Kecuali Merk: ".$currentdetailTOP->MERK." <br>"; }
																else if($kecuali = 'T'){echo "Berlaku Merk: ".$currentdetailTOP->MERK." <br>"; }
															}
														}else { echo "Berlaku semua merk"; }?>
													</td>
													</tr>
													<?php } } ?>
													</tbody>
										</table>	
												
												<?php
												  	 /* $jmldata     =  $queryDTagihancurrent->NUM;
													  $jmlhalaman  = $paging->jumlahHalaman($jmldata, $batas);
													  $linkHalaman = $paging->navHalaman('m_agen.php?id='.$agen_id.'&',$_GET['halkategori'], $jmlhalaman);
													  $awal = $posisi_data+1;
													  $akhir = $posisi_data+$batas;
													  if($akhir>=$jmldata) $akhir = $jmldata;
													  
													  echo "Showing $awal to $akhir of $jmldata entries";*/
												 ?>
										</p>
									  </div>
									  <div class="tab-pane" id="B">
										<p>
										<table class="table table-striped" id="table_custom3">
														<thead>
															<tr style="background-color: #4B8DF8;font-size: 12px;">
																<th class="center"><font color= "white">Limit</font></th>
																<th class="center"><font color= "white">Mata uang</font></th>
																<th class="center"><font color= "white">Tgl Mulai</font></th>
																<th class="center"><font color= "white">Tgl Selesai</font></th>
																<th class="center"><font color= "white">Detail</font></th>
															</tr>
														</thead>
														<tbody>
														<?php 
														if(isset($_REQUEST['idT'])){ 
														$idT = $_REQUEST['idT'];
														
														$table_limitkredit = new query('MARKETING','MARKETING.LIMITKREDIT T, MARKETING.CURRENCY C');
														
														$querylimitkredit = $table_limitkredit->selectBy("T.LIMITKREDIT_ID, T.LIMITKREDIT, C.CURRENCY, T.TGL_MULAI, T.TGL_SELESAI"
														," T.PELANGGAN_ID = '".$idT."' 
														AND C.CURRENCY_ID = T.CURRENCY_ID");
														
														foreach($querylimitkredit as $limitkreditcurrent){
														$limitkredit = $limitkreditcurrent->LIMITKREDIT;
														if ($limitkredit=='UNLIMITED' or $limitkredit=='') $limitkredit = $limitkredit;
														else $limitkredit = number_format($limitkredit, 2, ',', '.');
													
														$tgl_selesai = $limitkreditcurrent->TGL_SELESAI;
														if($tgl_selesai == '' OR $tgl_selesai == '0000-00-00 00:00:00') $tgl_selesai = 'Hingga pemberitahuan lebih lanjut';
														else $tgl_selesai = date("d M Y", strtotime($tgl_selesai));
														
														$table_detaillimit = new query('MARKETING', 'MARKETING.DETAIL D, MARKETING.MERK M');
														$querydetaillimit = $table_detaillimit->selectBy(
														'M.MERK, D.KECUALI'
														, 'D.MASTER_ID = "'.$limitkredit.'" 
														AND M.MERK_ID = D.MERK_ID
														order by D.KECUALI');
														$numlimit = $querydetaillimit->num_rows();
														$style = "font-size: 12px";?>
														<tr style="<?php echo $style; ?>">
														<td>
														<?php echo $limitkredit;  ?>
														</td>
														<td>
															<?php echo $limitkreditcurrent->CURRENCY;  ?>
														</td>
														<td>
															<?php echo date("d M Y",strtotime($limitkreditcurrent->TGL_MULAI));  ?>
														</td>
														<td>
															<?php echo stripslashes($tgl_selesai); ?> 
														</td>
														<td>
															<?php if($numlimit > 0){
																foreach($querydetaillimit as $currentdetaillimit){
																	$kecuali = $currentdetaillimit->KECUALI;
																	if($kecuali = 'Y'){	echo "Kecuali Merk: ".$currentdetaillimit->MERK." <br>"; }
																	else if($kecuali = 'T'){echo "Berlaku Merk: ".$currentdetaillimit->MERK." <br>"; }
																}
															} else { echo "Berlaku semua merk"; }?>
														</td>
														</tr>
														<?php } } ?>
														</tbody>
										</table>
												<?php
												  	  /*$jmldata     =  $queryPotcurrent->NUM;
													  $jmlhalaman  = $paging->jumlahHalaman($jmldata, $batas);
													  $linkHalaman = $paging->navHalaman('m_agen.php?id='.$agen_id.'&',$_GET['halkategori'], $jmlhalaman);
													  $awal = $posisi_data+1;
													  $akhir = $posisi_data+$batas;
													  if($akhir>=$jmldata) $akhir = $jmldata;
													  
													  echo "Showing $awal to $akhir of $jmldata entries";*/
												   ?>
												<?php //} } ?>
										</p>
									  </div>
									  <div class="tab-pane" id="C">
										<p>
											<table class="table table-striped" id="table_custom4">
												<thead>
													<tr style="background-color: #4B8DF8;font-size: 12px;">
														<th class="center"><font color= "white">Nama Diskon</font></th>
														<th class="center"><font color= "white">Diskon</font></th>
														<th class="center"><font color= "white">Tgl Mulai</font></th>
														<th class="center"><font color= "white">Tgl Selesai</font></th>
														<th class="center"><font color= "white">Detail</font></th>
													</tr>
												</thead>
												<tbody>
													<?php if(isset($_REQUEST['idT'])){ 
													$idT = $_REQUEST['idT']; 
													$table_diskon = new query('MARKETING','MARKETING.MASTER_DISKON M,MARKETING.DETAILAREA_KETENTUAN A');
														
														$querydiskon = $table_diskon->selectBy("distinct M.NAMADISKON, M.TGL_MULAI, M.TGL_AKHIR, M.DISKON,M.MASTERDISKON_ID"
														," A.AGEN_ID = '".$idT."' 
														AND M.MASTERDISKON_ID = A.MASTER_ID
														AND AKTIF = 'Y'
														AND STATUS = 'VALID'
														ORDER BY M.TGL_AKHIR");
														foreach($querydiskon as $diskoncurrent){
														$tgl_selesai = $diskoncurrent->TGL_AKHIR;
														if($tgl_selesai == '' OR $tgl_selesai == '0000-00-00 00:00:00') $tgl_selesai = 'Hingga pemberitahuan lebih lanjut';
														else $tgl_selesai = date("d M Y", strtotime($tgl_selesai));
														
														$diskon_id = $diskoncurrent->MASTERDISKON_ID;
														$table_detaildis = new query('MARKETING', 'MARKETING.DETAIL D, MARKETING.MERK M');
														$querydetaildis = $table_detaildis->selectBy(
														'M.MERK, D.KECUALI'
														, 'D.MASTER_ID = "'.$diskon_id.'" 
														AND M.MERK_ID = D.MERK_ID
														order by D.KECUALI');
														$numdiskon = $querydetaildis->num_rows();
														$style = "font-size: 12px"; ?>
													<tr style="<?php echo $style; ?>">
														<td><?php echo $diskoncurrent->NAMADISKON; ?></td>
														<td><?php echo $diskoncurrent->DISKON; ?></td>
														<td><?php echo date("d M Y", strtotime($diskoncurrent->TGL_MULAI)); ?></td>
														<td><?php echo $tgl_selesai; ?></td>
														<td>
															<?php if($numdiskon > 0){
																foreach($querydetaildis as $currentdetaildis){
																	$kecuali = $currentdetaildis->KECUALI;
																	if($kecuali = 'Y'){	echo "Kecuali Merk: ".$currentdetaildis->MERK." <br>"; }
																	else if($kecuali = 'T'){echo "Berlaku Merk: ".$currentdetaildis->MERK." <br>"; }
																}
															} else { echo "Berlaku semua merk"; }?>
														</td>
													</tr>
													<?php } } ?>
												</tbody>
											</table>														
										</p>
									  </div>
									</div>
								</div> <!-- /tabbable --><?php //} ?>
								 			 	
									
							</div>	
    					</div><!--playground-->
    					<br>
    
    					<div class="clearfix"></div>
   					</div>
	
	<?php //Tambah data maupun edit data, di hide terlebih dahulu untuk nanti ditampilkan dengan menggunakan javascript dibawah?>
  			</div>
		</div>
	</div>
<?php include "../_template/navbar_footer.php"; ?>
</div><!--/main-->



	<?php //javascript yang dibutuhkan untuk halaman ini saja ?>
		
		
		
		<script src="/js/jquery-ui/jquery-ui.js"></script>
		<script type="text/javascript" src="/js/DataTables/media/js/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="/js/DataTables/media/js/DT_bootstrap.js"></script>
		
		
		
		<!-- ----------------------------------------------------------------------------- -->
		
		
		<?php //ajax untuk insert data menggunakan _modal-and-datatable.js ?>
		
		
		
		<script  src="/js/ajax/_modal-and-datatable.js"></script>
		
		
		<!-- ----------------------------------------------------------------------------- -->
		
		
		<script>
	
	$(function() {
	
	// Menjalankan plugin DataTables pada id contoh. id contoh merupakan tabel yang kita gunakan untuk menampilkan data
	$('#table_custom1').dataTable({"bSort": false,"bFilter": false,"bInfo": false,"bLengthChange": false,"bPaginate": false,
});
	$('#table_custom2').dataTable({"bSort": false,"bFilter": false,"bInfo": false,"bLengthChange": false,"bPaginate": false,
});
	$('#table_custom3').dataTable({"bSort": false,"bFilter": false,"bInfo": false,"bLengthChange": false,"bPaginate": false,
});
	$('#table_custom4').dataTable({"bSort": false,"bFilter": false,"bInfo": false,"bLengthChange": false,"bPaginate": false,
});
	
	<?php //Datepicker ?>
	$( "#tanggal1" ).datepicker({changeMonth: true, changeYear: true, yearRange: "-60:+10", dateFormat:"yy-mm-dd"});
	$( "#tanggal2" ).datepicker({changeMonth: true, changeYear: true, yearRange: "-60:+10", dateFormat:"yy-mm-dd"});
	$( "#tanggal3" ).datepicker({changeMonth: true, changeYear: true, yearRange: "-60:+10", dateFormat:"yy-mm-dd"});
	
	<?php //untuk menampilkan div view_data saat awal load ?>
	//$('#view_data').show();
	act = 'AGEN';
	action = 'ADD';
	peringatan = 'Penyimpanan Gagal, Data sudah ada';
	//action = '';
	<?php //peringatan = ''; Digunakan untuk alert ?>
	
	<?php //untuk menampilkan div add_data ?>
	$(".tambah_data").click(function(){
		//$('#view_data').hide(); <?php //Div untuk view data di hide ?>
		//$('.select_komposisi_add').hide();
		//$('#add_data').show();
		//$('#data_token')[0].reset(); <?php //Data form direset ketika user kembali ke halaman tersebut ?>
		act = 'AGEN';
		action = 'ADD';
		peringatan = 'Gagal memasukkan data, refresh halaman / hubungi MIS';
		//var agen_id=$(this).attr('agen_id');
		//var agen_nama=$(this).attr('agen_nama');
		var select_hari=$(this).attr('select_hari');
		//$("#select_agen").html("<option value="+"'"+agen_id+"'"+">"+agen_nama+"</option>"); 
		$("#select_hari").html("<option>"+select_hari+"</option><?php echo $optionhari;?>"); 
	});
	

	<?php //Edit data ?>
	$('#table_custom2').on('click','.edit_data',function (){
	//$('#view_data').hide();
	//$('#add_data').show();
	//$('.select_komposisi_add').show(); <?php //Untuk pilihan token aktif atau tidak ?>
	act = 'AGEN';
	action = 'UPDATE'; 
	peringatan = 'Update gagal, Data sudah ada';
	//$('.notification_select_hari').hide();
	//$('.notification_periode_add').hide();
	//var agen_id=$(this).attr('agen_id');
	//var agen_nama=$(this).attr('agen_nama');
	var select_hari=$(this).attr('select_hari');
	var select_name=$(this).attr('select_name');
	//$("#select_agen").html("<option value="+"'"+agen_id+"'"+">"+agen_nama+"</option>"); 
	$("#select_hari").html("<option value="+select_hari+">"+select_name+"</option><?php echo $optionhari;?>"); 
	});
	
	 $(".lihat_data").click(function(){
		   $('#view_data').show();
		   $('#add_data').hide();
		    $(data_form)[0].reset();
	});	
	
	<?php //Ajax untuk ADD, DELETE DAN UPDATE DATA ?>
	
	$('#submit_agen').click(function(){
			/*if($("#select_agen").val()=="") {
						//alert('Field Asuransi tidak boleh kosong!');
						$('.notification_select_agen').show();
						$('.notification_select_agen').html('Pilihan Agen tidak boleh kosong');
						$("#select_agen").focus();
						return false;
					}
			else{
			$('.notification_select_agen').hide();
			}	*/
			//
			/*
			//Validasi field			
			if($("#select_hari").val()=="") {
						//alert('Field Asuransi tidak boleh kosong!');
						$('.notification_select_hari').show();
						$('.notification_select_hari').html('Field Nama Kota tidak boleh kosong');
						$("#select_hari").focus();
						return false;
					}
			else{
			$('.notification_select_hari').hide();
			}	
			//
			
			//Validasi field
			if($("#periode_add").val()=="") {
						//alert('Field periode_add tidak boleh kosong!');
						$('.notification_periode_add').show();
						$('.notification_periode_add').html('Field Keterangan tidak boleh kosong');
						$("#periode_add").focus();
						return false;
					}
			else{
			$('.notification_periode_add').hide();
			}	
			
			if($("#komposisi_add").val()=="") {
						//alert('Field periode_add tidak boleh kosong!');
						$('.notification_komposisi_add').show();
						$('.notification_komposisi_add').html('Field Komposisi PPN tidak boleh kosong');
						$("#komposisi_add").focus();
						return false;
					}
			else{
			$('.notification_komposisi_add').hide();
			}	*/
			//
			if (confirm('Anda yakin ?')) {
		   	$.ajax({
    		   	type: "POST",
			url: "_ADM/crud_master.php?act="+act+"&action="+action,
			data: $('#data_agen').serialize(),
        		success: function(msg){
 		        	if(msg==1){
					alert(peringatan);
					//$('.notification_select_hari').show();
					//$('.notification_select_hari').html('Update gagal, Data sudah ada');
					}else{
						alert("Penyimpanan Sukses");
						window.location.replace(msg);
					}
					
 		        },
			error: function(){
				alert("koneksi bermasalah, silahkan reload halaman");
				
				}
      			});
    
			} else { 
		return false;
		}
	
		});
		
	
	});
		</script>
		
	</body>
</html>
<?php
}
else{
header('location:/lock.php?logingagal');
}
?>