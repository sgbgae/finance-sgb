<?php
session_start();

if(!empty($_SESSION['username'])){


?>
<!DOCTYPE html>
<html lang="en">
	<?php include '_template/head.php';?>
	<body id="page-top">
<?php include '_template/navbar_head.php';?>
<?php //include '_template/navbar_sub.php';?>
<?php
include_once ("_ADM/_crud/function.php"); 
$alertclass = new alert;
$timeto = new timeto();
$bg = $alertclass->bg(); 

$hariini = date("Y-m-d");
$default = '22561';
$y =0;
for($i=2014;$i<2020;$i++){
	if(date("Y")==$i){
		$default = $default+$y;
		break;
	}
	$y += 512;
}

$date1 = date('Y-m-d', strtotime('- 30 day ', strtotime($hariini)));
$tgl_mulai = $default+((date('m',strtotime($date1))-1)*32)+date('d',strtotime($date1)); 

$tgl_selesai = $default+((date('m')-1)*32)+date('d');
//echo $tgl_mulai." test ".$tgl_selesai;

$calendar = "https://www.google.com/calendar/render?#main_7%7Csearch-1+23170+23170+23170-y++all+BG%20EXPIRED++++".$tgl_mulai."+".$tgl_selesai."+1+7"; 

//TOP 5 pelanggan jatuh tempo
$array = $alertclass->top5jt(); 
//echo $array['data2']." ".$array['data3'];
$diagram1 = $array['data2'];
$diagram2 = $array['data3'];
$jt = $alertclass->jt(); 

?>
<!-- CSS YANG DIBUTUHKAN DI PAGE INI SAJA -->
<script language="javascript">
	<?php include "charts.php"; ?>
</script>
<link rel="stylesheet" href="/js/DataTables/media/css/DT_bootstrap.css" />
<!------------------------------------------------------------------------->


<!--main-->
<div class="navbar">
	<div class="row">   
	<!-- BREADCRUMB UNTUK MEMPERLIHATKAN SEDANG BERADA DI MENU APA -->
		<!--<ol class="breadcrumb">
			<li class="active"><a href="#">Home</a></li>
		</ol>-->
	<!------------------------------------------------------------------------->
		<?php  if($bg['num'] >=1 ){ 
			 $numbg = $bg['num'];
		?>
		<br><br>
		<div class="col-md-12 col-sm-12"> <!-- lg = large, md = medium, sm = small untuk melihat width nya, ada pada bootstrap.css -->
			<div class="paneltosca">
				<div id="view_data">
					<div class="panel-body" style="margin-bottom: 0px;">
					 
					<!--  <h3 id="panels">Dashboard</h3>-->
						<span style="float:left;"><a href="/index_ADM.php">Home</a>
							> <b>Dashboard</b>
						</span>
						<br><br>
						<div class="row">
							<div id="col-lg-12">
								<div class="col-lg-6">
									<div class="panel panel-default">
										<div class="panel-heading" style="background-color: #FF0000;color:#FFFFFF">
										<table border="0" width="100%">
										<tr><td>BG Expired</td>
											<td><div align="right"><a href="<?php echo $calendar;?>" target="_blank"><img src="/images/google_calendar.png" width="30px" style="padding:5px;" ></a></div></td>
										</tr></table>
										</div>
										<div class="panel-body" style="overflow-y:scroll;max-height:100%;overflow-x:scroll;max-width:100%;height:250px">
											<table class="table table-striped" >
											<thead>
												<tr >
													<th class="center">Pelanggan - Area</th>
													<th class="center">Tanggal Expired</th>
												</tr>
											</thead>
											<tbody>
												<?php echo $bg['bgview']; ?>
											</tbody>
											</table>
										</div>
										<div class="panel-body">
											pelanggan yang sudah mendekati 30 hari tanggal expired dan sudah melewati 30 hari dari tanggal expired
										</div>
									</div>
								</div>
								<div class="col-lg-6">
									<div class="panel panel-default">
										<div class="panel-heading" style="background-color: #FF0000;color:#FFFFFF">
										<table border="0" width="100%">
										<tr><td>Mendekati Jatuh Tempo </td>
											<td><div align="right"><a href="<?php echo $calendar;?>" target="_blank"><img src="/images/google_calendar.png" width="30px" style="padding:5px;" ></a></div></td>
										</tr></table>
										</div>
										<div class="panel-body" style="overflow-y:scroll;max-height:100%;overflow-x:scroll;max-width:100%;height:250px">
											<table class="table table-striped" >
											<thead>
												<tr >
													<th class="center">Pelanggan - Area</th>
													<th class="center">Tanggal Jatuh Tempo</th>
												</tr>
											</thead>
											<tbody>
												<?php echo $alertclass->jt2(); ?>
											</tbody>
											</table>
										</div>
										<div class="panel-body">
											pelanggan yang sudah mendekati 7 hari dan melebihi dari tanggal jatuh tempo
										</div>
									</div>
								</div>
							</div>		
						</div>
						<div class="row">
							<div id="col-lg-12">
								<div class="col-lg-6">
									<div class="panel panel-default" style="height:500px">
										<div class="panel-heading" style="background-color: #FF0000;color:#FFFFFF">
											<table border="0" width="100%">
											<tr>
											  <td>Jumlah Surat Tagihan Terbanyak </td>
											</tr></table>
										</div>
										<div class="panel-body">
											<div id="chart_5" style="height:350px;">
											</div>
										</div>
										<div class="panel-body">
											5 pelanggan yang memiliki Surat Tagihan melewati batas jatuh tempo yang belum terlunasi
										</div>
									</div>
								</div>
								<div class="col-lg-6">
									<div class="panel panel-default"  style="height:450px">
										<div class="panel-heading" style="background-color: #FF0000;color:#FFFFFF">
											<table border="0" width="100%">
											<tr>
											  <td>Total Jatuh tempo  </td>
											</tr></table>
										</div>
										<div class="panel-body">
											<div id="interactive" class="chart">
											</div>
										</div>
										<div class="panel-body">
											perbandingan antara surat tagihan yang sudah jatuh tempo dan belum jatuh tempo yang belum terlunasi
										</div>
									</div>
								</div>
							</div>	
						</div>	
						<div class="row">
							<div id="col-lg-12">
								
							</div>	
						</div>	
					<!--div class="portlet box yellow">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-reorder"></i>Stack Chart Controls
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="javascript:;" class="reload">
								</a>
								<a href="javascript:;" class="remove">
								</a>
							</div>
						</div>
						<div class="portlet-body">
							<div id="chart_5" style="height:350px;">
							</div>
							<div class="btn-toolbar">
								<div class="btn-group stackControls">
									<input type="button" class="btn blue" value="With stacking"/>
									<input type="button" class="btn red" value="Without stacking"/>
								</div>
								<div class="space5">
								</div>
								<div class="btn-group graphControls">
									<input type="button" class="btn" value="Bars"/>
									<input type="button" class="btn" value="Lines"/>
									<input type="button" class="btn" value="Lines with steps"/>
								</div>
							</div>
						</div>
					</div-->
						<!--div class="row">
							<div id="col-lg-12">
								<div class="panel panel-default">
									<div class="panel-heading" style="background-color: #FF0000;color:#FFFFFF">
										<table border="0" width="100%">
										<tr><td>BAR </td>
										</tr></table>
									</div>
									<div class="panel-body">
										<div id="chart_1_1" class="chart">
										</div>	
									</div>
								</div>
							</div>
						</div-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN BASIC CHART PORTLET-->
					<!--div class="portlet box purple">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-reorder"></i>Horizontal Bar Chart
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="javascript:;" class="reload">
								</a>
								<a href="javascript:;" class="remove">
								</a>
							</div>
						</div>
						<div class="portlet-body">
							<div id="chart_1_2" class="chart">
							</div>
						</div>
					</div-->
					<!-- END BASIC CHART PORTLET-->
				</div>
			</div>
			<!-- END CHART PORTLETS-->
			<!-- BEGIN PIE CHART PORTLET-->
			<!-- END PIE CHART PORTLET-->
			<!-- BEGIN PIE CHART PORTLET-->
			<!--div class="row">
				<div class="col-md-6">
					<div class="portlet box yellow">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-reorder"></i>Graph4
							</div>
							<div class="tools">
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="javascript:;" class="reload">
								</a>
							</div>
						</div>
						<div class="portlet-body">
							<h4>Semi-transparent, black-colored label background.</h4>
							<div id="pie_chart_4" class="chart">
							</div>
						</div>
					</div>
					<div class="portlet box purple">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-reorder"></i>Graph5
							</div>
							<div class="tools">
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="javascript:;" class="reload">
								</a>
							</div>
						</div>
						<div class="portlet-body">
							<h4>Semi-transparent, black-colored label background placed at pie edge.</h4>
							<div id="pie_chart_5" class="chart">
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-reorder"></i>Graph6
							</div>
							<div class="tools">
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="javascript:;" class="reload">
								</a>
							</div>
						</div>
						<div class="portlet-body">
							<h4>Added a semi-transparent background to the labels and a custom labelFormatter function.</h4>
							<div id="pie_chart_6" class="chart">
							</div>
						</div>
					</div>
					<div class="portlet box red">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-reorder"></i>Graph7
							</div>
							<div class="tools">
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="javascript:;" class="reload">
								</a>
							</div>
						</div>
						<div class="portlet-body">
							<h4>Labels can be hidden if the slice is less than a given percentage of the pie (10% in this case).</h4>
							<div id="pie_chart_7" class="chart">
							</div>
						</div>
					</div>
				</div>
			</div-->
			<!-- END PIE CHART PORTLET-->
			<!-- BEGIN PIE CHART PORTLET-->
			<!--div class="row">
				<div class="col-md-6">
					<div class="portlet box yellow">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-reorder"></i>Graph8
							</div>
							<div class="tools">
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="javascript:;" class="reload">
								</a>
							</div>
						</div>
						<div class="portlet-body">
							<h4>The radius can also be set to a specific size (even larger than the container itself).</h4>
							<div id="pie_chart_8" class="chart">
							</div>
						</div>
					</div>
					<div class="portlet box purple">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-reorder"></i>Graph9
							</div>
							<div class="tools">
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="javascript:;" class="reload">
								</a>
							</div>
						</div>
						<div class="portlet-body">
							<h4>The pie can be tilted at an angle.</h4>
							<div id="pie_chart_9" class="chart">
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-reorder"></i>Graph10
							</div>
							<div class="tools">
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="javascript:;" class="reload">
								</a>
							</div>
						</div>
						<div class="portlet-body">
							<h4>A donut hole can be added.</h4>
							<div id="donut" class="chart">
							</div>
						</div>
					</div>
				</div>
			</div-->
						<!--chart end-->
		
						<!--playground-->
						<br>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		</div><?php } ?>
	</div>
<?php include "_template/navbar_footer.php"; ?>
</div><!--/main-->



	<!-- javascript yang dibutuhkan untuk halaman ini saja -->
		
		
		
		<script type="text/javascript" src="/js/DataTables/media/js/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="/js/DataTables/media/js/DT_bootstrap.js"></script>
		
		
		
		<!-- ----------------------------------------------------------------------------- -->
		
		
		<!-- ajax untuk insert data menggunakan _modal-and-datatable.js -->
		
		
		
		<script  src="/js/ajax/_modal-and-datatable.js"></script>
	
<!--script src="assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script-->
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="assets/plugins/flot/jquery.flot.min.js"></script>
<script src="assets/plugins/flot/jquery.flot.pie.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="assets/scripts/core/app.js"></script>
<!--script src="assets/scripts/custom/charts.js"></script-->
<script>
jQuery(document).ready(function() {       
   // initiate layout and plugins
   App.init();
   Charts.init();
   Charts.initCharts();
   Charts.initPieCharts();
   Charts.initBarCharts();
});
</script>	
	</body>
</html>
<?php
}
else{
header('location:/lock.php?logingagal');
}
?>