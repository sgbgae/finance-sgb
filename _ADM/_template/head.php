	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<title>Finance | PT. Saligading Bersama</title>
		<meta name="generator" content="" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link href="/css/bootstrap/bootstrap.css" rel="stylesheet">
		<link href="/css/styles.css" rel="stylesheet">
		<!--[if lt IE 9]>
			<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		<link href="/css/font-awesome/font-awesome.css" rel="stylesheet">
		 <link href="/css/scroll-nav/scrolling-nav.css" rel="stylesheet">
		 <script src="/js/jquery/jquery.min.js"></script>
		<link rel="shortcut icon" href="images/logo.png" type="image/x-icon">
		<script language="javascript">
function getkey(e)
{
	if (window.event)
	   return window.event.keyCode;
	else if (e)
	   return e.which;
	else
	   return null;
}

function goodchars(e, goods, field)
{
	var key, keychar;
	key = getkey(e);
	if (key == null) return true;

	keychar = String.fromCharCode(key);
	keychar = keychar.toLowerCase();
	goods = goods.toLowerCase();
	
	// check goodkeys
	if (goods.indexOf(keychar) != -1)
		return true;
	// control keys
	if ( key==null || key==0 || key==8 || key==9 || key==27 )
	   return true;
	   
	if (key == 13) {
		var i;
		for (i = 0; i < field.form.elements.length; i++)
			if (field == field.form.elements[i])
				break;
		i = (i + 1) % field.form.elements.length;
		field.form.elements[i].focus();
		return false;
	};
// else return false
	return false;
}

 function removeCharacter(v, ch)
 {
  var tempValue = v+"";
  var becontinue = true;
  while(becontinue == true)
  {
   var point = tempValue.indexOf(ch);
   if( point >= 0 )
   {
    var myLen = tempValue.length;
    tempValue = tempValue.substr(0,point)+tempValue.substr(point+1,myLen);
    becontinue = true;
   }else{
    becontinue = false;
   }
  }
  return tempValue;
 }
 
 function characterControl(value)
 {
  var tempValue = "";
  var len = value.length;
  for(i=0; i<len; i++)
  {
   var chr = value.substr(i,1);
   if( (chr < '0' || chr > '9') && chr != '.' && chr != ',' )
   {
    chr = '';
   }
   
   tempValue = tempValue + chr;
  }
  return tempValue;
 }
 
 function ThausandSeperator(value, digit)
 {
  var thausandSepCh = ".";
  var decimalSepCh = ",";
  
  var tempValue = "";
  var realValue = value+"";
  var devValue = "";
  realValue = characterControl(realValue);
  var comma = realValue.indexOf(decimalSepCh);
  if(comma != -1 )
  {
   tempValue = realValue.substr(0,comma);
   devValue = realValue.substr(comma);
   devValue = removeCharacter(devValue,thausandSepCh);
   devValue = removeCharacter(devValue,decimalSepCh);
   devValue = decimalSepCh+devValue;
   if( devValue.length > 3)
   {
    devValue = devValue.substr(0,3);
   }
  }else{
   tempValue = realValue;
  }
 tempValue = removeCharacter(tempValue,thausandSepCh);
   
  var result = "";
  var len = tempValue.length;
  while (len > 3){
  result = thausandSepCh+tempValue.substr(len-3,3)+result;
  len -=3;
 }
 result = tempValue.substr(0,len)+result;
 return result+devValue;
 }
		</script>
	</head>