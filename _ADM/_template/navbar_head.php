<?php
include_once '_conn/query.php';
include_once "_ADM/_crud/function.php";
//dapatkan nik user
$alertclass = new alert;
$bg = $alertclass->bg();
$nik=$_SESSION['nik'];
$alamat_email = $_SESSION['alamat_email']; 

//query dapatkan menu security			
/*$table_user2= new query('SECURITY','SECURITY.MASTER_USER as MASTER_USER');
$menuuser = $table_user2->select("DISTINCT MASTER_SECURITY.SECURITY",
"INNER JOIN SECURITY.DETAIL_USER DETAIL_USER ON DETAIL_USER.USER_ID = MASTER_USER.USER_ID
INNER JOIN SECURITY.DETAIL_GRUP DETAIL_GRUP ON DETAIL_GRUP.GRUP_ID = DETAIL_USER.GRUP_ID
INNER JOIN SECURITY.MASTER_SECURITY MASTER_SECURITY ON DETAIL_GRUP.SECURITY_ID = MASTER_SECURITY.SECURITY_ID
WHERE MASTER_USER.NIK =  '$nik'");*/
$table_user2= new query('FINANCE','FINANCE.DATA_SETUP');
$menuuser = $table_user2->selectBy("DISTINCT DATA_SETUP",
"NILAI LIKE '%$alamat_email%'");
//echo $menuuser->printquery();
$aksesuser = '';
$i=0;
foreach($menuuser as $menuuser2){
	$aksesuser[$i] = $menuuser2->DATA_SETUP;
	$i = $i+1;
}
//$aksesuserx = substr($aksesuser,1,-1);
//$aksesuser = array ($aksesuser);

//$aksesuser = $aksesuser;
//$SETJUDUL2 = '<p><a href="#" title="Kembali" onClick="window.history.back()" >Kembali ke halaman sebelum nya</a></p>';
?>
<nav class="navbar-fixed-top header" >
 	<div class="col-md-12">
        <div class="navbar-header">
          <a href="#" class="navbar-brand"><img src="images/logo.png" width="25" height="25">&nbsp;<strong><font color="#4B8DF8">PT. Saligading Bersama</font></strong></a>
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse1">
           <i class="fa fa-bars"></i>
          </button>
        </div>
		
		
		
        <div class="collapse navbar-collapse" id="navbar-collapse1">
          <ul class="nav navbar-nav navbar-right">
		  <li><a href="/index_ADM.php"><i class="fa fa-home"></i>&nbsp;Home</a></li>
			 <li><a href="#" class="dropdown-toggle hint" data-container="body" data-placement="bottom" data-content="Menu utama" data-toggle="dropdown"><i class="fa fa-windows"></i>&nbsp; Data <span class="caret"></span></a>
             
			 <ul class="nav dropdown-menu arrow-right">
			  <?php //if(in_array("MASTER_PELANGGAN", $aksesuser) != false){?>
		    <li> <a tabindex="-1" href="m_agen.php"><i class="fa fa-users"></i>&nbsp;<strong>Pelanggan</strong></a>
		   	  <ul class="dropdown-menu sub-menu">
              </ul>
		    </li><?php //} ?>
			<?php // if(in_array("MASTER_BANKGARANSI1", $aksesuser) != false or in_array("MASTER_BANKGARANSI2", $aksesuser) != false){?>
			<li> <a tabindex="-1" href="m_bg.php"><i class="glyphicon glyphicon-usd"></i>&nbsp;<strong>Bank Garansi</strong></a>
		   	  <ul class="dropdown-menu sub-menu">
              </ul>
		    </li>	
			<li> <a tabindex="-1" href="m_kelompok.php"><i class="fa fa-group"></i>&nbsp;<strong>Kelompok</strong></a>
		   	  <ul class="dropdown-menu sub-menu">
              </ul>
		    </li>
			<?php // } ?>		
        </ul>
		
			</li>
			 <li> <a href="#" class="dropdown-toggle hint" data-container="body" data-placement="bottom" data-content="Menu utama" data-toggle="dropdown"><i class="fa fa-exchange"></i>&nbsp; Transaksi <span class="caret"></span></a>
		 	<ul class="nav dropdown-menu arrow-right">
			  	<li> <a tabindex="-1" href="t_sj.php"><i class="fa fa-truck"></i>&nbsp;<strong>Surat jalan</strong></a>
              <ul class="dropdown-menu sub-menu">
		      </ul>
			</li>
		   <li> <a tabindex="-1" href="t_tagihan.php"><i class="fa fa-credit-card"></i>&nbsp;<strong>Tagihan</strong></a>
		   	  <ul class="dropdown-menu sub-menu">
              </ul>
		   </li>
			<li> <a tabindex="-1" href="t_pembayaran.php"><i class="fa fa-money"></i>&nbsp;<strong>Pembayaran</strong></a>
              <ul class="dropdown-menu">
              </ul>
			</li>
        </ul>
			</li>
			 <li>
			  <a href="#" class="dropdown-toggle hint" data-container="body" data-placement="bottom" data-content="Menu utama" data-toggle="dropdown"><i class="fa fa-files-o"></i>&nbsp; Laporan <span class="caret"></span></a>
			 <ul class="nav dropdown-menu arrow-right">
			 	<li> <a tabindex="-1" href="l_sj.php"><strong>Surat Jalan</strong></a>
				  <ul class="dropdown-menu sub-menu"></ul>
				</li>
				<li> <a tabindex="-1" href="l_penjualan.php"><strong>Penjualan Produk</strong></a>
				  <ul class="dropdown-menu sub-menu"></ul>
				</li>
				<li> <a tabindex="-1" href="l_penjharian.php"><strong>Penjualan Harian</strong></a>
				  <ul class="dropdown-menu sub-menu"></ul>
				</li>
				<li> <a tabindex="-1" href="l_produknol.php"><strong>Penjualan Produk Harga Nol</strong></a>
				  <ul class="dropdown-menu sub-menu"></ul>
				</li>				
				<!--li> <a tabindex="-1" href="l_penjagen.php"><strong>Penjualan Agen</strong></a>
				  <ul class="dropdown-menu sub-menu"></ul>
				</li>							
				<li> <a tabindex="-1" href="l_penjrupiah.php"><strong>Penjualan Rupiah</strong></a>
				  <ul class="dropdown-menu sub-menu"></ul>
				</li-->									
				<li> <a tabindex="-1" href="l_penjsubagen.php"><strong>Penjualan Bulanan Sub Agen</strong></a>
				  <ul class="dropdown-menu sub-menu"></ul>
				</li>						
				<li> <a tabindex="-1" href="l_penjbulproduk.php"><strong>Penjualan Bulanan Produk</strong></a>
				  <ul class="dropdown-menu sub-menu"></ul>
				</li>	
				<li> <a tabindex="-1" href="l_penjtahunan.php"><strong>Penjualan Tahunan</strong></a>
				  <ul class="dropdown-menu sub-menu"></ul>
				</li>
				<li> <a tabindex="-1" href="l_fakturmingguan.php"><strong>Faktur Mingguan</strong></a>
				  <ul class="dropdown-menu sub-menu"></ul>
				</li>
				<li> <a tabindex="-1" href="l_trackingpaket.php"><strong>Tracking Paket</strong></a>
				  <ul class="dropdown-menu"></ul>
				</li>
				<li> <a tabindex="-1" href="l_blokirpelanggan.php"><strong>Blokir Pelanggan</strong></a>
				  <ul class="dropdown-menu"></ul>
				</li>
				<li> <a tabindex="-1" href="l_sisalimit.php"><strong>Sisa Limit Kredit</strong></a>
				  <ul class="dropdown-menu"></ul>
				</li>
				<!--li> <a tabindex="-1" href="l_pricelist.php"><strong>Price List</strong></a>
				  <ul class="dropdown-menu sub-menu"></ul>
				</li>
				<li> <a tabindex="-1" href="l_audit.php"><strong>Rekap Audit</strong></a>
				  <ul class="dropdown-menu"></ul>
				</li>
				<li> <a tabindex="-1" href="l_fakming.php"><strong>Rekap Faktur Mingguan</strong></a>
				  <ul class="dropdown-menu"></ul>
				</li>
				<li> <a class="trigger right-caret" tabindex="-1" href="#"><strong>Rekap Penjualan</strong></a>
					<ul class="dropdown-menu sub-menu">
						<li><a tabindex="-1" href="l_r_penj.php">Rekap Penjualan</a></li>
						<li><a tabindex="-1" href="l_r_penjagen.php">Rekap Penjualan Agen</a></li>
						<li><a tabindex="-1" href="l_r_penjblnproduk.php">Rekap Penjualan Bulanan Produk</a></li>
						<li><a tabindex="-1" href="l_r_penjblnsubagen.php">Rekap Penjualan Bulanan Sub Agen</a></li>
						<li><a tabindex="-1" href="l_r_penjharganol.php">Rekap Penjualan Harga Nol</a></li>
						<li><a tabindex="-1" href="l_r_penjharian.php">Rekap Penjualan Harian</a></li>
						<li><a tabindex="-1" href="l_r_penjproduknetto.php">Rekap Penjualan Produk Netto</a></li>
						<li><a tabindex="-1" href="l_r_penjrupiah.php">Rekap Penjualan Rupiah</a></li>
						<li><a tabindex="-1" href="l_r_penjtahunan.php">Rekap Penjualan Tahunan</a></li>
					</ul>
				</li>  
				<li> <a tabindex="-1" href="l_sjfaktur.php"><strong>Rekap Surat Jalan Faktur</strong></a>
				  <ul class="dropdown-menu"></ul>
				</li-->
				
			</ul>
			</li>
			<li>
                <a href="#" class="dropdown-toggle hide-minimize" data-toggle="dropdown"><i class="fa fa-th" style="padding:4px 0 0 0;"></i></a>
                <ul class="dropdown-menu arrow-right">
                  <div align="center" style="width:200px;" class="list-inline">
				
				<li><a href="https://mail.google.com" target="_blank"><img src="/images/gmail.png" width="50px" style="padding:5px;" ></a><br/>Mail</li>
				<li><a href="https://drive.google.com" target="_blank"><img src="/images/google_drive.png" width="50px" style="padding:5px;" ></a><br/>Drive</li>
				<li><a href="https://calendar.google.com" target="_blank"><img src="/images/google_calendar.png" width="50px" style="padding:5px;" ></a><br/>Calendar</i>
				</div>
                </ul>
             </li>
             <li>
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Help</a>
                <ul class="dropdown-menu arrow-right">
                  <li><a href="#">Help Center</li>
                  <li><a href="#">Send Feedback</a></li>
                </ul>
             </li>
			 
			 <?php 
			 if($bg['num'] >=1 ){ 
			 	$numbg = $bg['num'];
			 ?>
			 <li>
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-warning"></i>
					<span class="badge">
						 <?php echo $numbg; ?>
					</span></a>
                <ul class="dropdown-menu arrow-right">
                  <div  style="padding: 5px 15px 10px 15px;break-word;width:450px;" class="list-inline">
					<h5>BG Expired</h5>
					<h6>
						<?php echo $bg['alertbg']; ?>
					</h6>
					
				</div>
                </ul>
             </li>
			 <?php } ?>
             <li style="display:none"><a href="#" id="btnToggle"><i class="glyphicon glyphicon-th-large"></i></a></li>
             <li><a class = "dropdown-toggle" href = "#" data-toggle = "dropdown"><?php echo $_SESSION['alamat_email'];?><span class="caret"></span></a>
			  <ul class="nav dropdown-menu arrow-right">
   
    <div style = "padding: 5px 15px 10px 15px; width: auto;word-wrap: break-word;">
	
	<h6>Apakah anda akan keluar dari aplikasi ?</h6>
		<form action="/logout.php">
         <button type="submit" class="btn btn-sm btn-danger pull-right"><i class="fa fa-sign-out"></i>&nbsp;Sign Out</button>
		 </form>
		 <hr>
      </div>
	
	  </ul>
			 </li>
           </ul>
        </div>	
     </div>	
	
</nav>

