<?php
date_default_timezone_set('Asia/Manila');
$act=$_GET['act'];
$action=$_GET['action'];
include_once '_conn/query.php';
include_once 'function.php';
include_once 'modul/generateid.php';

IF ($act == 'SJ'){
	$NAMA_DB = 'FINANCE';
	$URL_CALLBACK = 't_sj.php';
	$NAMA_TABEL = 'SJ';
	$exec = new crud($NAMA_DB,$NAMA_TABEL);
	$FIND = new query($NAMA_DB,$NAMA_TABEL);
	IF ($action == 'ADD'){ 
		$id = $exec->GenId();
		/*$NAMA = $FIND->selectBy('count(SJ_ID) AS NUM','PELANGGAN_ID = "'.trim(strtoupper($_REQUEST['select_agen'])).'" 
			AND TANGGAL = "'.trim(strtoupper($_REQUEST['date3'])).'" 
			AND TIPE_ID = "'.trim(strtoupper($_REQUEST['select_tipe'])).'" 
			AND NOFAKTUR = "'.trim(strtoupper($_REQUEST['nosj'])).'" 
			AND NOPOL = "'.trim(strtoupper($_REQUEST['nopol'])).'" ')->current();
		if($NAMA->NUM == 0){*/
			$URUTAN = $FIND->selectBy('count(SJ_ID) AS NUM','TANGGAL = "'.date("Y-m-d").'" ')->current();
			$urutan = 1+$URUTAN->NUM;
			$NOFAKTUR = 'MKT01.1.'.date("ymd").'.'.$urutan; //echo $NOFAKTUR.'.'.$urutan;
			$ARRAY_FIELD = array(  
				'SJ_ID' => $id,
				'TIPE_ID' => trim(strtoupper($_REQUEST['select_tipe'])),
				'PELANGGAN_ID' => trim(strtoupper($_REQUEST['select_agen'])),
				'NOSJ' => trim(strtoupper($NOFAKTUR)),
				'NOFAKTUR' => trim(strtoupper($_REQUEST['nosj'])),
				'TANGGAL' => trim(strtoupper($_REQUEST['date3'])),
				'NOPOL' => trim(strtoupper($_REQUEST['nopol'])),
				'DATE_MODIFIED' => date("Y-m-d H:i:s")
			);
			IF (($exec->CheckValue('SJ_ID','SJ_ID',$id)) == 0 && ($id) != ''){	//$SELECT,$WHERE,$VALUE
				//$exec->Add($ARRAY_FIELD,$URL_CALLBACK."?idT=".$id."#detail");
				$exec->Add($ARRAY_FIELD,$URL_CALLBACK."?idT=".$id."&detail=y");
			}else { echo "1"; }
		//} else { echo "1"; }
	} else if ($action == 'UPDATE'){
		$ARRAY_FIELD = array(
			'PELANGGAN_ID' => trim(strtoupper($_REQUEST['select_agen'])),
			'TIPE_ID' => trim(strtoupper($_REQUEST['select_tipe'])),
			'NOFAKTUR' => trim(strtoupper($_REQUEST['nosj'])),
			'TANGGAL' => trim(strtoupper($_REQUEST['date3'])),
			'NOPOL' => trim(strtoupper($_REQUEST['nopol'])),
			'DATE_MODIFIED' => date("Y-m-d H:i:s")
		);
		$exec->Update('SJ_ID',$_POST['id'],$ARRAY_FIELD,$URL_CALLBACK."?idT=".$_POST['id']."&detail=y");
		//echo "1";
	} 
}else IF ($act == 'DSJ'){
	$NAMA_DB = 'FINANCE';
	$URL_CALLBACK = 't_sj.php';
	$NAMA_TABEL = 'DETAIL_SJ';
	$exec = new crud($NAMA_DB,$NAMA_TABEL);
	$FIND = new query($NAMA_DB,$NAMA_TABEL);
	
	if ($action == 'DELETE'){
		$detailid = $_REQUEST['detailid'];
		$exec = new query($NAMA_DB,$NAMA_TABEL);
		$exec->delete('DETAIL_ID = "'.$detailid.'"');
		echo $detailid;
		exit;
	}
	//IF(isset($_REQUEST['net_prc']))
	IF($_REQUEST['diskon2'] != 0 //and $_REQUEST['diskon2'] == 0
	){ $net_prc = '0'; $diskon = '1';}
	else { $net_prc = '1'; $diskon = '0';}
	$diskon2 = $_REQUEST['diskon2'];
	IF(isset($_REQUEST['net_prc'])){ $net_prc = '1'; $diskon = '0'; $diskon2 = 0;}
	else { $net_prc = '0'; $diskon = '1'; $diskon2 = 2;}
	$trans = array("." => "");
	
	$harga = number_format(strtr($_REQUEST['harga'], $trans), 2, '.', ''); 
	$harga2 = number_format(strtr($_REQUEST['harga2'], $trans), 2, '.', ''); 
	if($harga == '0'){$harga2 = 0;}
	
	IF ($action == 'ADD'){ 	
		$id = $exec->GenId();
			$ARRAY_FIELD = array(  
				'DETAIL_ID' => $id,
				'SJ_ID' => trim(strtoupper($_REQUEST['idsj'])),
				'PRODUK_ID' => trim(strtoupper($_REQUEST['select_produk'])),
				'KARUNG' => trim(strtoupper($_REQUEST['jumlah'])),
				'HARGA' => trim(strtoupper($harga)),
				'HARGA2' => trim(strtoupper($harga2)),
				'DISKON' => trim(strtoupper($diskon)),
				'NETTO2' => trim(strtoupper($net_prc)),
				'DISKON2' => trim(strtoupper($diskon2))
			);
			IF (($exec->CheckValue('DETAIL_ID','DETAIL_ID',$id)) == 0 && ($id) != '' && ($_REQUEST['idsj']) != ''){	//$SELECT,$WHERE,$VALUE
				$exec->Add($ARRAY_FIELD,$URL_CALLBACK."?idT=".$_REQUEST['idsj']."&detail=y#detail");
			}else { echo "1"; }
		//} else { echo "1";}
	} else if ($action == 'UPDATE'){ 	
			if($_REQUEST['iddsj'] != ''){
				$ARRAY_FIELD = array(
					'PRODUK_ID' => trim(strtoupper($_REQUEST['select_produk'])),
					'KARUNG' => trim(strtoupper($_REQUEST['jumlah'])),
					'HARGA' => trim(strtoupper($harga)),
					'HARGA2' => trim(strtoupper($harga2)),
					'DISKON' => trim(strtoupper($diskon)),
					'NETTO2' => trim(strtoupper($net_prc)),
					'DISKON2' => trim(strtoupper($diskon2))
				);
				$exec->Update('DETAIL_ID',$_POST['iddsj'],$ARRAY_FIELD,$URL_CALLBACK."?idT=".$_POST['idsj']."&detail=y");
			}else { echo "1"; }
		//}else { echo "1"; }
	} 
	
}else IF ($act == 'NOTAPENJUALAN'){ 
	IF(isset($_REQUEST['select_agen'])){
		$PELANGGAN_ID = trim(strtoupper($_REQUEST['select_agen']));
	}
	$TANGGAL = date("Y/m/d");
	$WAKTU = date("H:i:s");
	$URL_CALLBACK = 't_tagihan.php';
	IF ($action == 'ADD'){ 	
		
		$NAMA_DB = 'FINANCE';
		$URL_CALLBACK = 't_sj.php';
		$NAMA_TABEL = 'DETAIL_SJ';
		$queryfinance = new queryfinance();
		$exec = new crud($NAMA_DB,$NAMA_TABEL);
		$FIND = new query($NAMA_DB,$NAMA_TABEL);
		//cek apakah ada nota penjualan agen yg sama sebelumnya sesuai dengan periode cut of, jika tidak ada terhitung harian
		//jika ada yg belum jadi nota tagihan sebelumnya, maka sistem akan menolak dan harus dibuatkan nota tagihan sebelumnya baru bisa membuat nota tpenjualan yang baru
		//start
		//$PELANGGAN_ID = trim(strtoupper($_REQUEST['select_agen']));
		
		$cek1 = new query('FINANCE','PROFILE.PROFILE P LEFT JOIN FINANCE.AGEN A ON A.PELANGGAN_ID = P.PROFILE_ID');
		$querycek1 = $cek1->selectBy("MINGGUAN, PERIODE"
			,"P.PROFILE_ID = '".trim(strtoupper($_REQUEST['select_agen']))."'");
		//echo $querycek1->printquery();
		foreach($querycek1 as $querycek1current){ 
			$mingguan = $querycek1current->MINGGUAN;
			//jika memiliki cut off tagihan
			if($mingguan == '1'){
				$daysE['Sunday'] = 1;
				$daysE['Monday'] = 2;
				$daysE['Tuesday'] = 3;
				$daysE['Wednesday'] = 4;
				$daysE['Thursday'] = 5;
				$daysE['Friday'] = 6;
				$daysE['Saturday'] = 7;
				$HrSkrg = $daysE[date('l')];
				$HrPeriode = $querycek1current->PERIODE;
				if($HrSkrg > $HrPeriode){
					$daysmin = $HrSkrg-$HrPeriode;
					$TglPeriodeAkhir = date('Y-m-d', strtotime('-'.$daysmin.' days'));
				} else { 
					$daysmin = ($HrSkrg + 7) - $HrPeriode;
					$TglPeriodeAkhir = date('Y-m-d', strtotime('-'.$daysmin.' days'));
				}
				$tglterakhirtagihan = date('Y-m-d', strtotime('-7 days',strtotime($TglPeriodeAkhir)));
				$cek2 = new query('FINANCE','TAGIHAN T, DETAIL_TAGIHAN DT, SJ S');
				$querycek2 = $cek2->selectBy("COUNT(T.TAGIHAN_ID) AS NUM"
				,"T.TAGIHAN_ID = DT.TAGIHAN_ID
				AND DT.SJ_ID = S.SJ_ID
				AND S.PELANGGAN_ID = '".trim(strtoupper($_REQUEST['select_agen']))."'
				AND S.TANGGAL <= '".$tglterakhirtagihan."' 
				AND ((T.NOTAGIHAN = '') or (T.NOTAGIHAN is null))
				"); //ECHO $querycek2->printquery();
				$querycek2current = $querycek2->current();
				$numcek2 = $querycek2current->NUM;
			} else {
				$cek2 = new query('FINANCE','TAGIHAN T, DETAIL_TAGIHAN DT, SJ S');
				$querycek2 = $cek2->selectBy("COUNT(T.TAGIHAN_ID) AS NUM"
				,"T.TAGIHAN_ID = DT.TAGIHAN_ID
				AND DT.SJ_ID = S.SJ_ID
				AND S.PELANGGAN_ID = '".trim(strtoupper($_REQUEST['select_agen']))."'
				AND ((T.NOTAGIHAN = '') or (T.NOTAGIHAN is null))
				"); //ECHO $querycek2->printquery();
				$querycek2current = $querycek2->current();
				$numcek2 = $querycek2current->NUM;
			}
			IF($numcek2 > 0){echo "2"; exit;
			}
			
			$cek1 = new query('FINANCE','SJ S');
			$querycek1 = $cek1->select("DISTINCT S.SJ_ID",
			"LEFT JOIN FINANCE.DETAIL_TAGIHAN DT ON S.SJ_ID = DT.SJ_ID 
			INNER JOIN PROFILE.PROFILE PR ON PR.PROFILE_ID = S.PELANGGAN_ID 
			WHERE PR.PROFILE_ID = '$PELANGGAN_ID'
			AND NOT EXISTS (SELECT * FROM DETAIL_TAGIHAN DT WHERE DT.SJ_ID =S.SJ_ID)");
			$jmldata     =  $querycek1->num_rows();
			if($jmldata == '0'){
				echo "1";
			}else{
				
				$cek1 = new query('FINANCE','PROFILE.PROFILE P LEFT JOIN FINANCE.AGEN A ON A.PELANGGAN_ID = P.PROFILE_ID');
				$querycek1 = $cek1->selectBy("MINGGUAN, PERIODE"
					,"P.PROFILE_ID = '$PELANGGAN_ID'");
				foreach($querycek1 as $querycek1current){ //echo "test"; exit;
					$mingguan = $querycek1current->MINGGUAN;
					//jika memiliki cut off tagihan
					//exit;
					for($z=0;$z<3;$z++){
						if($z == 0){
							$HrPeriode = $querycek1current->PERIODE;
							$table_NPenj = new query('FINANCE','SJ S
								INNER JOIN DETAIL_SJ DS ON S.SJ_ID =DS.SJ_ID 
								INNER JOIN GBJ.PRODUK_VARIAN PV ON PV.MASTERBARANG_ID = DS.PRODUK_ID
								INNER JOIN GBJ.PRODUK_ARTIKEL PA ON PA.PRODUKARTIKEL_ID = PV.PRODUKARTIKEL_ID 
								INNER JOIN MARKETING.PELANGGAN P ON P.PELANGGAN_ID = S.PELANGGAN_ID
								INNER JOIN MARKETING.TOP T ON T.PELANGGAN_ID = P.PELANGGAN_ID
								INNER JOIN MARKETING.DETAIL D ON D.MASTER_ID = T.TOP_ID AND (D.MERK_ID = PA.MERK_ID OR D.PRODUK_ID = PV.MASTERBARANG_ID)');
						
							$queryNPenj = $table_NPenj->selectBy("DISTINCT S.SJ_ID, S.TANGGAL, T.TOP_ID , D.DETAIL_ID"
								," S.PELANGGAN_ID = '$PELANGGAN_ID'
								AND NOT EXISTS ( SELECT * FROM DETAIL_TAGIHAN DT WHERE DT.SJ_ID =S.SJ_ID)
								AND D.KECUALI = 'T'
								ORDER BY 2,1");
							
						} else if($z == 1){
							$table_NPenj = new query('FINANCE','SJ S
								INNER JOIN DETAIL_SJ DS ON S.SJ_ID =DS.SJ_ID 
								INNER JOIN GBJ.PRODUK_VARIAN PV ON PV.MASTERBARANG_ID = DS.PRODUK_ID
								INNER JOIN GBJ.PRODUK_ARTIKEL PA ON PA.PRODUKARTIKEL_ID = PV.PRODUKARTIKEL_ID 
								INNER JOIN MARKETING.PELANGGAN P ON P.PELANGGAN_ID = S.PELANGGAN_ID
								INNER JOIN MARKETING.TOP T ON T.PELANGGAN_ID = P.PELANGGAN_ID 
								INNER JOIN MARKETING.DETAIL D2 ON D2.MASTER_ID = T.TOP_ID');
							/*$queryNPenj = $table_NPenj->selectBy("DISTINCT S.SJ_ID, S.TANGGAL, T.TOP_ID , '' as DETAIL_ID"
								," S.PELANGGAN_ID = '$PELANGGAN_ID'
								AND NOT EXISTS ( SELECT * FROM DETAIL_TAGIHAN DT WHERE DT.SJ_ID =S.SJ_ID)
                                AND NOT EXISTS ( SELECT * FROM MARKETING.DETAIL D WHERE D.KECUALI = 'Y' AND PA.MERK_ID = D.MERK_ID)
								AND D2.KECUALI = 'Y'
								ORDER BY 2,1"); */
							$queryNPenj = $table_NPenj->selectBy("DISTINCT S.SJ_ID, S.TANGGAL, T.TOP_ID , '' as DETAIL_ID"
								," S.PELANGGAN_ID = '$PELANGGAN_ID'
								AND NOT EXISTS ( SELECT * FROM DETAIL_TAGIHAN DT WHERE DT.SJ_ID =S.SJ_ID)
								AND D2.KECUALI = 'Y'  AND 
									((PA.MERK_ID <> D2.MERK_ID AND D2.MERK_ID IS NOT NULL) OR (D2.PRODUK_ID <> PV.MASTERBARANG_ID AND D2.PRODUK_ID IS NOT NULL))
								UNION
								select DISTINCT S.SJ_ID, S.TANGGAL, T.TOP_ID , '' as DETAIL_ID
								from FINANCE.SJ S
								INNER JOIN FINANCE.DETAIL_SJ DS ON S.SJ_ID =DS.SJ_ID 
								INNER JOIN GBJ.PRODUK_VARIAN PV ON PV.MASTERBARANG_ID = DS.PRODUK_ID
								INNER JOIN GBJ.PRODUK_ARTIKEL PA ON PA.PRODUKARTIKEL_ID = PV.PRODUKARTIKEL_ID 
								INNER JOIN MARKETING.PELANGGAN P ON P.PELANGGAN_ID = S.PELANGGAN_ID
								INNER JOIN MARKETING.TOP T ON T.PELANGGAN_ID = P.PELANGGAN_ID 
								WHERE S.PELANGGAN_ID = '$PELANGGAN_ID' and NOT EXISTS ( SELECT * FROM FINANCE.DETAIL_TAGIHAN DT WHERE DT.SJ_ID =S.SJ_ID)
								AND NOT EXISTS (SELECT * FROM MARKETING.DETAIL D WHERE D.MASTER_ID = T.TOP_ID)
								ORDER BY 2,1");
							
							//echo $queryNPenj->num_rows()."TEST";
							//exit;
							if($num != 0){break;}
						} else if($z == 2){
							if($num != 0){break;}
							/*$table_NPenj = new query('FINANCE','SJ S
								INNER JOIN DETAIL_SJ DS ON S.SJ_ID =DS.SJ_ID ');
							$queryNPenj = $table_NPenj->selectBy("DISTINCT S.SJ_ID, S.TANGGAL, '' as TOP_ID , '' as DETAIL_ID"
								," S.PELANGGAN_ID = '$PELANGGAN_ID'
								AND NOT EXISTS ( SELECT * FROM DETAIL_TAGIHAN DT WHERE DT.SJ_ID =S.SJ_ID)
								ORDER BY 2,1"); */
								echo "5";
							break;
						}
						//break;
						$num = $queryNPenj->num_rows();
						//echo $num;
						//echo $queryNPenj->printquery();
						//exit;
						$i=0;
						$j=0;
						$k=1;
						$hariini = date("Y-m-d");
						foreach($queryNPenj as $NPenjcurrent){
							$j =$j+1;		 //echo 	$j;	
							$TANGGALSJ = $NPenjcurrent->TANGGAL;
							$tglmulai = $TANGGALSJ;
							$tglselesai = $TANGGALSJ;			
							/*if($mingguan == '1'){
								$periode = new periode($NPenjcurrent->TANGGAL, '');
								$Ptagihan = $periode->tagihan($HrPeriode);
								$periode2 = new periode($Ptagihan, '');
								$array = $periode2->mingguan();
								$tglmulai = $array['tglmulai'];
								$tglselesai = $array['tglselesai'];
							} else {
								$tglmulai = $hariini;
								$tglselesai = $hariini;
							}*/
							//$querynota = " AND S.TANGGAL BETWEEN '".$array['tglmulai']."' AND '".$array['tglselesai']."' ";
							
							$TOPid = $NPenjcurrent->TOP_ID;
							$SJid[$j] = $NPenjcurrent->SJ_ID;
							
							if($num == 1){
								$querynota = " AND S.SJ_ID = '".$SJid[$j]."'";
								//ECHO $querynota;
								$arrayagennota = $queryfinance->savenotapenjualan($querynota, $PELANGGAN_ID, $TOPid);
								//echo $arrayagennota;
							} else if($i !=0 and $j == $num){
								if($tglmulaix == $tglmulai and $tglselesaix == $tglselesai and $TOPidx == $TOPid){
									$wheresjid = '';
									for($l=$k;$l<$j;$l++){
										$wheresjid .= " S.SJ_ID = '".$SJid[$l]."'  OR ";
									}
									$wheresjid .= " S.SJ_ID = '".$SJid[$j]."' ";
									$querynota = " AND (".$wheresjid.")";
									//ECHO $querynota;
									$arrayagennota = $queryfinance->savenotapenjualan($querynota, $PELANGGAN_ID, $TOPid);
									
								} else {						
									$querynota = " AND S.SJ_ID = '".$SJid[$j]."'";
									//ECHO $querynota;
									$arrayagennota = $queryfinance->savenotapenjualan($querynota, $PELANGGAN_ID, $TOPid);
									
									$wheresjid = '';
									for($l=$k;$l<$j;$l++){
										$wheresjid .= "  OR S.SJ_ID = '".$SJid[$l]."' ";
									} //echo $querynota;
									$querynota = " AND (".substr($wheresjid,5).") ";
									$arrayagennota = $queryfinance->savenotapenjualan($querynota, $PELANGGAN_ID, $TOPidx);
									//ECHO $querynota;
									
								}
							} else if($i !=0){
								if($tglmulaix == $tglmulai and $tglselesaix == $tglselesai and $TOPidx == $TOPid){
								
								} else {
									$wheresjid = '';
									for($l=$k;$l<$j;$l++){
										$wheresjid .= "  OR S.SJ_ID = '".$SJid[$l]."' ";
									}
									$querynota = " AND (".substr($wheresjid,5).") ";
									//ECHO $querynota;
									$arrayagennota = $queryfinance->savenotapenjualan($querynota, $PELANGGAN_ID, $TOPidx);
									$k = $j;
								}
							} else {}
							
							$i = 1;
							$tglmulaix = $tglmulai;
							$tglselesaix = $tglselesai;
							$TOPidx = $TOPid;
						}
					}
				}
				
			}
		}
		//End
	} ELSE IF ($action == 'UPDATE'){ 	
		$SIMPANTAG5 = new crud("FINANCE","TAGIHAN");
		$TAGIHAN_ID = trim(strtoupper($_REQUEST['id']));
		$NONOTA = trim(strtoupper($_REQUEST['no_nota']));
		if($NONOTA == ''){
			echo "3";
		}else{
			$BACK = array(
				'NONOTA' => $NONOTA
			);
			$SIMPANTAG5->Updateloop('TAGIHAN_ID',$TAGIHAN_ID,$BACK);
		}
	} ELSE IF ($action == 'CREATE'){ 			
		$PELANGGAN_ID = trim(strtoupper($_REQUEST['PELANGGAN_ID']));
		$halkategori = '';
		if(isset($_GET['halkategori'])){
			$halkategori = "halkategori=".$_GET['halkategori'];
		}
		//echo $PELANGGAN_ID;
		//exit;
		$TAGIHAN_ID=$_GET['TAGIHAN_ID'];
		//CEK TAGIHAN
		$SIMPANTAG5 = new crud("FINANCE","TAGIHAN");
		$ceks = new query('FINANCE','TAGIHAN T');
		$querycek5 = $ceks->select("DISTINCT T.TAGIHAN_ID, T.TOP_ID, S.TANGGAL, A.MINGGUAN,A.PERIODE,
		CASE SUBSTRING(S.NOFAKTUR,1,1)
			WHEN '0' THEN '1'
			ELSE '0'
		END as TIPE ",
		"INNER JOIN DETAIL_TAGIHAN DT ON DT.TAGIHAN_ID = T.TAGIHAN_ID
		left JOIN AGEN A ON A.PELANGGAN_ID = T.PELANGGAN_ID
		INNER JOIN SJ S ON S.SJ_ID = DT.SJ_ID
		WHERE T.TAGIHAN_ID = '$TAGIHAN_ID' ");
		//echo $querycek5->printquery();
		$CURRENTquerycek5 = $querycek5->current();
		$TANGGALS = date("ymd");
		$jmldata5 = $CURRENTquerycek5->TIPE;
		$TOP_ID = $CURRENTquerycek5->TOP_ID;		
		$mingguan = $CURRENTquerycek5->MINGGUAN;
		$TGLSJ = $CURRENTquerycek5->TANGGAL;
		$HrPeriode = $CURRENTquerycek5->PERIODE;
		if($jmldata5 == '0'){
			//NONFAKTUR
			$max = $ceks->select("MAX(NOTAGIHAN) AS MAX",
			"INNER JOIN DETAIL_TAGIHAN DT ON DT.TAGIHAN_ID = T.TAGIHAN_ID
			INNER JOIN SJ S ON S.SJ_ID = DT.SJ_ID
			WHERE SUBSTRING(S.NOFAKTUR,1,1) != '0'");
			$max = $max->current();
			$nomax = $max->MAX;
			$NOURUT = explode(".", $nomax);
			$NOUTUTFIX = $NOURUT[3]+1;
			$querywhere = " AND SUBSTRING(S.NOFAKTUR,1,1) != '0'";
			$NOTAGIHAN = "ACC01.2.".$TANGGALS.".".$NOUTUTFIX;
		}else{
			//FAKTUR
			$max = $ceks->select("MAX(NOTAGIHAN) AS MAX",
			"INNER JOIN DETAIL_TAGIHAN DT ON DT.TAGIHAN_ID = T.TAGIHAN_ID
			INNER JOIN SJ S ON S.SJ_ID = DT.SJ_ID
			WHERE SUBSTRING(S.NOFAKTUR,1,1) = '0'");
			$max = $max->current();
			$nomax = $max->MAX;
			$NOURUT = explode(".", $nomax);
			$NOUTUTFIX = $NOURUT[3]+1;
			$querywhere =  " AND SUBSTRING(S.NOFAKTUR,1,1) = '0'";			
			$NOTAGIHAN = "ACC01.1.".$TANGGALS.".".$NOUTUTFIX;
		} //ECHO $NOTAGIHAN;
		if($mingguan == '1'){
			$periode = new periode($TGLSJ, '');
			$Ptagihan = $periode->tagihan($HrPeriode);
			$periode2 = new periode($Ptagihan, '');
			$array = $periode2->mingguan();
			$tglmulai = $array['tglmulai'];
			$tglselesai = $array['tglselesai'];
		} else {
			$tglmulai = $TGLSJ;
			$tglselesai = $TGLSJ;
		}
		$querywhere .=  " AND S.TANGGAL BETWEEN '".$tglmulai."' AND '".$tglselesai."' ";
		if($TOP_ID == ''){ $querywhere .= "  AND (T.TOP_ID = '' OR T.TOP_ID IS NULL) ";}
		else { $querywhere .= "  AND T.TOP_ID = '".$TOP_ID."' ";}
		
		/*$querycek5 = $ceks->select("DISTINCT T.NONOTA,T.TAGIHAN_ID,S.TANGGAL,M.TOP, DS.KARUNG*DS.HARGA AS TOTAL,
		CASE SUBSTRING(S.NOFAKTUR,1,1)
			WHEN '0' THEN '1'
			ELSE '0'
		END as TIPE, AG.PERIODE",
		"INNER JOIN DETAIL_TAGIHAN DT ON DT.TAGIHAN_ID = T.TAGIHAN_ID
		INNER JOIN SJ S ON S.SJ_ID = DT.SJ_ID
			INNER JOIN AGEN AG ON AG.PELANGGAN_ID = S.PELANGGAN_ID
			INNER JOIN DETAIL_SJ DS ON DS.SJ_ID =DT.SJ_ID 
		LEFT JOIN MARKETING.TOP M ON M.PELANGGAN_ID = T.PELANGGAN_ID  
			AND M.TOP_ID = T.TOP_ID
		WHERE 1 AND (NOTAGIHAN IS NULL OR NOTAGIHAN = '')
			AND T.PELANGGAN_ID  = '".$PELANGGAN_ID."' ".$querywhere.
		" ORDER BY T.NONOTA ASC ");
		$querycek5 = $ceks->select("DISTINCT T.NONOTA,T.TAGIHAN_ID,S.TANGGAL,M.TOP, DS.KARUNG*(V.ISI/SS.ISI)*DS.HARGA AS TOTAL,
		CASE SUBSTRING(S.NOFAKTUR,1,1)
			WHEN '0' THEN '1'
			ELSE '0'
		END as TIPE, AG.PERIODE",
		"INNER JOIN DETAIL_TAGIHAN DT ON DT.TAGIHAN_ID = T.TAGIHAN_ID
		INNER JOIN SJ S ON S.SJ_ID = DT.SJ_ID
			LEFT JOIN AGEN AG ON AG.PELANGGAN_ID = S.PELANGGAN_ID
			INNER JOIN DETAIL_SJ DS ON DS.SJ_ID =DT.SJ_ID 
			INNER JOIN MASTERGUDANG.MASTER_BARANG MB ON MB.MASTERBARANG_ID = DS.PRODUK_ID
        	INNER JOIN GBJ.PRODUK_VARIAN V ON V.MASTERBARANG_ID = MB.MASTERBARANG_ID
			INNER JOIN MASTERGUDANG.SATUAN SS ON SS.SATUAN_ID = MB.SATUANHITUNG_ID
		LEFT JOIN MARKETING.TOP M ON M.PELANGGAN_ID = T.PELANGGAN_ID  
			AND M.TOP_ID = T.TOP_ID
		WHERE 1 AND (NOTAGIHAN IS NULL OR NOTAGIHAN = '')
			AND T.PELANGGAN_ID  = '".$PELANGGAN_ID."' ".$querywhere.
		" ORDER BY T.NONOTA ASC ");*/
		$querycek5 = $ceks->select("DISTINCT  DS.DETAIL_ID, T.NONOTA,T.TAGIHAN_ID,S.TANGGAL,M.TOP, 
		CASE SUBSTRING(S.NOFAKTUR,1,1)
			WHEN '0' THEN (DS.KARUNG*(V.ISI/SS.ISI)*DS.HARGA)-((DS.KARUNG*(V.ISI/SS.ISI)*DS.HARGA)*DS.DISKON2/100)+((DS.KARUNG*(V.ISI/SS.ISI)*DS.HARGA)*10/100)
			ELSE (DS.KARUNG*(V.ISI/SS.ISI)*DS.HARGA)-((DS.KARUNG*(V.ISI/SS.ISI)*DS.HARGA)*DS.DISKON2/100)
		END as TOTAL, 
		CASE SUBSTRING(S.NOFAKTUR,1,1)
			WHEN '0' THEN '1'
			ELSE '0'
		END as TIPE, AG.PERIODE, 
		CASE count(DISTINCT POT.POTONGAN_ID) 
			WHEN 0 THEN 0 
			ELSE Sum(POT.JUMLAH)*count(DISTINCT POT.POTONGAN_ID)/count(*) 
		END AS POTONGAN",
		"INNER JOIN DETAIL_TAGIHAN DT ON DT.TAGIHAN_ID = T.TAGIHAN_ID
		INNER JOIN SJ S ON S.SJ_ID = DT.SJ_ID
			LEFT JOIN AGEN AG ON AG.PELANGGAN_ID = S.PELANGGAN_ID
			INNER JOIN DETAIL_SJ DS ON DS.SJ_ID =DT.SJ_ID 
			INNER JOIN MASTERGUDANG.MASTER_BARANG MB ON MB.MASTERBARANG_ID = DS.PRODUK_ID
        	INNER JOIN GBJ.PRODUK_VARIAN V ON V.MASTERBARANG_ID = MB.MASTERBARANG_ID
			INNER JOIN MASTERGUDANG.SATUAN SS ON SS.SATUAN_ID = MB.SATUANHITUNG_ID
		LEFT JOIN MARKETING.TOP M ON M.PELANGGAN_ID = T.PELANGGAN_ID  
			AND M.TOP_ID = T.TOP_ID
		LEFT JOIN FINANCE.POTONGAN POT ON POT.TAGIHAN_ID = DT.TAGIHAN_ID
		WHERE 1 AND (NOTAGIHAN IS NULL OR NOTAGIHAN = '')
			AND T.PELANGGAN_ID  = '".$PELANGGAN_ID."' ".$querywhere.
		" GROUP BY 1,2,3,4,5,6,7 ORDER BY T.NONOTA ASC ");
		
		
		//echo $querycek5->printquery();
		//EXIT;
		$querywhere2 = '';
		$TOTAL = 0;
		$POTONGAN = 0;
		foreach($querycek5 as $querycek5current){
			$TAGIHAN_ID2 = $querycek5current->TAGIHAN_ID;
			$NONOTA = $querycek5current->NONOTA;
			IF($querycek5current->POTONGAN != 0){$POTONGAN = $querycek5current->POTONGAN;}
			if($NONOTA == ''){
			//echo "test";
				echo "<script lang=javascript>
				alert('Warning: Periksa Kembali, Nomor Nota belum di isi');
				document.location.href = '/t_tagihan.php?".$halkategori."';
				</script>"; 
				exit;
			}
			//exit;
			$querywhere2 .= " OR T.TAGIHAN_ID = '".$TAGIHAN_ID2."'";
			$jumlah = $querycek5current->TOTAL;
			$TOTAL = $TOTAL+$querycek5current->TOTAL;
			$HrPeriode = $querycek5current->PERIODE;
			$TGLSJ = $querycek5current->TANGGAL;
			//$TGLSJ = '2015-08-20';
			if($HrPeriode != ''){
				$daysE['Sunday'] = 1;
				$daysE['Monday'] = 2;
				$daysE['Tuesday'] = 3;
				$daysE['Wednesday'] = 4;
				$daysE['Thursday'] = 5;
				$daysE['Friday'] = 6;
				$daysE['Saturday'] = 7;
				$HrSkrg = $daysE[date('l', strtotime($TGLSJ))]; 
				if($HrSkrg > $HrPeriode){
					$daysmin = $HrSkrg-$HrPeriode; echo $daysmin." ";
					$TglPeriodeAkhir = date('Y-m-d', strtotime($TGLSJ.' -'.$daysmin.' days'));
					$TglPeriodeAkhir = date('Y-m-d', strtotime($TglPeriodeAkhir.' +7 days'));
				} else { 
					$daysmin = ($HrSkrg + 7) - $HrPeriode; echo $daysmin." ";
					$TglPeriodeAkhir = date('Y-m-d', strtotime($TGLSJ.' -'.$daysmin.' days'));
					$TglPeriodeAkhir = date('Y-m-d', strtotime($TglPeriodeAkhir.' +7 days'));
				}
				$DATE = $TglPeriodeAkhir;
			} else {
				$DATE = $TGLSJ;
			}
			
			
			$DATE1 = str_replace('-', '/', $DATE);
			$TOPS = $querycek5current->TOP;
			IF($TOPS == 'CASH'){ 
				$TGLJATUHTEMPO = $TGLSJ;				
			} ELSE {
				$TOPFIX = "+".$TOPS."days";
				IF($TOPS == ''){
					$TGLJATUHTEMPO = NULL;
				} else {
					$TGLJATUHTEMPO = date('Y-m-d',strtotime($DATE1 . $TOPFIX));
				}
			}
			//echo $DATE." ".$TOPFIX." ".$TGLJATUHTEMPO;
		//exit;
			//echo $TGLJATUHTEMPO;
			$SIMPANTAG5 = new crud("FINANCE","TAGIHAN");
			$BACK = array(
			'NOTAGIHAN' => $NOTAGIHAN,
			'JATUH_TEMPO' => $TGLJATUHTEMPO,
			'TOTAL' => $jumlah
			);
			$SIMPANTAG5->Updateloop('TAGIHAN_ID',$TAGIHAN_ID2,$BACK);
		}
		$querywhere2 = "(".substr($querywhere2,3).")";
		$querycek6 = $ceks->select("DISTINCT T.TAGIHAN_ID",
		"where 1 AND ".$querywhere2);
		//echo $querycek6->printquery();
		$TOTAL = $TOTAL-$POTONGAN;
		foreach($querycek6 as $querycek6current){
			$TAGIHAN_ID3 = $querycek6current->TAGIHAN_ID;
			IF($TAGIHAN_ID == $TAGIHAN_ID3){
				$SIMPANTAG5 = new crud("FINANCE","TAGIHAN");
				$BACK = array(
				'KURANG_BAYAR' => $TOTAL
				);
				$SIMPANTAG5->Updateloop('TAGIHAN_ID',$TAGIHAN_ID3,$BACK);
			}
			$SIMPANTAG5 = new crud("FINANCE","TAGIHAN");
			$BACK = array(
			'TOTAL2' => $TOTAL
			);
			$SIMPANTAG5->Updateloop('TAGIHAN_ID',$TAGIHAN_ID3,$BACK);
		}
		//exit;
			
		echo "<script lang=javascript>
		document.location.href = '/t_tagihan.php?".$halkategori."';
		</script>"; 
		
	}
}else IF ($act == 'POTONGAN'){
	$NAMA_DB = 'FINANCE';
	$URL_CALLBACK = 't_tagihan.php';
	$NAMA_TABEL = 'POTONGAN';
	$exec = new crud($NAMA_DB,$NAMA_TABEL);
	$FIND = new query($NAMA_DB,$NAMA_TABEL);
	$trans = array("." => "");
	$jumlah = strtr(trim(strtoupper($_REQUEST['jumlah2'])), $trans);
	IF ($action == 'ADD'){ 	
		$id = $exec->GenId();
		$ARRAY_FIELD = array(  
			'POTONGAN_ID' => $id,
			'TAGIHAN_ID' => trim(strtoupper($_REQUEST['tagihanid'])),
			'JNSPOTONG_ID' => trim(strtoupper($_REQUEST['select_jnspot'])),
			'JUMLAH' => $jumlah,
			'KETERANGAN' => trim(strtoupper($_REQUEST['ket_pot']))
		);
		IF (($exec->CheckValue('POTONGAN_ID','POTONGAN_ID',$id)) == 0 && ($id) != ''){	//$SELECT,$WHERE,$VALUE
			//$exec->Add($ARRAY_FIELD,$URL_CALLBACK."?idT=".$id."#detail");
			$exec->Add($ARRAY_FIELD,$URL_CALLBACK."?idT=".$_REQUEST['tagihanid']."&detail=y");
		}else { echo "1"; }
	} ELSE IF ($action == 'UPDATE'){ 
			$ARRAY_FIELD = array(
				'JNSPOTONG_ID' => trim(strtoupper($_REQUEST['select_jnspot'])),
				'JUMLAH' => $jumlah,
				'KETERANGAN' => trim(strtoupper($_REQUEST['ket_pot']))
			);
			$exec->Update('POTONGAN_ID',$_POST['potonganid'],$ARRAY_FIELD,$URL_CALLBACK."?idT=".$_REQUEST['tagihanid']."");
	}
} else IF ($act == 'PEMBAYARAN2'){
	IF ($action == 'ADD'){ 
		$trans = array("," => "");
		$PELANGGAN_ID = trim(strtoupper($_REQUEST['select_agen']));
		$P_ID2 = trim(strtoupper($_REQUEST['pemb_id']));
		
		$JUMLAH = strtr(trim(strtoupper($_REQUEST['JUMLAH'])), $trans);
		
		$cek1 = new query('FINANCE','TAGIHAN T');
		$cek2 = new query('FINANCE','PEMBAYARAN');
		$SIMPAN = new crud("FINANCE","PEMBAYARAN");
		$SIMPAN2 = new crud("FINANCE","TAGIHAN");
		$SIMPAN3 = new crud("FINANCE","BAYAR_TAGIHAN");
		
		
		$QUERYPEMB = $cek2->select("SALDO, KEMBAR_PEMBAYARAN_ID","WHERE PEMBAYARAN_ID = '$P_ID2' ");
		$PEMBCURRENT = $QUERYPEMB->current();		
		$JUMLAH = $PEMBCURRENT->SALDO; 
		$KEMBAR_PEMBAYARAN_ID = $PEMBCURRENT->KEMBAR_PEMBAYARAN_ID; //ECHO $KEMBAR_PEMBAYARAN_ID;  
		
		
			//KURANGI TAGIHAN
			//QUERY TAGIHAN SORT JATUH TEMPO AND KURANG BAYAR != ''
			$JUMLAHTAGIHAN = $cek1->select("T.TAGIHAN_ID, T.KURANG_BAYAR",
			"WHERE T.PELANGGAN_ID = '$PELANGGAN_ID' AND T.KURANG_BAYAR IS NOT NULL AND T.KURANG_BAYAR != '0'
			AND T.KURANG_BAYAR IS NOT NULL
			AND T.NOTAGIHAN != '' ORDER BY JATUH_TEMPO ASC limit 0,1");
			$numjumtag = $JUMLAHTAGIHAN->num_rows();
			$i = 0; 
			foreach($JUMLAHTAGIHAN as $CEKTAGIHANX){
				$i = $i+1;
				//for ($i= 1; $i <= $JUMLAHTAGIHAN; $i++){
				$KURANGBAYAR = $CEKTAGIHANX->KURANG_BAYAR;
				$TAGIHANBAYAR_ID = $CEKTAGIHANX->TAGIHAN_ID;
				//$SISA = $KURANGBAYAR - $JUMLAH;
				$SISA = $JUMLAH - $KURANGBAYAR;
				//echo $JUMLAH."-".$KURANGBAYAR."=".$SISA;
			//exit;
				//JIKA ADA SISA
				if($SISA > 0){
					//PERULANGAN BERULANG TERUS
					//UPDATE KURANG BAYAR
					$JUMLAHBAYAR = $KURANGBAYAR;
					$KURANG_BAYAR = 0;
					$JUMLAH = $SISA;
					$SALDO = $JUMLAH;
					//JIKA ADA SALDO
					/*$BACK2 = array(
						'SALDO' => $JUMLAH
					);
					$SIMPAN->Updateloop('PEMBAYARAN_ID',$P_ID2,$BACK2);					
					
					/*$BACK1 = array(
						'SALDO' => $JUMLAH
					);
					$SIMPAN->Updateloop('KEMBAR_PEMBAYARAN_ID',$P_ID2,$BACK1);*/
				} else{
					//PERULANGAN BERHENTI
					//UPDATE KURANG BAYAR
					//$SISA = str_replace(array('['-']'), '',$SISA);
					$JUMLAHBAYAR = $JUMLAH;
					$trans2 = array("-" => "");
		            $SISA = strtr($SISA, $trans2);
					$KURANG_BAYAR = $SISA;
					$SALDO = 0;
					/*										
					$BACK1 = array(
						'SALDO' => 0
					);
					$SIMPAN->Updateloop('PEMBAYARAN_ID',$P_ID2,$BACK1);
					
					/*$BACK1 = array(
						'SALDO' => 0
					);
					$SIMPAN->Updateloop('KEMBAR_PEMBAYARAN_ID',$P_ID2,$BACK1);*/
				}
				
				$BACK1 = array(
					'SALDO' => $SALDO
				);
				$SIMPAN->Updateloop('PEMBAYARAN_ID',$P_ID2,$BACK1);
				
				$QUERYPEMB2 = $cek2->select("SALDO","WHERE KEMBAR_PEMBAYARAN_ID = '$P_ID2' ");
				$PEMBCURRENT2 = $QUERYPEMB2->current();					
				$JUMLAH2 = $PEMBCURRENT2->SALDO; 
				//echo $SALDO." ".$JUMLAH2;
				if($SALDO == $JUMLAH2){
					$BACK = array(
						'KURANG_BAYAR' => $KURANG_BAYAR
					);
								
					$SIMPAN2->Updateloop('TAGIHAN_ID',$TAGIHANBAYAR_ID,$BACK);
					//echo $SIMPAN3->GenId();
						
					$ARRAY_FIELD = array(
						'BAYARTAGIHAN_ID' => $SIMPAN3->GenId(),
						'TAGIHAN_ID' => $TAGIHANBAYAR_ID,
						'PEMBAYARAN_ID' => $P_ID2,
						'JUMLAH' => $JUMLAHBAYAR
					);
					$SIMPAN3->AddLoop($ARRAY_FIELD);
				}
			} //exit;
		echo "<script lang=javascript>
		document.location.href = '/t_pembayaran.php?';
		</script>"; 
	}	
} else IF ($act == 'PEMBAYARAN'){
	IF ($action == 'ADD'){ 	
		$PELANGGAN_ID = trim(strtoupper($_REQUEST['select_agen']));
		$METODE_ID = trim(strtoupper($_REQUEST['select_metode']));
		$TGL_BAYAR = trim(strtoupper($_REQUEST['tanggalbayar']));
		$TGL_TEMPO = trim(strtoupper($_REQUEST['tanggaltempo']));
		$BAYAR = trim(strtoupper($_REQUEST['select_bayar']));
		$JUMLAH = trim(strtoupper($_REQUEST['jumlah']));
		$trans = array("." => "");
		$JUMLAH = strtr($JUMLAH, $trans);
		$BANK = trim(strtoupper($_REQUEST['bank']));
		$GIRO = trim(strtoupper($_REQUEST['giro']));
		$ACC = trim(strtoupper($_REQUEST['acc']));
		$USERNAME = trim(strtoupper($_REQUEST['username']));

		$cek1 = new query('FINANCE','TAGIHAN T');
		$cek2 = new query('FINANCE','PEMBAYARAN P');
		$SIMPAN = new crud("FINANCE","PEMBAYARAN");
		$SIMPAN2 = new crud("FINANCE","TAGIHAN");
		$SIMPAN3 = new crud("FINANCE","BAYAR_TAGIHAN");

		$querycek2 = $cek2->select("P.PEMBAYARAN_ID",
		"WHERE P.KLOP = 'T' AND PELANGGAN_ID =  '$PELANGGAN_ID'");
		$CEKKLOP2= $querycek2->current();		
		$KLOP1 = $querycek2->num_rows();
		IF($JUM >= 2){ EXIT;}
		
		//PENGECEKAN TAGIHAN
		$querycek1 = $cek1->select("T.TAGIHAN_ID,T.JATUH_TEMPO",
		"WHERE T.PELANGGAN_ID =  '$PELANGGAN_ID'
		AND T.KURANG_BAYAR !=  '' AND T.NOTAGIHAN != '' ORDER BY JATUH_TEMPO ASC LIMIT 1");
		$TEMPO23 = $querycek1->current();
		$TAGIHANS_ID = $TEMPO23->TAGIHAN_ID;
		$jmldata = $querycek1->num_rows();
		
		//DEKLARASI PENGINPUT USER BAYAR
		
		if($USERNAME == 'S_UTAMI'){
			$USERBAYAR = '1';
		}else if($USERNAME == 'W_KUSUMAWATI'){
			$USERBAYAR = '2';
		}
		
		//if($USERNAME == 'w_kusumawati'){
			//$USERBAYAR = '2';
		//}
		//PENGECEKAN KLOP
		/*
		$querycek2 = $cek2->select("P.USERNAME",
		"WHERE P.TAGIHAN_ID =  '$TAGIHANS_ID'
		AND P.USERBAYAR =  '1' AND P.KLOP = 'T'");
		*/
		$querycek2 = $cek2->select("P.PEMBAYARAN_ID,P.USERNAME",
		"WHERE P.TAGIHAN_ID =  '$TAGIHANS_ID'
		AND P.KLOP = 'T' AND KEMBAR_PEMBAYARAN_ID = '' AND USERBAYAR != '$USERBAYAR'");
		$CEKKLOP2= $querycek2->current();
		$USERKLOP1 = $CEKKLOP2->USERNAME;
		$P_ID1 = $CEKKLOP2->PEMBAYARAN_ID;
		
		if($jmldata >= '1'){
			if($KLOP1 == '0'){
				$ARRAY_FIELD = array(
					'PEMBAYARAN_ID' => $SIMPAN->GenId(),
					'PELANGGAN_ID' => $PELANGGAN_ID,
					'METODE_ID' => $METODE_ID,
					'TAGIHAN_ID' => $TAGIHANS_ID,
					'TGL_DIBAYAR' => $TGL_BAYAR,
					'TGL_JTH_TEMPO' => $TGL_TEMPO,
					'JENIS_BAYAR' => $BAYAR,
					'STATUS_BAYAR' => '',
					'JUMLAH' => $JUMLAH,
					'SALDO' => '0',
					'BANK' => $BANK,
					'NOGIRO' => $GIRO,
					'NOACC' => $ACC,
					'USERBAYAR' => $USERBAYAR,
					'KLOP' => 'T',
					'KEMBAR_PEMBAYARAN_ID' => '',
					'USERNAME' => $USERNAME
					//'TRANSAKSI_ID' => ''
				);
			
				$SIMPAN->AddLoop($ARRAY_FIELD);
				
			//}else if($KLOP1 >= '2'){
				//echo "4";
			
		   	} else{
		
			//	if($USERKLOP1 == $USERNAME){
			//	echo "3";
			//	}else{

				$ARRAY_FIELD = array(
					'PEMBAYARAN_ID' => $SIMPAN->GenId(),
					'PELANGGAN_ID' => $PELANGGAN_ID,
					'METODE_ID' => $METODE_ID,
					'TAGIHAN_ID' => $TAGIHANS_ID,
					'TGL_DIBAYAR' => $TGL_BAYAR,
					'TGL_JTH_TEMPO' => $TGL_TEMPO,
					'JENIS_BAYAR' => $BAYAR,
					'STATUS_BAYAR' => '',
					'JUMLAH' => $JUMLAH,
					'SALDO' => '0',
					'BANK' => $BANK,
					'NOGIRO' => $GIRO,
					'NOACC' => $ACC,
					'USERBAYAR' => $USERBAYAR,
					'KLOP' => 'T',
					'KEMBAR_PEMBAYARAN_ID' => $P_ID1,
					'USERNAME' => $USERNAME
					//'TRANSAKSI_ID' => ''
				);
				$SIMPAN->AddLoop($ARRAY_FIELD);
			
				//DAPATKAN NILAI KEMBAR PEMBAYARAN 2
				$querycek200 = $cek2->select("P.PEMBAYARAN_ID,P.USERNAME",
					"WHERE P.KEMBAR_PEMBAYARAN_ID = '$P_ID1'");
				$CEKKLOP200= $querycek200->current();
				$P_ID2 = $CEKKLOP200->PEMBAYARAN_ID;

				//UPDATE KEMBAR PEMBAYARAN 1
				$UPDATE1 = array(
					'KEMBAR_PEMBAYARAN_ID' => $P_ID2
				);
				$SIMPAN->Updateloop('PEMBAYARAN_ID',$P_ID1,$UPDATE1);
		
				//AMBIL JUMLAH 1
				$JUMLAH1 = $cek2->select("P.JUMLAH",
					"WHERE P.TAGIHAN_ID = '$TAGIHANS_ID' AND P.USERBAYAR = '1' AND PEMBAYARAN_ID = '$P_ID1'");
				$JUMLAH1 = $JUMLAH1->current();
				$SUMJUMLAH1 = $JUMLAH1->JUMLAH;
			
				//AMBIL JUMLAH 2
				$JUMLAH2 = $cek2->select("P.JUMLAH AS JUMLAH2",
					"WHERE P.TAGIHAN_ID = '$TAGIHANS_ID' AND P.USERBAYAR = '2' AND PEMBAYARAN_ID = '$P_ID2'");
				$JUMLAH2 = $JUMLAH2->current();
				$SUMJUMLAH2 = $JUMLAH2->JUMLAH2;
				
				//JIKA KLOP
				if($SUMJUMLAH1 == $SUMJUMLAH2){
					$BACK = array(
						'KLOP' => 'Y'
					);
					$SIMPAN->Updateloop('PEMBAYARAN_ID',$P_ID1,$BACK);
					$SIMPAN->Updateloop('PEMBAYARAN_ID',$P_ID2,$BACK);

					//KURANGI TAGIHAN
					//QUERY TAGIHAN SORT JATUH TEMPO AND KURANG BAYAR != ''
					$JUMLAHTAGIHAN = $cek1->select("T.TAGIHAN_ID, T.KURANG_BAYAR",
					"WHERE T.PELANGGAN_ID = '$PELANGGAN_ID' AND T.KURANG_BAYAR IS NOT NULL AND T.KURANG_BAYAR != '0'
					AND T.KURANG_BAYAR IS NOT NULL AND TAGIHAN_ID = '$TAGIHANS_ID'
					AND T.NOTAGIHAN != '' ORDER BY JATUH_TEMPO ASC limit 0,1");
					$numjumtag = $JUMLAHTAGIHAN->num_rows();
					$i = 0;
					foreach($JUMLAHTAGIHAN as $CEKTAGIHANX){
						$i = $i+1;
						//for ($i= 1; $i <= $JUMLAHTAGIHAN; $i++){
						$KURANGBAYAR = $CEKTAGIHANX->KURANG_BAYAR;
						$TAGIHANBAYAR_ID = $CEKTAGIHANX->TAGIHAN_ID;
						//$SISA = $KURANGBAYAR - $JUMLAH;
						$SISA = $JUMLAH - $KURANGBAYAR;
						
						//JIKA ADA SISA
						if($SISA > 0){
							//PERULANGAN BERULANG TERUS
							//UPDATE KURANG BAYAR
							$JUMLAHBAYAR = $KURANGBAYAR;
							$BACK = array(
								'KURANG_BAYAR' => '0'
											);
							$SIMPAN2->Updateloop('TAGIHAN_ID',$TAGIHANBAYAR_ID,$BACK);
							$JUMLAH = $SISA;
							//JIKA ADA SALDO
							if($i == $numjumtag){
							$BACK2 = array(
								'SALDO' => $SISA
											);
							$SIMPAN->Updateloop('PEMBAYARAN_ID',$P_ID2,$BACK2);
							
							$BACK1 = array(
								'SALDO' => $SISA
							);
							$SIMPAN->Updateloop('KEMBAR_PEMBAYARAN_ID',$P_ID2,$BACK1);
							}
						} else{
							//PERULANGAN BERHENTI
							//UPDATE KURANG BAYAR
							//$SISA = str_replace(array('['-']'), '',$SISA);
							$JUMLAHBAYAR = $JUMLAH;
							$trans2 = array("-" => "");
		                    $SISA = strtr($SISA, $trans2);
							
							$BACK = array(
								'KURANG_BAYAR' => $SISA
							);
							
							$SIMPAN2->Updateloop('TAGIHAN_ID',$TAGIHANBAYAR_ID,$BACK);
							
							$BACK2 = array(
								'SALDO' => 0
											);
							$SIMPAN->Updateloop('PEMBAYARAN_ID',$P_ID2,$BACK2);
							
							$BACK1 = array(
								'SALDO' => 0
							);
							$SIMPAN->Updateloop('KEMBAR_PEMBAYARAN_ID',$P_ID2,$BACK1);
						}
						//echo $SIMPAN3->GenId();
						
						$ARRAY_FIELD = array(
							'BAYARTAGIHAN_ID' => $SIMPAN3->GenId(),
							'TAGIHAN_ID' => $TAGIHANBAYAR_ID,
							'PEMBAYARAN_ID' => $P_ID2,
							'JUMLAH' => $JUMLAHBAYAR
						);
						$SIMPAN3->AddLoop($ARRAY_FIELD);
					}
					//JIKA TIDAK KLOP
				} else{
					echo "2";
					exit;
				}			
		
				//}
			}	
			//$TANGGAL3 = $TANGGAL."3";	
		} else {
			echo "1";
		}
	} else if ($action == 'UPDATE'){ 
		$PEMBAYARAN_ID = trim(strtoupper($_REQUEST['id']));
		$PELANGGAN_ID = trim(strtoupper($_REQUEST['select_agen']));
		$METODE_ID = trim(strtoupper($_REQUEST['select_metode']));
		$TGL_BAYAR = trim(strtoupper($_REQUEST['tanggalbayar']));
		$TGL_TEMPO = trim(strtoupper($_REQUEST['tanggaltempo']));
		$BAYAR = trim(strtoupper($_REQUEST['select_bayar']));
		$JUMLAH = trim(strtoupper($_REQUEST['jumlah']));
		$trans = array("." => "");
		$JUMLAH = strtr($JUMLAH, $trans);
		$BANK = trim(strtoupper($_REQUEST['bank']));
		$GIRO = trim(strtoupper($_REQUEST['giro']));
		$ACC = trim(strtoupper($_REQUEST['acc']));
		$USERNAME = trim(strtoupper($_REQUEST['username']));
		
		$cek1 = new query('FINANCE','TAGIHAN T');
		$cek2 = new query('FINANCE','PEMBAYARAN P');
		$SIMPAN = new crud("FINANCE","PEMBAYARAN");
		$SIMPAN2 = new crud("FINANCE","TAGIHAN");
		$SIMPAN3 = new crud("FINANCE","BAYAR_TAGIHAN");
		//user bayar
		if($USERNAME == 'M_RIZKY'){
			$USERBAYAR = '2';
		}
		//QUERY UPDATE
		$querycek1 = $cek1->select("T.TAGIHAN_ID,T.JATUH_TEMPO",
		"WHERE T.PELANGGAN_ID =  '$PELANGGAN_ID'
		AND T.KURANG_BAYAR !=  '' AND T.NOTAGIHAN != '' ORDER BY JATUH_TEMPO ASC LIMIT 1");
		$TEMPO23 = $querycek1->current();
		$TAGIHANS_ID = $TEMPO23->TAGIHAN_ID;
		$jmldata = $querycek1->num_rows();

		//ECHO $TAGIHANS_ID;
		
		$UPDATE1 = array(
			'PELANGGAN_ID' => $PELANGGAN_ID,
			'METODE_ID' => $METODE_ID,
			'TAGIHAN_ID' => $TAGIHANS_ID,
			'TGL_DIBAYAR' => $TGL_BAYAR,
			'TGL_JTH_TEMPO' => $TGL_TEMPO,
			'JENIS_BAYAR' => $BAYAR,
			'STATUS_BAYAR' => '',
			'JUMLAH' => $JUMLAH,
			'SALDO' => '0',
			'BANK' => $BANK,
			'NOGIRO' => $GIRO,
			'NOACC' => $ACC,
			'KLOP' => 'T',
			'KEMBAR_PEMBAYARAN_ID' => $P_ID1,
			'USERNAME' => $USERNAME
		);
		$SIMPAN->Updateloop('PEMBAYARAN_ID',$PEMBAYARAN_ID,$UPDATE1);
		
		//CEK JIKA SUDAH ADA 2 ORANG INPUT
		$querycek2 = $cek2->select("P.PEMBAYARAN_ID,P.USERNAME",
		"WHERE P.TAGIHAN_ID =  '$TAGIHANS_ID'
		AND P.KLOP = 'T' AND USERBAYAR != '$USERBAYAR'");
		$CEKKLOP2= $querycek2->current();
		$USERKLOP1 = $CEKKLOP2->USERNAME;
		$P_ID1 = $CEKKLOP2->PEMBAYARAN_ID;
		$KLOP1 = $querycek2->num_rows();
		//jika hanya ada 1 orang yg input
		if($KLOP1 == '0'){
			//do nothing		
		}else{
			//jika sudah ada 2
			//cek klop
			//AMBIL JUMLAH 1
			$JUMLAH1 = $cek2->select("P.JUMLAH",
				"WHERE P.TAGIHAN_ID = '$TAGIHANS_ID'  AND PEMBAYARAN_ID = '$PEMBAYARAN_ID'");
			$JUMLAH1 = $JUMLAH1->current();
			$SUMJUMLAH1 = $JUMLAH1->JUMLAH;
					
			//AMBIL JUMLAH 2
			$JUMLAH2 = $cek2->select("P.JUMLAH AS JUMLAH2",
				"WHERE P.TAGIHAN_ID = '$TAGIHANS_ID' AND PEMBAYARAN_ID = '$P_ID1'");
			$JUMLAH2 = $JUMLAH2->current();
			$SUMJUMLAH2 = $JUMLAH2->JUMLAH2;
			
			if($SUMJUMLAH1 == $SUMJUMLAH2){
				$BACK = array(
					'KLOP' => 'Y'
				);
				$SIMPAN->Updateloop('PEMBAYARAN_ID',$P_ID1,$BACK);
				$SIMPAN->Updateloop('PEMBAYARAN_ID',$PEMBAYARAN_ID,$BACK);
				
				//potong tagihan
				//KURANGI TAGIHAN
				//QUERY TAGIHAN SORT JATUH TEMPO AND KURANG BAYAR != ''
				$JUMLAHTAGIHAN = $cek1->select("T.TAGIHAN_ID, T.KURANG_BAYAR",
				"WHERE T.PELANGGAN_ID = '$PELANGGAN_ID' AND T.KURANG_BAYAR IS NOT NULL AND T.KURANG_BAYAR != '0'
				AND T.NOTAGIHAN != '' AND TAGIHAN_ID = '$TAGIHANS_ID'  ORDER BY JATUH_TEMPO ASC LIMIT 0,1");
				foreach($JUMLAHTAGIHAN as $CEKTAGIHANX){
					
					$KURANGBAYAR = $CEKTAGIHANX->KURANG_BAYAR;
					$TAGIHANBAYAR_ID = $CEKTAGIHANX->TAGIHAN_ID;
					//$SISA = $KURANGBAYAR - $JUMLAH;
					$SISA = $JUMLAH - $KURANGBAYAR;
					//JIKA ADA SISA
					if($SISA > 0){
						$JUMLAHBAYAR = $KURANGBAYAR;
						//PERULANGAN BERULANG TERUS
						//UPDATE KURANG BAYAR
						$BACK = array(
							'KURANG_BAYAR' => '0'
						);
						$SIMPAN2->Updateloop('TAGIHAN_ID',$TAGIHANBAYAR_ID,$BACK);
						
						$BACK2 = array(
							'SALDO' => $SISA,
							'KEMBAR_PEMBAYARAN_ID' => $PEMBAYARAN_ID
						);
						$SIMPAN->Updateloop('PEMBAYARAN_ID',$P_ID1,$BACK2);
						
						$BACK2 = array(
							'SALDO' => $SISA,
							'KEMBAR_PEMBAYARAN_ID' => $P_ID1
						);
						$SIMPAN->Updateloop('PEMBAYARAN_ID',$PEMBAYARAN_ID,$BACK2);
					}ELSE{
						$JUMLAHBAYAR = $JUMLAH;
						
						$trans2 = array("-" => "");
		                $SISA = strtr($SISA, $trans2);
						$BACK = array(
							'KURANG_BAYAR' => $SISA
						);
						$SIMPAN2->Updateloop('TAGIHAN_ID',$TAGIHANBAYAR_ID,$BACK);
						
						$BACK2 = array(
							'SALDO' => 0,
							'KEMBAR_PEMBAYARAN_ID' => $PEMBAYARAN_ID
						);						
						$SIMPAN->Updateloop('PEMBAYARAN_ID',$P_ID1,$BACK2);

												
						$BACK2 = array(
							'SALDO' => 0,
							'KEMBAR_PEMBAYARAN_ID' => $P_ID1
						);
						$SIMPAN->Updateloop('PEMBAYARAN_ID',$PEMBAYARAN_ID,$BACK2);	
					
					}
					$ARRAY_FIELD = array(
						'BAYARTAGIHAN_ID' => $SIMPAN3->GenId(),
						'TAGIHAN_ID' => $TAGIHANBAYAR_ID,
						'PEMBAYARAN_ID' => $P_ID1,
						'JUMLAH' => $JUMLAHBAYAR
					);
					$SIMPAN3->AddLoop($ARRAY_FIELD);
				}
			}
			/*
			//JIKA KLOP
			if($SUMJUMLAH1 == $SUMJUMLAH2){
				$BACK = array(
					'KLOP' => 'Y'
				);
				$SIMPAN->Updateloop('PEMBAYARAN_ID',$P_ID1,$BACK);
				$SIMPAN->Updateloop('PEMBAYARAN_ID',$PEMBAYARAN_ID,$BACK);
				
				//potong tagihan
				//KURANGI TAGIHAN
				//QUERY TAGIHAN SORT JATUH TEMPO AND KURANG BAYAR != ''
				$JUMLAHTAGIHAN = $cek1->select("T.TAGIHAN_ID, T.KURANG_BAYAR",
				"WHERE T.PELANGGAN_ID = '$PELANGGAN_ID' AND T.KURANG_BAYAR IS NOT NULL AND T.KURANG_BAYAR != '0'
				AND T.NOTAGIHAN != '' ORDER BY JATUH_TEMPO ASC");
				
				foreach($JUMLAHTAGIHAN as $CEKTAGIHANX){
					//for ($i= 1; $i <= $JUMLAHTAGIHAN; $i++){
					$KURANGBAYAR = $CEKTAGIHANX->KURANG_BAYAR;
					$TAGIHANBAYAR_ID = $CEKTAGIHANX->TAGIHAN_ID;
					//$SISA = $KURANGBAYAR - $JUMLAH;
					$SISA = $JUMLAH - $KURANGBAYAR;
					//JIKA ADA SISA
					if($SISA > 0){
						//PERULANGAN BERULANG TERUS
						//UPDATE KURANG BAYAR
						$BACK = array(
							'KURANG_BAYAR' => '0'
						);
						$SIMPAN2->Updateloop('TAGIHAN_ID',$TAGIHANBAYAR_ID,$BACK);
						
							$BACK2 = array(
								'SALDO' => $JUMLAH,
								'KEMBAR_PEMBAYARAN_ID' => $PEMBAYARAN_ID
							);
							$SIMPAN->Updateloop('PEMBAYARAN_ID',$P_ID2,$BACK2);
							
							$BACK2 = array(
								'SALDO' => $JUMLAH,
								'KEMBAR_PEMBAYARAN_ID' => $P_ID2
							);
							$SIMPAN->Updateloop('PEMBAYARAN_ID',$PEMBAYARAN_ID,$BACK2);
					}else{
						//PERULANGAN BERHENTI
						//UPDATE KURANG BAYAR
						$SISA = str_replace(array('['-']'), '',$SISA);
						$BACK = array(
							'KURANG_BAYAR' => $SISA
						);
						$SIMPAN2->Updateloop('TAGIHAN_ID',$TAGIHANBAYAR_ID,$BACK);
						
						$BACK2 = array(
							'SALDO' => 0,
							'KEMBAR_PEMBAYARAN_ID' => $PEMBAYARAN_ID
						);						
						$SIMPAN->Updateloop('PEMBAYARAN_ID',$P_ID2,$BACK2);

												
						$BACK2 = array(
							'SALDO' => 0,
							'KEMBAR_PEMBAYARAN_ID' => $P_ID2
						);
						$SIMPAN->Updateloop('PEMBAYARAN_ID',$PEMBAYARAN_ID,$BACK2);						
					}
					
					$ARRAY_FIELD = array(
						'BAYARTAGIHAN_ID' => $SIMPAN3->GenId(),
						'TAGIHAN_ID' => $TAGIHANBAYAR_ID,
						'PEMBAYARAN_ID' => $P_ID2,
						'JUMLAH' => $JUMLAHBAYAR
					);
					$SIMPAN3->AddLoop($ARRAY_FIELD);
				}	
			} else{
				//data belum klop
			}
			*/
		}
/*
		$UPDATE1 = array(
			'PELANGGAN_ID' => $PELANGGAN_ID,
			'METODE_ID' => $METODE_ID,
			'TAGIHAN_ID' => $TAGIHANS_ID,
			'TGL_DIBAYAR' => $TGL_BAYAR,
			'TGL_JTH_TEMPO' => $TGL_TEMPO,
			'JENIS_BAYAR' => $BAYAR,
			'STATUS_BAYAR' => '',
			'JUMLAH' => $JUMLAH,
			'SALDO' => '0',
			'BANK' => $BANK,
			'NOGIRO' => $GIRO,
			'NOACC' => $ACC,
			'KLOP' => 'T',
			'KEMBAR_PEMBAYARAN_ID' => $P_ID1,
			'USERNAME' => $USERNAME
		);
		$SIMPAN->Updateloop('PEMBAYARAN_ID',$PEMBAYARAN_ID,$UPDATE1);
		
		
		//CEK JIKA SUDAH ADA 2 ORANG INPUT
		$querycek2 = $cek2->select("P.PEMBAYARAN_ID,P.USERNAME",
		"WHERE P.TAGIHAN_ID =  '$TAGIHANS_ID'
		AND P.KLOP = 'T' AND USERBAYAR != '$USERBAYAR'");
		$CEKKLOP2= $querycek2->current();
		$USERKLOP1 = $CEKKLOP2->USERNAME;
		$P_ID1 = $CEKKLOP2->PEMBAYARAN_ID;
		$KLOP1 = $querycek2->num_rows();
		//jika hanya ada 1 orang yg input
		if($KLOP1 == '0'){
			//do nothing
		
		}else{
			//jika sudah ada 2
			//cek klop
			//AMBIL JUMLAH 1
			$JUMLAH1 = $cek2->select("P.JUMLAH",
				"WHERE P.TAGIHAN_ID = '$TAGIHANS_ID'  AND PEMBAYARAN_ID = '$PEMBAYARAN_ID'");
			$JUMLAH1 = $JUMLAH1->current();
			$SUMJUMLAH1 = $JUMLAH1->JUMLAH;
					
			//AMBIL JUMLAH 2
			$JUMLAH2 = $cek2->select("P.JUMLAH AS JUMLAH2",
				"WHERE P.TAGIHAN_ID = '$TAGIHANS_ID' AND PEMBAYARAN_ID = '$P_ID1'");
			$JUMLAH2 = $JUMLAH2->current();
			$SUMJUMLAH2 = $JUMLAH2->JUMLAH2;
		
			//JIKA KLOP
			if($SUMJUMLAH1 == $SUMJUMLAH2){
				$BACK = array(
					'KLOP' => 'Y'
				);
				$SIMPAN->Updateloop('PEMBAYARAN_ID',$P_ID1,$BACK);
				$SIMPAN->Updateloop('PEMBAYARAN_ID',$PEMBAYARAN_ID,$BACK);
				
				//potong tagihan
				//KURANGI TAGIHAN
				//QUERY TAGIHAN SORT JATUH TEMPO AND KURANG BAYAR != ''
				$JUMLAHTAGIHAN = $cek1->select("T.TAGIHAN_ID, T.KURANG_BAYAR",
				"WHERE T.PELANGGAN_ID = '$PELANGGAN_ID' AND T.KURANG_BAYAR IS NOT NULL AND T.KURANG_BAYAR != '0'
				AND T.NOTAGIHAN != '' ORDER BY JATUH_TEMPO ASC");
				
				foreach($JUMLAHTAGIHAN as $CEKTAGIHANX){
					//for ($i= 1; $i <= $JUMLAHTAGIHAN; $i++){
					$KURANGBAYAR = $CEKTAGIHANX->KURANG_BAYAR;
					$TAGIHANBAYAR_ID = $CEKTAGIHANX->TAGIHAN_ID;
					//$SISA = $KURANGBAYAR - $JUMLAH;
					$SISA = $JUMLAH - $KURANGBAYAR;
					//JIKA ADA SISA
					if($SISA > 0){
						//PERULANGAN BERULANG TERUS
						//UPDATE KURANG BAYAR
						$BACK = array(
							'KURANG_BAYAR' => '0'
						);
						$SIMPAN2->Updateloop('TAGIHAN_ID',$TAGIHANBAYAR_ID,$BACK);
						
							$BACK2 = array(
								'SALDO' => $JUMLAH,
								'KEMBAR_PEMBAYARAN_ID' => $PEMBAYARAN_ID
							);
							$SIMPAN->Updateloop('PEMBAYARAN_ID',$P_ID2,$BACK2);
							
							$BACK2 = array(
								'SALDO' => $JUMLAH,
								'KEMBAR_PEMBAYARAN_ID' => $P_ID2
							);
							$SIMPAN->Updateloop('PEMBAYARAN_ID',$PEMBAYARAN_ID,$BACK2);
					}else{
						//PERULANGAN BERHENTI
						//UPDATE KURANG BAYAR

						$SISA = str_replace(array('['-']'), '',$SISA);
						$BACK = array(
							'KURANG_BAYAR' => $SISA
						);
						$SIMPAN2->Updateloop('TAGIHAN_ID',$TAGIHANBAYAR_ID,$BACK);
						
						$BACK2 = array(
							'SALDO' => 0,
							'KEMBAR_PEMBAYARAN_ID' => $PEMBAYARAN_ID
						);						
						$SIMPAN->Updateloop('PEMBAYARAN_ID',$P_ID2,$BACK2);

												
						$BACK2 = array(
							'SALDO' => 0,
							'KEMBAR_PEMBAYARAN_ID' => $P_ID2
						);
						$SIMPAN->Updateloop('PEMBAYARAN_ID',$PEMBAYARAN_ID,$BACK2);						
					}
					
					$ARRAY_FIELD = array(
						'BAYARTAGIHAN_ID' => $SIMPAN3->GenId(),
						'TAGIHAN_ID' => $TAGIHANBAYAR_ID,
						'PEMBAYARAN_ID' => $P_ID2,
						'JUMLAH' => $JUMLAHBAYAR
					);
					$SIMPAN3->AddLoop($ARRAY_FIELD);
				}	
			} else{
				//data belum klop
			}
		}*/
	}
}
?>
