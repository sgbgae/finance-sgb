<?php
//include_once '../../_conn/query.php';

include_once '_conn/query.php';

class crud{

	protected $_NAMA_DB;
	protected $_NAMA_TABEL;
	//protected $_NAMA_FIELD_FINDBY;
	//protected $_VALUE_FIELD_FINDBY;
	protected $_NUM_ROW;
	

function __construct($NAMA_DB,$NAMA_TABEL){
		$this->_NAMA_DB = $NAMA_DB;
		$this->_NAMA_TABEL = $NAMA_TABEL;

		$this->_DB_CONN = new query($this->_NAMA_DB,$this->_NAMA_TABEL);
		
	}
	
public function GenId(){

		$gen_id = new generateid($this->_NAMA_TABEL);
		$id = $gen_id->generate_id($this->_NAMA_DB);
		return $id;
		
	}	

public function CheckValue($SELECT,$WHERE,$VALUE){

		//$currentNama = $this->_DB_CONN->selectBy($SELECT,$WHERE." = "."'".$VALUE."'"." AND SEMBUNYI = 'T' ");
		$currentNama = $this->_DB_CONN->selectBy($SELECT,$WHERE." = "."'".$VALUE."'"."");
		$this->_NUM_ROW = $currentNama->num_rows();
		return $this->_NUM_ROW;
	}	
	
public function Add($ARRAY_FIELD,$URL_CALLBACK){

		try{
		
			$this->_DB_CONN->save($ARRAY_FIELD);
			echo $URL_CALLBACK;
			
			exit;
		}catch(Exception $e){
			echo 'Gagal Menyimpan data';
			echo '<br/>Error: '.$e->getMessage();
		}
	}	

public function AddLoop($ARRAY_FIELD){

		try{
			$this->_DB_CONN->save($ARRAY_FIELD);
			//echo $URL_CALLBACK;
			
			//exit;
		}catch(Exception $e){
			echo 'Gagal Menyimpan data';
			echo '<br/>Error: '.$e->getMessage();
		}
	}	


public function Delete($ID_DEL){

		try{
			$this->_DB_CONN->updateBy($ID_DEL, $_POST['id']);
			
			exit;
		}catch(Exception $e){
			echo "Gagal menghapus data";
			echo '<br/>Error: '.$e->getMessage();
		}	
	
	}
	
public function Update($ID_UPDATE,$ID_VAL,$ARRAY_FIELD,$URL_CALLBACK){

		try{
		
		$this->_DB_CONN->updateBy($ID_UPDATE, $ID_VAL,$ARRAY_FIELD);
		
		echo $URL_CALLBACK;				
		exit;
	}catch(Exception $e){
		
		echo '<br/>Error: '.$e->getMessage();
	}
	}
	
public function Updateloop($ID_UPDATE,$ID_VAL,$ARRAY_FIELD){

		try{
		
		$this->_DB_CONN->updateBy($ID_UPDATE, $ID_VAL,$ARRAY_FIELD);
		
		//echo $URL_CALLBACK;				
		//exit;
	}catch(Exception $e){
		
		echo '<br/>Error: '.$e->getMessage();
	}
	}
		
}


class periode{
	public $_tglmulai;
	public $_tglselesai;

	function __construct($tglmulai,$tglselesai){
			$this->_tglmulai = $tglmulai;
			$this->_tglselesai = $tglselesai;
	}
	function tagihan($HrPeriode){
		$tglmulai = $this->_tglmulai;
		$daysE['Sunday'] = 1;
		$daysE['Monday'] = 2;
		$daysE['Tuesday'] = 3;
		$daysE['Wednesday'] = 4;
		$daysE['Thursday'] = 5;
		$daysE['Friday'] = 6;
		$daysE['Saturday'] = 7;
		$HrSkrg = $daysE[date('l')];
		if($daysE[date('l', strtotime($tglmulai))]>$HrPeriode){
			$day = $daysE[date('l', strtotime($tglmulai))]-$HrPeriode-1;
			//echo $day." ".date("Y-m-d", strtotime("- ".$day." day", strtotime($tglmulai)))." - ".$tglmulai."<br>";
		} else {
			$day = 6-($HrPeriode-$daysE[date('l', strtotime($tglmulai))]);
			//ECHO $day." ".date("Y-m-d", strtotime("- ".$day." day", strtotime($tglmulai)))." - ".$tglmulai."<BR>";
		}
		return date("Y-m-d", strtotime("- ".$day." day", strtotime($tglmulai)));
	}
	function mingguan(){
		$tglmulai = $this->_tglmulai;
		$tglselesai = $this->_tglselesai;
		if($tglselesai == ''){ 
			$tglselesai = date("Y-m-d", strtotime("+6 day", strtotime($tglmulai)));
		}
		return array('tglmulai'=>$tglmulai, 'tglselesai'=>$tglselesai);
		
	}	
}
class queryfinance {
	function agenarea(){
		$from = ' PROFILE.PROFILE ,MARKETING.AGEN_AREA, MARKETING.MKT_AREA';
		$where = ' AND MARKETING.AGEN_AREA.TGL_SELESAI IS NULL
		AND MARKETING.AGEN_AREA.KONTAK_ID = PROFILE.PROFILE.PROFILE_ID
		AND MARKETING.MKT_AREA.MKTAREA_ID = MARKETING.AGEN_AREA.MKTAREA_ID
		AND PROFILE.PROFILE.AKTIF = "Y" ';
		return array('from'=>$from, 'where'=>$where);
	}
	
	function produk(){
		$from = 'MASTERGUDANG.MASTER_BARANG , GBJ.PRODUK_VARIAN , GBJ.PRODUK_ARTIKEL, GBJ.MERK';
		$where = " AND MASTERGUDANG.MASTER_BARANG.AKTIF = 'Y' 
			AND GBJ.PRODUK_VARIAN.MASTERBARANG_ID = MASTERGUDANG.MASTER_BARANG.MASTERBARANG_ID
			AND GBJ.PRODUK_ARTIKEL.PRODUKARTIKEL_ID = GBJ.PRODUK_VARIAN.PRODUKARTIKEL_ID
			AND GBJ.MERK.MERK_ID = GBJ.PRODUK_ARTIKEL.MERK_ID ";
		return array('from'=>$from, 'where'=>$where);
	}
	
	function savenotapenjualan($query, $PELANGGAN_ID, $TOP_ID){
		$TANGGAL = date("Y/m/d");
		$WAKTU = date("H:i:s");
		$ceks = new query('FINANCE','SJ');
		// Nota Tagihan Lusin Faktur 
		$querycek2 = $ceks->select("SJ.SJ_ID",
		"LEFT JOIN FINANCE.DETAIL_TAGIHAN ON SJ.SJ_ID = FINANCE.DETAIL_TAGIHAN.SJ_ID 
		INNER JOIN PROFILE.PROFILE ON PROFILE.PROFILE.PROFILE_ID = SJ.PELANGGAN_ID 
		inner join DETAIL_SJ ON DETAIL_SJ.SJ_ID = SJ.SJ_ID
		inner join MASTERGUDANG.MASTER_BARANG ON MASTERGUDANG.MASTER_BARANG.MASTERBARANG_ID = DETAIL_SJ.PRODUK_ID
		inner join MASTERGUDANG.SATUAN ON MASTERGUDANG.SATUAN.SATUAN_ID = MASTERGUDANG.MASTER_BARANG.SATUANHITUNG_ID
		WHERE PROFILE.PROFILE.PROFILE_ID = '$PELANGGAN_ID'
		AND NOT EXISTS (SELECT * FROM DETAIL_TAGIHAN WHERE FINANCE.DETAIL_TAGIHAN.SJ_ID = SJ.SJ_ID)
		AND SUBSTRING(SJ.NOFAKTUR,1,1) = '0' AND MASTERGUDANG.SATUAN.NAMA_SATUAN = 'LUSIN' ".$query);
		$jmldata2     =  $querycek2->num_rows();
		$SIMPANDETAILTAG = new crud("FINANCE","DETAIL_TAGIHAN");

		//jika tidak ada maka tidak simpan
		if($jmldata2 == '0'){}
		else{
			//SIMPAN TAGIHAN
			$FIND = new query("FINANCE","TAGIHAN");
			$SIMPANTAG = new crud("FINANCE","TAGIHAN");
			$ARRAY_FIELD1 = array(
				'TAGIHAN_ID' => $SIMPANTAG->GenId(),
				'TANGGAL' => $TANGGAL,
				'WAKTU' => $WAKTU,
				'NONOTA' => $TANGGAL."1",
				'PELANGGAN_ID' => $PELANGGAN_ID,
				'MASTER' => '1',
				'TOP_ID' => $TOP_ID
			);
		
			$SIMPANTAG->AddLoop($ARRAY_FIELD1);
			$TANGGAL1 = $TANGGAL."1";	
			$TAG_ID1 = $FIND->findBy('TAGIHAN_ID','NONOTA',$TANGGAL1);
			$TAG_ID1 = $TAG_ID1->current();
			foreach($querycek2 as $query2){
				$ARRAY_FIELD13 = array(
				'DETAIL_ID' => $SIMPANDETAILTAG->GenId(),
				'TAGIHAN_ID' => $TAG_ID1->TAGIHAN_ID,
				'SJ_ID' => $query2->SJ_ID
				);
			
				$SIMPANDETAILTAG->AddLoop($ARRAY_FIELD13);
			}
			//NORMALISASI NO NOTE
			$BACK4 = array(
				'NONOTA' => ''
			);
			$SIMPANTAG->Updateloop('NONOTA',$TANGGAL1,$BACK4);
		}

		// Nota Tagihan Pasang Faktur 
		$querycek3 = $ceks->select("SJ.SJ_ID",
		"LEFT JOIN FINANCE.DETAIL_TAGIHAN ON SJ.SJ_ID = FINANCE.DETAIL_TAGIHAN.SJ_ID 
		INNER JOIN PROFILE.PROFILE ON PROFILE.PROFILE.PROFILE_ID = SJ.PELANGGAN_ID 
		inner join DETAIL_SJ ON DETAIL_SJ.SJ_ID = SJ.SJ_ID
		inner join MASTERGUDANG.MASTER_BARANG ON MASTERGUDANG.MASTER_BARANG.MASTERBARANG_ID = DETAIL_SJ.PRODUK_ID
		inner join MASTERGUDANG.SATUAN ON MASTERGUDANG.SATUAN.SATUAN_ID = MASTERGUDANG.MASTER_BARANG.SATUANHITUNG_ID
		WHERE PROFILE.PROFILE.PROFILE_ID = '$PELANGGAN_ID'
		AND NOT EXISTS (SELECT * FROM DETAIL_TAGIHAN WHERE FINANCE.DETAIL_TAGIHAN.SJ_ID =SJ.SJ_ID)
		AND SUBSTRING(SJ.NOFAKTUR,1,1) = '0' AND MASTERGUDANG.SATUAN.NAMA_SATUAN <> 'LUSIN' ".$query);
		$jmldata3    =  $querycek3->num_rows();
		$SIMPANDETAILTAG3 = new crud("FINANCE","DETAIL_TAGIHAN");
	
		//jika tidak ada maka tidak simpan
		if($jmldata3 == '0'){}
		else{
			//SIMPAN TAGIHAN
			$FIND3 = new query("FINANCE","TAGIHAN");
			$SIMPANTAG3 = new crud("FINANCE","TAGIHAN");
			$ARRAY_FIELD3 = array(
				'TAGIHAN_ID' => $SIMPANTAG3->GenId(),
				'TANGGAL' => $TANGGAL,
				'WAKTU' => $WAKTU,
				'NONOTA' => $TANGGAL."2",
				'PELANGGAN_ID' => $PELANGGAN_ID,
				'MASTER' => '1',
				'TOP_ID' => $TOP_ID
			);
			$TANGGAL2 = $TANGGAL."2";			
			$SIMPANTAG3->AddLoop($ARRAY_FIELD3);
			$TAG_ID3  = $FIND3->findBy('TAGIHAN_ID','NONOTA',$TANGGAL2);
			$TAG_ID3 = $TAG_ID3->current();
			foreach($querycek3 as $query3){
				$ARRAY_FIELD12 = array(
				'DETAIL_ID' => $SIMPANDETAILTAG3->GenId(),
				'TAGIHAN_ID' => $TAG_ID3->TAGIHAN_ID,
				'SJ_ID' => $query3->SJ_ID
				);
				
				$SIMPANDETAILTAG3->AddLoop($ARRAY_FIELD12);
			}
			//NORMALISASI NO NOTE
			$BACK3 = array(
				'NONOTA' => ''
			);
			$SIMPANTAG3->Updateloop('NONOTA',$TANGGAL2,$BACK3);
		
		}
		
		// Nota Tagihan Lusin Non Faktur
		$querycek4 = $ceks->select("SJ.SJ_ID",
		"LEFT JOIN FINANCE.DETAIL_TAGIHAN ON SJ.SJ_ID = FINANCE.DETAIL_TAGIHAN.SJ_ID 
		INNER JOIN PROFILE.PROFILE ON PROFILE.PROFILE.PROFILE_ID = SJ.PELANGGAN_ID 
		inner join DETAIL_SJ ON DETAIL_SJ.SJ_ID = SJ.SJ_ID
		inner join MASTERGUDANG.MASTER_BARANG ON MASTERGUDANG.MASTER_BARANG.MASTERBARANG_ID = DETAIL_SJ.PRODUK_ID
		inner join MASTERGUDANG.SATUAN ON MASTERGUDANG.SATUAN.SATUAN_ID = MASTERGUDANG.MASTER_BARANG.SATUANHITUNG_ID
		WHERE PROFILE.PROFILE.PROFILE_ID = '$PELANGGAN_ID'
		AND NOT EXISTS (SELECT * FROM DETAIL_TAGIHAN WHERE FINANCE.DETAIL_TAGIHAN.SJ_ID = SJ.SJ_ID)
		AND SUBSTRING(SJ.NOFAKTUR,1,1) != '0' AND MASTERGUDANG.SATUAN.NAMA_SATUAN = 'LUSIN' ".$query);
		$jmldata4   =  $querycek4->num_rows();
		
		//echo $querycek4->printquery()."<br><br>";
		//EXIT;
		$SIMPANDETAILTAG4 = new crud("FINANCE","DETAIL_TAGIHAN");
		//jika tidak ada maka tidak simpan
		if($jmldata4 == '0'){}
		else{
		//SIMPAN TAGIHAN
			$FIND4 = new query("FINANCE","TAGIHAN");
			$SIMPANTAG4 = new crud("FINANCE","TAGIHAN");
			$ARRAY_FIELD4 = array(
				'TAGIHAN_ID' => $SIMPANTAG4->GenId(),
				'TANGGAL' => $TANGGAL,
				'WAKTU' => $WAKTU,
				'NONOTA' => $TANGGAL."3",
				'PELANGGAN_ID' => $PELANGGAN_ID,
				'MASTER' => '1',
				'TOP_ID' => $TOP_ID
			);
			$TANGGAL3 = $TANGGAL."3";		
			$SIMPANTAG4->AddLoop($ARRAY_FIELD4);
			$TAG_ID4  = $FIND4->findBy('TAGIHAN_ID','NONOTA',$TANGGAL3);
			$TAG_ID4 = $TAG_ID4->current();
		
			foreach($querycek4 as $query4){
				$ARRAY_FIELD11 = array(
				   'DETAIL_ID' => $SIMPANDETAILTAG4->GenId(),
					'TAGIHAN_ID' => $TAG_ID4->TAGIHAN_ID,
					'SJ_ID' => $query4->SJ_ID
				);
				$SIMPANDETAILTAG4->AddLoop($ARRAY_FIELD11);
			}
			//NORMALISASI NO NOTE
			$BACK2 = array(
				'NONOTA' => ''
			);
			$SIMPANTAG4->Updateloop('NONOTA',$TANGGAL3,$BACK2);
		}
			
		// Nota Tagihan Pasang Non Faktur 
		$querycek5 = $ceks->select("SJ.SJ_ID",
		"LEFT JOIN FINANCE.DETAIL_TAGIHAN ON SJ.SJ_ID = FINANCE.DETAIL_TAGIHAN.SJ_ID 
		INNER JOIN PROFILE.PROFILE ON PROFILE.PROFILE.PROFILE_ID = SJ.PELANGGAN_ID 
		inner join DETAIL_SJ ON DETAIL_SJ.SJ_ID = SJ.SJ_ID
		inner join MASTERGUDANG.MASTER_BARANG ON MASTERGUDANG.MASTER_BARANG.MASTERBARANG_ID = DETAIL_SJ.PRODUK_ID
		inner join MASTERGUDANG.SATUAN ON MASTERGUDANG.SATUAN.SATUAN_ID = MASTERGUDANG.MASTER_BARANG.SATUANHITUNG_ID
		WHERE PROFILE.PROFILE.PROFILE_ID = '$PELANGGAN_ID'
		AND NOT EXISTS (SELECT * FROM DETAIL_TAGIHAN WHERE FINANCE.DETAIL_TAGIHAN.SJ_ID = SJ.SJ_ID)
		AND SUBSTRING(SJ.NOFAKTUR,1,1) != '0' AND MASTERGUDANG.SATUAN.NAMA_SATUAN <> 'LUSIN' ".$query);
		$jmldata5  =  $querycek5->num_rows();
		$SIMPANDETAILTAG5 = new crud("FINANCE","DETAIL_TAGIHAN");
	
		//jika tidak ada maka tidak simpan
		if($jmldata5 == '0'){}
		else{
			//SIMPAN TAGIHAN
			$FIND5 = new query("FINANCE","TAGIHAN");
			$SIMPANTAG5 = new crud("FINANCE","TAGIHAN");
			$ARRAY_FIELD5 = array(
				'TAGIHAN_ID' => $SIMPANTAG5->GenId(),
				'TANGGAL' => $TANGGAL,
				'WAKTU' => $WAKTU,
				'NONOTA' => $TANGGAL."4",
				'PELANGGAN_ID' => $PELANGGAN_ID,
				'MASTER' => '1',
				'TOP_ID' => $TOP_ID
			);
			$TANGGAL4 = $TANGGAL."4";
			$SIMPANTAG5->AddLoop($ARRAY_FIELD5);
			$TAG_ID5  = $FIND5->findBy('TAGIHAN_ID','NONOTA',$TANGGAL4);
			$TAG_ID5 = $TAG_ID5->current();
			//SIMPAN DETAIL TAGIHAN
			foreach($querycek5 as $query5){
				$ARRAY_FIELD10 = array(
				'DETAIL_ID' => $SIMPANDETAILTAG5->GenId(),
				'TAGIHAN_ID' => $TAG_ID5->TAGIHAN_ID,
				'SJ_ID' => $query5->SJ_ID
				);
			
				$SIMPANDETAILTAG5->AddLoop($ARRAY_FIELD10);
			}
			//NORMALISASI NO NOTE
			$BACK = array(
				'NONOTA' => ''
			);
			$SIMPANTAG5->Updateloop('NONOTA',$TANGGAL4,$BACK);
	
		}
	}
}

class timeto{
	public $date5;
	public $date6;
	public $periode1;
	public $periode2;
	public $expire1;
	public $expire2;
	public function tglAkhirBulan($thn,$bln){
				$bulan['01']='31';
				$bulan['02']='28';
				$bulan['03']='31';
				$bulan['04']='30';
				$bulan['05']='31';
				$bulan['06']='30';
				$bulan['07']='31';
				$bulan['08']='31';
				$bulan['09']='30';
				$bulan['10']='31';
				$bulan['11']='30';
				$bulan['12']='31';
				if ($thn%4==0){
					$bulan[2]=29;
				}
				return $bulan[$bln];
	}
}
class alert{
	
	function bg(){
		$table_bg= new query('FINANCE','FINANCE.TAGIHAN ');
		$querybg = $table_bg->selectBy("COUNT(TAGIHAN_ID) AS JUMLAH, 'Sudah Jatuh Tempo' AS NAMA"
		,"NOTAGIHAN <> '' AND NOTAGIHAN IS NOT NULL
		AND KURANG_BAYAR <> 0 AND JATUH_TEMPO < CURDATE()
		UNION ALL
		SELECT COUNT(TAGIHAN_ID) AS JUMLAH, 'Belum Jatuh Tempo' AS NAMA
		FROM FINANCE.TAGIHAN 
		WHERE NOTAGIHAN <> '' AND NOTAGIHAN IS NOT NULL
		AND KURANG_BAYAR <> 0 AND JATUH_TEMPO > CURDATE()");
		
		$num = $querybg->num_rows();
		$i = 0;
		$alertbg = '';
		$bgview = '';
		foreach($querybg as $querybgcurrent){
			$i = $i+1;
			$label = '<span class="label label-danger">'.$i.'</span>';
//			$selisih = $querybgcurrent->SELISIH;
//			if($selisih <= 10){ $label = '<span class="label label-danger">'.$selisih.'</span>'; }
//			else { $label = '<span class="label label-warning">'.$selisih.'</span>'; }
			$alertbg .=  '<li>'.$label." ".$querybgcurrent->NAMA."</li><br>";
			$bgview .= '<tr>';
			$bgview .= '<td>'.$querybgcurrent->NAMA.' '.$label.'</td>';
//			$bgview .= '<td>'.date("d M Y", strtotime($querybgcurrent->TGL_SELESAI)).'</td>';
			$bgview .= '</tr>';
		}
		return array('num'=>$num, 'alertbg'=>$alertbg, 'bgview'=>$bgview);
	}

	function top5jt(){
		$table= new query('FINANCE','FINANCE.TAGIHAN
		INNER JOIN PROFILE.PROFILE ON PROFILE.PROFILE.PROFILE_ID = FINANCE.TAGIHAN.PELANGGAN_ID'); 
		$query_top5jt = $table->selectBy("COUNT(PELANGGAN_ID) AS JUMLAHJT, PROFILE.PROFILE.NAMA"
		,"NOTAGIHAN <> '' AND NOTAGIHAN IS NOT NULL
		AND KURANG_BAYAR <> 0 AND JATUH_TEMPO < CURDATE()
		GROUP BY PROFILE.PROFILE.NAMA
		ORDER BY COUNT(PELANGGAN_ID) DESC
		LIMIT 0,5");
		$i = 0;
		$data2 = '';
		$data3 = '';
		foreach($query_top5jt as $top5jtcurrent){
			$i = $i+1;
			$nama = $top5jtcurrent->NAMA;
			$jumlahjt = $top5jtcurrent->JUMLAHJT;
			/*$data2 .= 'data2['.$i.'] = {label: "'.$nama.'",data: '.$jumlahjt.'}
';*/
			$data2 .= 'var d'.$i.' = [];
d'.$i.'.push(['.$i.', '.$jumlahjt.']);
';
			$data3 .= '{
                            label: "'.$nama.'",
                            data: d'.$i.',
                            lines: {
                                lineWidth: 1,
                            },
                            shadowSize: 0
                        },
';


			
		}
		return array("data2" => $data2, "data3" => $data3);
		
	}	
	
	function jt(){
		$table= new query('FINANCE','FINANCE.TAGIHAN '); 
		$query_topjt = $table->selectBy("COUNT(TAGIHAN_ID) AS JUMLAH, 'Sudah Jatuh Tempo' AS NAMA"
		,"NOTAGIHAN <> '' AND NOTAGIHAN IS NOT NULL
		AND KURANG_BAYAR <> 0 AND JATUH_TEMPO < CURDATE()
		UNION ALL
		SELECT COUNT(TAGIHAN_ID) AS JUMLAH, 'Belum Jatuh Tempo' AS NAMA
		FROM FINANCE.TAGIHAN 
		WHERE NOTAGIHAN <> '' AND NOTAGIHAN IS NOT NULL
		AND KURANG_BAYAR <> 0 AND JATUH_TEMPO > CURDATE()");
		//echo $query_topjt->printquery();
		$i = 0;
		$data = '';
		foreach($query_topjt as $topjtcurrent){
			$nama = $topjtcurrent->NAMA;
			$jumlah = $topjtcurrent->JUMLAH;
			$data .= 'data['.$i.'] = {label: "'.$nama.'",data: '.$jumlah.'}
';
			$i = $i+1;
		}
		return $data;
	}

	function topbgex(){
		$table= new query('FINANCE','FINANCE.TAGIHAN '); 
		$query_topjt = $table->selectBy("COUNT(TAGIHAN_ID) AS JUMLAH, 'Sudah Jatuh Tempo' AS NAMA"
		,"NOTAGIHAN <> '' AND NOTAGIHAN IS NOT NULL
		AND KURANG_BAYAR <> 0 AND JATUH_TEMPO < CURDATE()
		UNION ALL
		SELECT COUNT(TAGIHAN_ID) AS JUMLAH, 'Belum Jatuh Tempo' AS NAMA
		FROM FINANCE.TAGIHAN 
		WHERE NOTAGIHAN <> '' AND NOTAGIHAN IS NOT NULL
		AND KURANG_BAYAR <> 0 AND JATUH_TEMPO > CURDATE()");
		//echo $query_topjt->printquery();
		$i = 0;
		$data = '';
		foreach($query_topjt as $topjtcurrent){
			$nama = $topjtcurrent->NAMA;
			$jumlah = $topjtcurrent->JUMLAH;
			$data .= 'data['.$i.'] = {label: "'.$nama.'",data: '.$jumlah.'}
';
			$i = $i+1;
		}
		return $data;
	}
	
/*	
	function topbgex(){
		$hariini = date("Y-m-d");
		$table= new query('FINANCE','FINANCE.BANKGARANSI 
			INNER JOIN MARKETING.TOP ON MARKETING.TOP.PELANGGAN_ID = FINANCE.BANKGARANSI.PELANGGAN_ID
			INNER JOIN PROFILE.PROFILE ON PROFILE.PROFILE.PROFILE_ID = FINANCE.BANKGARANSI.PELANGGAN_ID
			INNER JOIN MARKETING.AGEN_AREA ON MARKETING.AGEN_AREA.KONTAK_ID = FINANCE.BANKGARANSI.PELANGGAN_ID
			INNER JOIN MARKETING.MKT_AREA ON MARKETING.MKT_AREA.MKTAREA_ID = MARKETING.AGEN_AREA.MKTAREA_ID'); 
		$query = $table->selectBy("DISTINCT PROFILE.PROFILE.PROFILE_ID, CONCAT(PROFILE.PROFILE.NAMA, ' - ', MARKETING.MKT_AREA.AREA) AS NAMA, MAX(FINANCE.BANKGARANSI.TGL_SELESAI), MAX(MARKETING.TOP.TOP), datediff(FINANCE.BANKGARANSI.TGL_SELESAI,'".$hariini."')"
		,"FINANCE.BANKGARANSI.KLOP = 'Y' AND (MARKETING.TOP.TGL_SELESAI >= '".$hariini."' OR MARKETING.TOP.TGL_SELESAI IS NULL)
		AND (datediff(FINANCE.BANKGARANSI.TGL_SELESAI,'".$hariini."') < MARKETING.TOP.TOP 
		)
		GROUP BY PROFILE.PROFILE.PROFILE_ID, PROFILE.PROFILE.NAMA
		ORDER BY 2");
		$view = '';
		foreach($query as $querycurrent){
			$nama = $querycurrent->NAMA;
			$view .= '<tr>';
			$view .= '<td>'.$nama.'</td>';
			//$topview .= '<td>'.date("d M Y", strtotime($queryjtcurrent->JATUH_TEMPO)).'</td>';
			$view .= '</tr>';
		}		
		return $view;
	}
	//*/
}
?>