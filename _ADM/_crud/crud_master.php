<?php
date_default_timezone_set('Asia/Manila');
$act=$_GET['act'];
$action=$_GET['action'];
include_once '_conn/query.php';
include_once 'function.php';
include_once 'modul/generateid.php';
IF ($act == 'AGEN'){
	$NAMA_DB = 'FINANCE';
	$NAMA_TABEL = 'AGEN';
	$URL_CALLBACK = 'm_agen.php';
	$FIND = new query($NAMA_DB,$NAMA_TABEL);
	$exec = new crud($NAMA_DB,$NAMA_TABEL);
	if($_REQUEST['select_hari'] != ''){$periode_add = '1';}
	else {$periode_add = '1';}
	IF ($action == 'UPDATE'){
		$NAMA = $FIND->findBy('count(PELANGGAN_ID) AS NUM,PELANGGAN_ID,PERIODE,KOMPOSISI_PPN','PELANGGAN_ID',$_POST['id'])->current();	
		
		if($NAMA->NUM != 0){
			$ARRAY_FIELD = array(
						'MINGGUAN' => trim(strtoupper($periode_add)),
						'PERIODE' => trim(strtoupper($_REQUEST['select_hari']))
					);
					
			
				$exec->Update('PELANGGAN_ID',$_POST['id'],$ARRAY_FIELD,$URL_CALLBACK);
			
			//}ELSE{
			//		echo "1";
			//}
		}ELSE{
			$ARRAY_FIELD = array( 
				'PELANGGAN_ID' => trim(strtoupper($_REQUEST['id'])),
				'MINGGUAN' => trim(strtoupper($periode_add)),
				'PERIODE' => trim(strtoupper($_REQUEST['select_hari']))
			);
			IF (($exec->CheckValue('PELANGGAN_ID','PELANGGAN_ID',$_REQUEST['id'])) == 0 && ($_REQUEST['id']) != ''){	//$SELECT,$WHERE,$VALUE
				$exec->Add($ARRAY_FIELD,$URL_CALLBACK);
			} else{echo "1";}
		}
	}
} else IF ($act == 'BG'){
	$NAMA_DB = 'FINANCE';
	$NAMA_TABEL = 'BANKGARANSI';
	$URL_CALLBACK = 'm_bg.php';
	$FIND = new query($NAMA_DB,$NAMA_TABEL);
	$exec = new crud($NAMA_DB,$NAMA_TABEL);
	
	IF ($action == 'AMBIL'){ 
		$tglambil = $_REQUEST['date4'];
		$id = $_REQUEST['id2'];
		$ARRAY_FIELD = array(
			'TGL_AMBIL' => $tglambil
		);
		$exec->UpdateLoop('BANKGARANSI_ID',$id,$ARRAY_FIELD);
		//echo "test".$tglambil;
		exit;
	}
	$trans = array("." => "");
	$nominal = number_format(strtr($_REQUEST['nominal'], $trans), 2, '.', ''); //ECHO $nominal;
	IF ($action == 'ADD'){ 
		$NAMA = $FIND->selectBy('count(PELANGGAN_ID) AS NUM, USERNAME, BANKGARANSI_ID','PELANGGAN_ID = "'.trim(strtoupper($_REQUEST['select_agen'])).'" 
			AND (KLOP IS NULL OR KLOP = "") AND TGL_TERBIT = "'.trim(strtoupper($_REQUEST['date1'])).'" 
			AND TGL_MULAI = "'.trim(strtoupper($_REQUEST['date2'])).'" 
			AND TGL_SELESAI = "'.trim(strtoupper($_REQUEST['date3'])).'" 
			AND BANK = "'.trim(strtoupper($_REQUEST['bank'])).'"
			AND CABANG = "'.trim(strtoupper($_REQUEST['cabang'])).'"
			AND NOMER = "'.trim(strtoupper($_REQUEST['nomor'])).'" 
			AND NOMINAL = "'.trim(strtoupper($nominal)).'" ')->current();
		$id = $exec->GenId();
		if($NAMA->NUM == 0){
			$ARRAY_FIELD = array( 
				'BANKGARANSI_ID' => $id,
				'PELANGGAN_ID' => trim(strtoupper($_REQUEST['select_agen'])),
				'BANK' => trim(strtoupper($_REQUEST['bank'])),
				'CABANG' => trim(strtoupper($_REQUEST['cabang'])),
				'NOMINAL' => trim(strtoupper($nominal)),
				'TGL_TERBIT' => trim(strtoupper($_REQUEST['date1'])),
				'TGL_MULAI' => trim(strtoupper($_REQUEST['date2'])),
				'TGL_SELESAI' => trim(strtoupper($_REQUEST['date3'])),
				'NOMER' => trim(strtoupper($_REQUEST['nomor'])),
				'USERNAME' => trim($_REQUEST['alamat_email']),
				'DATE_MODIFIED' => date("Y-m-d H:i:s")
			);
			//IF (($exec->CheckValue('BANKGARANSI_ID','BANKGARANSI_ID',$id)) == 0 && ($id) != ''){	//$SELECT,$WHERE,$VALUE
			mysqli_autocommit($FIND, FALSE);
				$exec->Add($ARRAY_FIELD,$URL_CALLBACK);
			if (!mysqli_commit($FIND)) { echo "1";}
			//} else{echo "1";}
		} else{
			IF($NAMA->USERNAME != trim($_REQUEST['alamat_email'])){
				$ARRAY_FIELD = array(  
					'BANKGARANSI_ID' => $id,
					'PELANGGAN_ID' => trim(strtoupper($_REQUEST['select_agen'])),
					'BANK' => trim(strtoupper($_REQUEST['bank'])),
					'CABANG' => trim(strtoupper($_REQUEST['cabang'])),
					'NOMINAL' => trim(strtoupper($nominal)),
					'TGL_TERBIT' => trim(strtoupper($_REQUEST['date1'])),
					'TGL_MULAI' => trim(strtoupper($_REQUEST['date2'])),
					'TGL_SELESAI' => trim(strtoupper($_REQUEST['date3'])),
					'NOMER' => trim(strtoupper($_REQUEST['nomor'])),
					'KLOP' => 'Y',
					'KEMBAR_BANKGARANSI_ID' => trim(strtoupper($NAMA->BANKGARANSI_ID)),
					'USERNAME' => trim($_REQUEST['alamat_email']),
					'DATE_MODIFIED' => date("Y-m-d H:i:s")
				);
				
				$ARRAY_FIELD2 = array(
					'KLOP' => 'Y',
					'KEMBAR_BANKGARANSI_ID' => $id
				);
				
				
				try{		
					mysqli_autocommit($FIND, FALSE);
					$update = $FIND->updateBy('BANKGARANSI_ID', trim(strtoupper($NAMA->BANKGARANSI_ID)),$ARRAY_FIELD2);
					$exec->Add($ARRAY_FIELD,$URL_CALLBACK);
					$current = $update->current; 
					if (!mysqli_commit($FIND)) { echo "1";}
				}catch(Exception $e){
					echo '1';
				}
			
			}else{echo "1";}
		}
	} else IF ($action == 'UPDATE'){ 
		$NAMA = $FIND->selectBy('count(PELANGGAN_ID) AS NUM, USERNAME, BANKGARANSI_ID','PELANGGAN_ID = "'.trim(strtoupper($_REQUEST['pelangganid'])).'" 
			AND (KLOP IS NULL OR KLOP = "") AND TGL_TERBIT = "'.trim(strtoupper($_REQUEST['date1'])).'" 
			AND TGL_MULAI = "'.trim(strtoupper($_REQUEST['date2'])).'" 
			AND TGL_SELESAI = "'.trim(strtoupper($_REQUEST['date3'])).'" 
			AND BANK = "'.trim(strtoupper($_REQUEST['bank'])).'"
			AND CABANG = "'.trim(strtoupper($_REQUEST['cabang'])).'"
			AND NOMER = "'.trim(strtoupper($_REQUEST['nomor'])).'" 
			AND NOMINAL = "'.trim(strtoupper($nominal)).'" ')->current();
		if($NAMA->NUM == 0){
			$ARRAY_FIELD = array(
				'BANK' => trim(strtoupper($_REQUEST['bank'])),
				'CABANG' => trim(strtoupper($_REQUEST['cabang'])),
				'NOMINAL' => trim(strtoupper($nominal)),
				'TGL_TERBIT' => trim(strtoupper($_REQUEST['date1'])),
				'TGL_MULAI' => trim(strtoupper($_REQUEST['date2'])),
				'TGL_SELESAI' => trim(strtoupper($_REQUEST['date3'])),
				'NOMER' => trim(strtoupper($_REQUEST['nomor'])),
				'USERNAME' => trim($_REQUEST['alamat_email']),
				'DATE_MODIFIED' => date("Y-m-d H:i:s")
			);
			$exec->Update('BANKGARANSI_ID',$_POST['id'],$ARRAY_FIELD,$URL_CALLBACK);
		} else {
			IF($NAMA->USERNAME != trim($_REQUEST['alamat_email'])){
				$ARRAY_FIELD2 = array(
					'KLOP' => 'Y',
					'KEMBAR_BANKGARANSI_ID' => $_POST['id']
				);
				$update = $FIND->updateBy('BANKGARANSI_ID', trim(strtoupper($NAMA->BANKGARANSI_ID)),$ARRAY_FIELD2);
				
				$ARRAY_FIELD = array(
					'BANK' => trim(strtoupper($_REQUEST['bank'])),
					'CABANG' => trim(strtoupper($_REQUEST['cabang'])),
					'NOMINAL' => trim(strtoupper($nominal)),
					'TGL_TERBIT' => trim(strtoupper($_REQUEST['date1'])),
					'TGL_MULAI' => trim(strtoupper($_REQUEST['date2'])),
					'TGL_SELESAI' => trim(strtoupper($_REQUEST['date3'])),
					'NOMER' => trim(strtoupper($_REQUEST['nomor'])),
					'KLOP' => 'Y',
					'KEMBAR_BANKGARANSI_ID' => trim(strtoupper($NAMA->BANKGARANSI_ID)),
					'USERNAME' => trim($_REQUEST['alamat_email']),
					'DATE_MODIFIED' => date("Y-m-d H:i:s")
				);
				$exec->Update('BANKGARANSI_ID',$_POST['id'],$ARRAY_FIELD,$URL_CALLBACK);
			}
		}
	}
}else IF ($act == 'KELOMPOK'){ 
	$NAMA_DB = 'FINANCE';
	$NAMA_TABEL = 'FINANCE.PRODUK';
	$URL_CALLBACK = 'm_kelompok.php';
	$FIND = new query($NAMA_DB,$NAMA_TABEL);
	$exec = new crud($NAMA_DB,$NAMA_TABEL);
	IF ($action == 'ADD'){ 
		$NAMA = $FIND->selectBy('count(PRODUK_ID) AS NUM','PRODUK_ID = "'.trim(strtoupper($_REQUEST['select_produk'])).'" ')->current();
		//$id = $exec->GenId();
		if($NAMA->NUM == 0){
			$ARRAY_FIELD = array( 
				'PRODUK_ID' => trim(strtoupper($_REQUEST['select_produk'])),
				'KELOMPOK_ID' => trim(strtoupper($_REQUEST['select_kelompok']))
			);
			//IF (($exec->CheckValue('BANKGARANSI_ID','BANKGARANSI_ID',$id)) == 0 && ($id) != ''){	//$SELECT,$WHERE,$VALUE
			mysqli_autocommit($FIND, FALSE);
				$exec->Add($ARRAY_FIELD,$URL_CALLBACK);
			if (!mysqli_commit($FIND)) { echo "1";}
		} ELSE {
			echo "1";
		}
	} else IF ($action == 'UPDATE'){ 
		$ARRAY_FIELD = array(
			'KELOMPOK_ID' => trim(strtoupper($_REQUEST['select_kelompok']))
		);
		$exec->Update('PRODUK_ID',$_POST['id'],$ARRAY_FIELD,$URL_CALLBACK);
		
	}
	
}
?>
