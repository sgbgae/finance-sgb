<?php
session_start();

if(!empty($_SESSION['username'])){

?>
<?php 
include_once '_conn/query.php';
include_once 'modul/class_paging.php';

?>
<!DOCTYPE html>
<html lang="en">
	<?php include '../_template/head.php';?>
	<body id="page-top">
<?php include '../_template/navbar_head.php';?>
<?php //include '../_template/navbar_sub.php';?>

<?PHP
/*
//Query
select 
WEEK(A.TANGGAL) AS MINGGUKE 
, CONCAT( DATE_FORMAT( DATE_ADD(A.TANGGAL, INTERVAL(1-DAYOFWEEK(A.TANGGAL)) DAY),'%d-%m-%Y' )
	,' S/D '
	, DATE_FORMAT(DATE_ADD(A.TANGGAL, INTERVAL(7-DAYOFWEEK(A.TANGGAL)) DAY),'%d-%m-%Y' )) AS PERIODE
, DAYOFWEEK(A.TANGGAL) AS HARI 
, C.PROFILE_ID
, C.KODE, C.NAMA
, GBJ.MERK.MERK 
, FORMAT(sum(B.HARGA * B.KARUNG*E.ISI),2) AS TOTAL 
from 
		FINANCE.SJ A
		, FINANCE.DETAIL_SJ B
		, PROFILE.PROFILE C
		, MASTERGUDANG.MASTER_BARANG D
		, MASTERGUDANG.SATUAN E 
	  , GBJ.PRODUK_VARIAN
	  , GBJ.PRODUK_ARTIKEL
	  , GBJ.MERK

where 1=1 
AND A.SJ_ID=B.SJ_ID 
and TRIM(A.PELANGGAN_ID) = C.PROFILE_ID 
and B.PRODUK_ID= D.MASTERBARANG_ID
AND D.SATUAN_ID = E.SATUAN_ID 
and A.NOFAKTUR like '0%'
AND D.MASTERBARANG_ID = GBJ.PRODUK_VARIAN.MASTERBARANG_ID 
AND GBJ.PRODUK_VARIAN.PRODUKARTIKEL_ID  =  GBJ.PRODUK_ARTIKEL.PRODUKARTIKEL_ID 
AND GBJ.PRODUK_ARTIKEL.MERK_ID  =  GBJ.MERK.MERK_ID 
	
group by 1,2,3,4,5,6,7 
ORDER BY 1,3,4,2,7

*/
/*$query1 = "select WEEK(A.TANGGAL) AS MINGGUKE , CONCAT( DATE_FORMAT( DATE_ADD(A.TANGGAL, INTERVAL(1-DAYOFWEEK(A.TANGGAL)) DAY),'%d-%m-%Y' )	,' S/D '	, DATE_FORMAT(DATE_ADD(A.TANGGAL, INTERVAL(7-DAYOFWEEK(A.TANGGAL)) DAY),'%d-%m-%Y' )) AS PERIODE, DAYOFWEEK(A.TANGGAL) AS HARI , C.PROFILE_ID, C.KODE, C.NAMA, GBJ.MERK.MERK , FORMAT(sum(B.HARGA * B.KARUNG*E.ISI),2) AS TOTAL from 	FINANCE.SJ A , FINANCE.DETAIL_SJ B , PROFILE.PROFILE C , MASTERGUDANG.MASTER_BARANG D , MASTERGUDANG.SATUAN E   , GBJ.PRODUK_VARIAN , GBJ.PRODUK_ARTIKEL , GBJ.MERK where 1=1 AND A.SJ_ID=B.SJ_ID  and TRIM(A.PELANGGAN_ID) = C.PROFILE_ID  and B.PRODUK_ID= D.MASTERBARANG_ID AND D.SATUAN_ID = E.SATUAN_ID  and A.NOFAKTUR like '0%'  AND D.MASTERBARANG_ID = GBJ.PRODUK_VARIAN.MASTERBARANG_ID AND GBJ.PRODUK_VARIAN.PRODUKARTIKEL_ID  =  GBJ.PRODUK_ARTIKEL.PRODUKARTIKEL_ID AND GBJ.PRODUK_ARTIKEL.MERK_ID  =  GBJ.MERK.MERK_ID ";*/
$query1 = "select K.KDKELOMPOK AS KELOMPOK,D.NAMA_BARANG AS JENIS_BARANG, FORMAT(SUM( DISTINCT PRODUK_VARIAN.ISI*B.KARUNG/E.ISI*HARGA2)+(SUM( DISTINCT PRODUK_VARIAN.ISI*B.KARUNG/E.ISI*HARGA2)*0.1),2) AS TOTAL from 	FINANCE.SJ A , FINANCE.DETAIL_SJ B LEFT JOIN FINANCE.PRODUK P ON P.PRODUK_ID = B.PRODUK_ID LEFT JOIN FINANCE.KELOMPOK K ON K.KELOMPOK_ID = P.KELOMPOK_ID, PROFILE.PROFILE C , MASTERGUDANG.MASTER_BARANG D , MASTERGUDANG.SATUAN E   , GBJ.PRODUK_VARIAN , GBJ.PRODUK_ARTIKEL , GBJ.MERK where 1=1 AND A.SJ_ID=B.SJ_ID  and TRIM(A.PELANGGAN_ID) = C.PROFILE_ID  and B.PRODUK_ID= D.MASTERBARANG_ID AND D.SATUANHITUNG_ID = E.SATUAN_ID  and A.NOFAKTUR like '0%'  AND D.MASTERBARANG_ID = GBJ.PRODUK_VARIAN.MASTERBARANG_ID AND GBJ.PRODUK_VARIAN.PRODUKARTIKEL_ID  =  GBJ.PRODUK_ARTIKEL.PRODUKARTIKEL_ID AND GBJ.PRODUK_ARTIKEL.MERK_ID  =  GBJ.MERK.MERK_ID ";

$query2 = " AND A.TANGGAL BETWEEN '".date("Y-m-d")."' AND '".date("Y-m-d")."' ";

$query3 = " group by 1,2 ORDER BY 2";

$setrow = ',rows: ["KELOMPOK","JENIS_BARANG", "TOTAL"]';
?>

<!-- CSS YANG DIBUTUHKAN DI PAGE INI SAJA -->
<link rel="stylesheet" href="/js/DataTables/media/css/DT_bootstrap.css" />
<link rel="stylesheet" href="/css/jquery-ui/jquery-ui.css">
<script src="/js/jquery/jquery.min.js"></script>
		<script src="/js/jquery-ui/jquery-ui.js"></script>
		

<!------------------------------------------------------------------------->


<!--main-->

<div class="navbar">
	
	<div class="row">   
	<br><br>
	<!-- BREADCRUMB UNTUK MEMPERLIHATKAN SEDANG BERADA DI MENU APA -->
			<!--<ol class="breadcrumb">
			<li><a href="/index_ADM.php">Home</a></li>
			<li class="">Transaksi</li>
			<li class="active">View Penerimaan</li>
			</ol>-->
	<!------------------------------------------------------------------------->
	
	
     <div class="col-md-12 col-sm-12"> <!-- lg = large, md = medium, sm = small untuk melihat width nya, ada pada bootstrap.css -->

    	<div class="panelblue">
			<span style="float:left;"><a href="/index_ADM.php">Home</a>
			> Laporan
			> <b>Penjualan Rupiah </b>
</span>
		
		
		<!--<div id="view_data" style="display:none;">
           <div class="panel-heading" style="margin-bottom: 0px;"> -->
		   <h3>Laporan Penjualan Rupiah
		
		  <!--
		    <a href="#tambah_data" class="tambah_data" style="font-size:15px;" data-toggle="modal"
			onClick="document.getElementById('judul').innerHTML='Tambah Penerimaan';"
			>
			-->
		
			
		   </h3>
		    <!--</div>
		  
   			<div class="panel-body" style="margin-bottom: 0px;">
                     
					  
						<div class="panel panel-default" style="margin-bottom: 0px;">
								
								<div class="panel-body">-->
									<!--form method="GET" action="http://test.rpt-sgb.appspot.com/gchartcrud"-->
									<FORM METHOD=post ACTION=http://test.rpt-sgb.appspot.com/gchartcrud enctype="multipart/form-data" id="<?php echo $data_form =  'data_upload';?>" >
			
								<div style="padding-bottom:5px;border-bottom:1px solid #dadada;">
								<!--<h4><b>Master Penerimaan</b></h4>-->
								<?php   //echo $search;?>
									<table class="table table-condensed table-hover borderless tableedit" id="biodata">
										<tbody>
											<tr>
												<td class="col-md-4" colspan="4"><strong>Search:</strong></td>
											</tr>
											
										</tbody>
									</table>
									
								</div>
								<!--<div class="input-group"style="padding-bottom:5px;border-bottom:1px solid #dadada;">-->
								
								<div class="input-group" style="padding-bottom:5px;border-bottom:1px solid #dadada;">
									<input type="text" class="form-control hint" style="z-index:1;" placeholder="Pencarian .......... " name="search" id="search" data-container="body" data-placement="bottom" data-content="Untuk pencarian data Pelanggan berdasarkan Nama" autocomplete="off">
								
								
								
								
									
									<div class="input-group-btn" >
										<!--<button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>-->
										<button class="btn btn-default" type="submit"  name="kirim"/><i class="glyphicon glyphicon-search"></i> </button>
										<a href="#" class="btn btn-default" id="up" title="Menampilkan Pencarian" style="display:none"><i class="fa fa-caret-up"></i></a>
										<a href="#" class="btn btn-default" id="down" title="Menampilkan Pencarian" ><i class="fa fa-caret-down"></i></a>

									</div>
								</div>
								<center>
								
								
								</center><br>
								<div id="searchoption" style="padding-bottom:5px;border-bottom:1px solid #dadada;">
									<table class="table table-condensed table-hover borderless tableedit" id="biodata">
										<tbody>
											<tr>
												<td width = "15%"><label><input id="tanggal" name="tanggal" type="checkbox" value="Y" checked> Tanggal</label></td>
												<td width = "30%"><input type="text" placeholder="YYYY/MM/DD" data-date-format="yyyy-mm-dd" data-date-viewmode="years" id="tanggal1" name="mulai" class="form-control date-picker input-sm tanggal_lahir" value="<?php echo date("Y-m-d") ;?>" autocomplete="off"><?php //Datepicker ?>
			</td>
												<td align="center" width = "15%">S/D</td>
												<td width = "30%"><input type="text" placeholder="YYYY/MM/DD" data-date-format="yyyy-mm-dd" data-date-viewmode="years" id="tanggal2" name="selesai" class="form-control date-picker input-sm tanggal_lahir" value="<?php echo date("Y-m-d") ;?>" autocomplete="off"><?php //Datepicker ?>
												<textarea name="SQLSELECT" id="SQLSELECT" cols="20" rows="10" style="display:none"><?PHP echo $query1.$query2.$query3; ?></textarea>
													<textarea name="SETROW" cols="20" rows="10"  style="display:none"><?PHP echo $setrow; ?></textarea>
			</td>	
											</tr>
										</tbody>
									</table>
									
									<div class="input-group-btn">
								
									</div>
								</div>
								
						
								</form>
						
									
									<br><br><br>
									
									<br><br>
								<center>
									
									 
									  <?php
						
									 ?>
								</center>
								
								</div>
						
</div>
</div>

<?php include "../_template/navbar_footer.php"; ?>
</div><!--/main-->



	<!-- javascript yang dibutuhkan untuk halaman ini saja -->
		
		
		
		<script type="text/javascript" src="/js/DataTables/media/js/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="/js/DataTables/media/js/DT_bootstrap.js"></script>
		
		
		
		<!-- ----------------------------------------------------------------------------- -->
		
		
		<!-- ajax untuk insert data menggunakan _modal-and-datatable.js -->
		
		
		
		<script  src="/js/ajax/_modal-and-datatable.js"></script>
		<!--script>
		var submit = <?php //echo "'"."button#".$submit."'" ;?>;
		var data_form = <?php //echo "'"."#".$data_form."'" ;?>;
		var act = 'ADD_ASURANSI';
		var peringatan = 'Penyimpanan gagal, nama kode / jenis sudah ada';
		</script-->
		
		<!-- ----------------------------------------------------------------------------- -->
		

		
		
		<script>
	window.test = function(e){
		
        //alert(e.value);
		//IF(e.value != ''){
		if(sj.value == 'nf'){
			window.location.href = 'pdffinnonfaktur?mulai='+tanggal1.value+'&selesai='+tanggal2.value;
		} else {
			window.location.href = 'pdffinfaktur?mulai='+tanggal1.value+'&selesai='+tanggal2.value;
		}
		//}
}
	$(function() {
	$('#table_custom').dataTable({"bSort": false,"bFilter": false,"bInfo": false,"bLengthChange": false,"bPaginate": false,
});

$( "#tanggal1" ).datepicker({changeMonth: true, changeYear: true, yearRange: "-60:+10", dateFormat:"yy-mm-dd"});
$( "#tanggal2" ).datepicker({changeMonth: true, changeYear: true, yearRange: "-60:+10", dateFormat:"yy-mm-dd"});

var htmlobjek;
	$("#down").click(function(){
		$('#searchoption').show();
		$('#up').show();
		$('#down').hide();
	}); 
	$("#up").click(function(){
		$('#searchoption').hide();
		$('#up').hide();
		$('#down').show();
	}); 
	
	var htmlobjek;
		
	$("#tanggal1").change(function(){
		var query1 = "<?php echo $query1; ?>";
		var query2 = "";
		var tanggal1 = $("#tanggal1").val();
		var tanggal2 = $("#tanggal2").val();
		
		if ($('#tanggal').is(':checked')) {
			var query2 = query2+" AND A.TANGGAL BETWEEN '"+tanggal1+"' AND '"+tanggal2+"' ";
		} else{ var query2 = query2; }
		
		
		var query3 = "<?php echo $query3; ?>";
		$('#SQLSELECT').html(query1+query2+query3);
	});
	$("#tanggal2").change(function(){
		var query1 = "<?php echo $query1; ?>";
		var query2 = "";
		var tanggal1 = $("#tanggal1").val();
		var tanggal2 = $("#tanggal2").val();
		
		if ($('#tanggal').is(':checked')) {
			var query2 = query2+" AND A.TANGGAL BETWEEN '"+tanggal1+"' AND '"+tanggal2+"' ";
		} else{ var query2 = query2; }
		
		
		var query3 = "<?php echo $query3; ?>";
		$('#SQLSELECT').html(query1+query2+query3);
	});
	
	$('#tanggal').change(function() {
		var query1 = "<?php echo $query1; ?>";
		var query2 = "";
		var tanggal1 = $("#tanggal1").val();
		var tanggal2 = $("#tanggal2").val();
		
		if ($('#tanggal').is(':checked')) {
			var query2 = query2+" AND A.TANGGAL BETWEEN '"+tanggal1+"' AND '"+tanggal2+"' ";
		} else{ var query2 = query2; }
		
		
		var query3 = "<?php echo $query3; ?>";
		$('#SQLSELECT').html(query1+query2+query3);
	});
	/*	
	$('#view_data').show();
	act = '';
	action = '';
	peringatan = '';
	 //untuk menampilkan div add_data
	 
	$('#submit_as1').click(function(){
	 
	 var nik= $("#nik").val();
	 var validasi= $("#validasi").val();
	 
  	if(validasi=="") {
						//alert('Field Asuransi tidak boleh kosong!');
						$('.notification_validasi').show();
						$('.notification_validasi').html('Isi NIK Terlebih Dahulu');
						$("#validasi").focus();
						return false;
					}
			else{
			$('.notification_validasi').hide();
			}
			
	if(validasi != nik) {
						//alert('Field Asuransi tidak boleh kosong!');
						$('.notification_validasi').show();
						$('.notification_validasi').html('NIK tidak valid');
						$("#validasi").focus();
						return false;
					}
			else{
			$('.notification_validasi').hide();
			}		
			
	 
			if (confirm('Anda yakin ?')) {
		   	$.ajax({
    		   	type: "POST",
			url: "_ADM/crud_transaksi.php?act=PENERIMAAN&action=VALID",
			data: $('#data_as').serialize(),
        		success: function(msg){
 		        	if(msg==1){
					alert("Status Gagal Disimpan");
					}else{
					window.location.replace("t_viewpenerimaan.php?menu=transaksi");
					}},
			error: function(){
				alert("koneksi bermasalah, silahkan reload halaman");
				}
      			});
			} else {
		return false;
		}
		});

		
		$('#submit_as2').click(function(){
		
		var nik= $("#nik").val();
	 var validasi= $("#validasi").val();
	 
  	if(validasi=="") {
						//alert('Field Asuransi tidak boleh kosong!');
						$('.notification_validasi').show();
						$('.notification_validasi').html('Isi NIK Terlebih Dahulu');
						$("#validasi").focus();
						return false;
					}
			else{
			$('.notification_validasi').hide();
			}
			
	if(validasi != nik) {
						//alert('Field Asuransi tidak boleh kosong!');
						$('.notification_validasi').show();
						$('.notification_validasi').html('NIK tidak valid');
						$("#validasi").focus();
						return false;
					}
			else{
			$('.notification_validasi').hide();
			}		
		
		
			if (confirm('Anda yakin ?')) {
		   	$.ajax({
    		   	type: "POST",
			url: "_ADM/crud_transaksi.php?act=PENERIMAAN&action=DROP",
			data: $('#data_as').serialize(),
        		success: function(msg){
 		        	if(msg==1){
					alert("Status Gagal Disimpan");
					}else{
					window.location.replace("t_viewpenerimaan.php?menu=transaksi");
					}},
			error: function(){
				alert("koneksi bermasalah, silahkan reload halaman");
				}
      			});
			} else {
		return false;
		}
		});
	
	
	
	*/

	

	/** Ajax untuk ADD, DELETE DAN UPDATE DATA **/
	

		
	
	});
		</script>
		
	</body>
</html>
<?php
}
else{
header('location:/lock.php?logingagal');
}
?>