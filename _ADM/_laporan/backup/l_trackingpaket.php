<?php
//ada export to excel
session_start();

if(!empty($_SESSION['username'])){ 

$date1 = date("Y-m-d");
$date2 = date("Y-m-d");
$tanggal = '';
$satuan = '';
$checkedT1 = 'checked';
$checkedT2 = '';
$checkedS1 = 'checked';
$checkedS2 = '';
IF(isset($_REQUEST['tanggal'])){
	$tanggal = $_REQUEST['tanggal'];
	if($tanggal == '2'){$checkedT2 = 'checked';}
	else{$checkedT1 = 'checked';}
}
IF(isset($_REQUEST['satuan'])){
	$satuan = $_REQUEST['satuan'];
	if($satuan == '2'){$checkedS2 = 'checked';}
	else{$checkedS1 = 'checked';}
}
?>
<?php 
include_once '_conn/query.php';
/*$table_jenisp = new query('FINANCE','JENIS_POTONGAN'); //('NAMA DATABASE','NAMA TABEL')
//FORMAT SEPERTI PADA CLASS QUERY, FUNCTION SELECT ('FIELD','WHERE CLAUSE')
$queryjenisp = $table_jenisp->selectBy("KDPOTONGAN, JENIS_POTONGAN","1=1 ORDER BY JENIS_POTONGAN ASC");
//$table_karyawan= new query('HRD','KARYAWAN'); //('NAMA DATABASE','NAMA TABEL')
*/
?>
<!DOCTYPE html>
<html lang="en">
	<?php include '../_template/head.php';?>
	<body id="page-top">
<?php include '../_template/navbar_head.php';?>
<?php include '../_template/navbar_sub.php';?>

<!-- CSS YANG DIBUTUHKAN DI PAGE INI SAJA -->
<link rel="stylesheet" href="/js/DataTables/media/css/DT_bootstrap.css" />
<link rel="stylesheet" href="/css/jquery-ui/jquery-ui.css"> <?php //Datepicker ?>
<script src="/js/jquery/jquery.min.js"></script>
		<script src="/js/jquery-ui/jquery-ui.js"></script>

<!------------------------------------------------------------------------->
<!--main-->
<div class="container" id="main">
	<div class="row">   
	<!-- BREADCRUMB UNTUK MEMPERLIHATKAN SEDANG BERADA DI MENU APA -->
			<ol class="breadcrumb">
			<li><a href="#">Home</a></li>
			<li><a href="#">Laporan</a></li>
			<li class="active">Rekap Surat jalan Faktur</li>
			</ol>
	<!------------------------------------------------------------------------->
     <div class="col-md-12 col-sm-12"> <!-- lg = large, md = medium, sm = small untuk melihat width nya, ada pada bootstrap.css -->
    	<div class="panelblue">
			<div id="view_data">
           		<div class="panel-heading" style="margin-bottom: 0px;"> <h3>Rekap Surat jalan Faktur
					<!--<a href="#" class="tambah_data" style="font-size:15px;"><b style="background-color: #eee;padding: 5px 5px;border-radius: 14px;">+ Tambah data </b></a>
					<a href="#tambah_jpot" style="font-size:15px;" data-toggle="modal"><strong>+ Tambah data</strong></a>--></h3></div>
   					<div class="panel-body" style="margin-bottom: 0px;">
						<div class="panel panel-default" style="margin-bottom: 0px;">
								<div class="panel-body">
								<form method="GET" action="l_sjfaktur.php" >
								<!--<div class="input-group"style="padding-bottom:5px;border-bottom:1px solid #dadada;">-->
								<div style="padding-bottom:5px;border-bottom:1px solid #dadada;">
									<table class="table table-condensed table-hover borderless tableedit" id="biodata">
										<tbody>
											<tr>
												<td class="col-md-4" colspan="4"><strong>Search:</strong></td>
											</tr>
											<tr>
												<td><label><input name="tanggal" type="radio" value="1" <?php echo $checkedT1; ?>> Tanggal</label></td>
												<td><input type="text" placeholder="YYYY/MM/DD" data-date-format="yyyy-mm-dd" data-date-viewmode="years" id="tanggal1" name="date1" class="form-control date-picker input-sm tanggal_lahir" value="<?php echo $date1; ?>" autocomplete="off"><?php //Datepicker ?>
			</td>
												<td align="center">sampai</td>
												<td><input type="text" placeholder="YYYY/MM/DD" data-date-format="yyyy-mm-dd" data-date-viewmode="years" id="tanggal2" name="date2" class="form-control date-picker input-sm tanggal_lahir" value="<?php echo $date2; ?>" autocomplete="off"><?php //Datepicker ?></td>
											</tr>
											<tr>
												<td colspan="4"><label><input name="tanggal" type="radio" value="2" <?php echo $checkedT2; ?>> All</label></td>
											</tr>
											<tr>
												<td><label>Produk</label></td>
												<td colspan="3"><select class="form-control input-sm jenis_kelamin" id="jenis_kelamin_add" name="jenis_kelamin_add" autocomplete="off">
														<option value="">Pilih</option>
													</select></td>
											</tr>
											<tr>
												<td><label>Agen</label></td>
												<td colspan="3"><select class="form-control input-sm jenis_kelamin" id="jenis_kelamin_add" name="jenis_kelamin_add" autocomplete="off">
														<option value="">Pilih</option>
													</select></td>
											</tr>
											<tr>
												<td><label>No Reg</label></td>
												<td><input type="text" name="date2" class="form-control date-picker input-sm" value="" autocomplete="off"></td>
												<td><label>No SJ</label></td>
												<td><input type="text" name="date2" class="form-control date-picker input-sm" value="" autocomplete="off"></td>
											</tr>
											<tr>
												<td><label>Jumlah Karung</label></td>
												<td><input type="text" name="date2" class="form-control date-picker input-sm" value="" autocomplete="off"></td>
												<td><label>Harga</label></td>
												<td><input type="text" name="date2" class="form-control date-picker input-sm" value="" autocomplete="off"></td>
											</tr>
										</tbody>
									</table>
								</div>
								</form>
									<div class="table-responsive">
									 <h4 id="tabs">Surat jalan Paket</h4>
									<table class="table table-striped" id="table_custom">
										<thead>
											<tr style="background-color: #4B8DF8;font-size: 12px;">											
												<th class="center" valign="middle"><div align="center"><font color= "white">No Reg</font></div></th>							
												<th class="center" valign="middle"><div align="center"><font color= "white">Tanggal</font></div></th>							
												<th class="center" valign="middle"><div align="center"><font color= "white">Agen</font></div></th>							
												<th class="center" valign="middle"><div align="center"><font color= "white">No SJ</font></div></th>							
												<th class="center" valign="middle"><div align="center"><font color= "white">KD Produk</font></div></th>							
												<th class="center" valign="middle"><div align="center"><font color= "white">Produk</font></div></th>							
												<th class="center" valign="middle"><div align="center"><font color= "white">Karung</font></div></th>							
												<th class="center" valign="middle"><div align="center"><font color= "white">Harga</font></div></th>
											</tr>
										</thead>
										<tbody>
										</tbody>
									</table>
									<?php
								  /*$jmldata     =  $querySJcurrent->NUM;
								  $jmlhalaman  = $paging->jumlahHalaman($jmldata, $batas);
								  $linkHalaman = $paging->navHalaman('t_sj.php?id='.$agen_id.'&',$_GET['halkategori'], $jmlhalaman);
								  $awal = $posisi_data+1;
								  $akhir = $posisi_data+$batas;
								  if($akhir>$jmldata) $akhir = $jmldata;
								  echo "Showing $awal to $akhir of $jmldata entries";*/?>
								  <ul class="pagination pull-right">
								  <?php
								  	//echo $linkHalaman ; //banyak halaman yang ditampilkan jd tombol paging
								  ?> 
								  </ul>
									<a name="detail"></a>
									<h4 id="tabs">Detail Surat jalan</h4>
								  	<table class="table table-striped" id="table_custom1">
										<thead>
											<tr style="background-color: #4B8DF8;font-size: 12px;">	
												<th class="center" valign="middle"><div align="center"><font color= "white">PRODUK</font></div></th>
												<th class="center" valign="middle"><div align="center"><font color= "white">KARUNG</font></div></th>
												<th class="center" valign="middle"><div align="center"><font color= "white">KEMASAN</font></div></th>
												<th class="center" valign="middle"><div align="center"><font color= "white">HARGA</font></div></th>
												<th class="center" valign="middle"><div align="center"><font color= "white">DISKON</font></div></th>
											</tr>
										</thead>
										<tbody>
										</tbody>
									</table>
									</div>
								</div>
							</div>	
    					</div><!--playground-->
    					<br>
    					<div class="clearfix"></div>
   					</div>
    				<div class="clearfix"></div>
    			</div>
  			</div>
		</div>
	</div>
	<?php include "../_template/navbar_footer.php"; ?>
</div><!--/main-->
<!-- javascript yang dibutuhkan untuk halaman ini saja -->
<script type="text/javascript" src="/js/DataTables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/js/DataTables/media/js/DT_bootstrap.js"></script>
<!-- ----------------------------------------------------------------------------- -->
		
<!-- ajax untuk insert data menggunakan _modal-and-datatable.js -->
<script  src="/js/ajax/_modal-and-datatable.js"></script>
<!--script>
var submit = <?php //echo "'"."button#".$submit."'" ;?>;
var data_form = <?php //echo "'"."#".$data_form."'" ;?>;
var act = 'ADD_ASURANSI';
var peringatan = 'Penyimpanan gagal, nama kode / jenis sudah ada';
</script-->
	
<!-- ----------------------------------------------------------------------------- -->
		
<script>
	
	$(function() {
	
	$( "#tanggal1" ).datepicker({changeMonth: true, changeYear: true, yearRange: "-60:+10", dateFormat:"yy-mm-dd"});
	$( "#tanggal2" ).datepicker({changeMonth: true, changeYear: true, yearRange: "-60:+10", dateFormat:"yy-mm-dd"});
	
	$('#table_custom').dataTable({"bSort": false,"bFilter": false,"bInfo": false,"bLengthChange": false,"bPaginate": false,
});
	$('#table_custom1').dataTable({"bSort": false,"bFilter": false,"bInfo": false,"bLengthChange": false,"bPaginate": false,
});
	//untuk menampilkan div view_data saat awal load
	//$('#view_data').show();
	act = '';
	action = '';
	peringatan = '';
	 //untuk menampilkan div add_data
	/*$(".tambah_data").click(function(){
	$('#view_data').hide();
	$('.select_aktif').hide();
	$('#add_data').show();
	$('#data_bank')[0].reset();
	act = 'BANK';
	action = 'ADD';

	peringatan = 'Update gagal, nama sudah ada';
	$('.notification_nama_bank').hide();
	$('.notification_keterangan').hide();		
	});
	
	$('#data_table').on('click','.edit_data',function (){
	$('#view_data').hide();
	$('#add_data').show();
	$('.select_aktif').show();
	act = 'BANK';
	action = 'UPDATE';
	peringatan = 'Update gagal, nama sudah ada';
	$('.notification_nama_bank').hide();
	$('.notification_keterangan').hide();	 
	});
	*/
	$('#data_table').on('click','.hapus_data',function (){
		var del_id= $(this).attr('id');
		act = 'BANK';
		action = 'DELETE';
		if (confirm('Anda yakin ?')) {
		   	jQuery.ajax({
			
    		   	type: "POST",
			url: "_ADM/crud_master.php?act="+act+"&action="+action,
			 data:{'id':del_id},
        		success: function(msg){
 	          		 location.reload();
 		        },
			error: function(){
				alert("failure");
				
				}
      			});
		} else {
		return false;
		}			
	});
	
	 $(".lihat_data").click(function(){
		   $('#view_data').show();
		   $('#add_data').hide();
		    $(data_form)[0].reset();
	});	

	/** Ajax untuk ADD, DELETE DAN UPDATE DATA **/
	
	$('#submit_bank').click(function(){
			
			//Validasi field			
			if($("#nama_bank").val()=="") {
						//alert('Field Asuransi tidak boleh kosong!');
						$('.notification_nama_bank').show();
						$('.notification_nama_bank').html('Field Nama bank tidak boleh kosong');
						$("#nama_bank").focus();
						return false;
					}
			else{
			$('.notification_nama_bank').hide();
			}	
			//
			
			//Validasi field
			if($("#keterangan").val()=="") {
						//alert('Field keterangan tidak boleh kosong!');
						$('.notification_keterangan').show();
						$('.notification_keterangan').html('Field Keterangan tidak boleh kosong');
						$("#keterangan").focus();
						return false;
					}
			else{
			$('.notification_keterangan').hide();
			}	
			//
			
			if (confirm('Anda yakin ?')) {
		   	$.ajax({
    		   	type: "POST",
			url: "_ADM/crud_master.php?act="+act+"&action="+action,
			data: $('#data_bank').serialize(),
        		success: function(msg){
 		        	if(msg==1){
					alert(peringatan);
					$('.notification_nama_bank').show();
					$('.notification_nama_bank').html('Update gagal, nama sudah ada');
					}else{
					window.location.replace(msg);
					
					}
					
 		        },
			error: function(){
				alert("koneksi bermasalah, silahkan reload halaman");
				
				}
      			});
    
			} else {
		return false;
		}
	
		});
});
</script>
</body>
</html>
<?php
}
else{
header('location:/lock.php?logingagal');
}
?>