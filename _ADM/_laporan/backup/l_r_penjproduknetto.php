<?php
//ada export to excel
session_start();

if(!empty($_SESSION['username'])){ 

$date1 = date("Y-m-d");
$date2 = date("Y-m-d");
$tanggal = '';
$satuan = '';
$checkedT1 = 'checked';
$checkedT2 = '';
$checkedS1 = 'checked';
$checkedS2 = '';
IF(isset($_REQUEST['tanggal'])){
	$tanggal = $_REQUEST['tanggal'];
	if($tanggal == '2'){$checkedT2 = 'checked';}
	else{$checkedT1 = 'checked';}
}
?>
<?php 
include_once '_conn/query.php';
/*$table_jenisp = new query('FINANCE','JENIS_POTONGAN'); //('NAMA DATABASE','NAMA TABEL')
//FORMAT SEPERTI PADA CLASS QUERY, FUNCTION SELECT ('FIELD','WHERE CLAUSE')
$queryjenisp = $table_jenisp->selectBy("KDPOTONGAN, JENIS_POTONGAN","1=1 ORDER BY JENIS_POTONGAN ASC");
//$table_karyawan= new query('HRD','KARYAWAN'); //('NAMA DATABASE','NAMA TABEL')
*/
?>
<!DOCTYPE html>
<html lang="en">
	<?php include '../_template/head.php';?>
	<body id="page-top">
<?php include '../_template/navbar_head.php';?>
<?php include '../_template/navbar_sub.php';?>

<!-- CSS YANG DIBUTUHKAN DI PAGE INI SAJA -->
<link rel="stylesheet" href="/js/DataTables/media/css/DT_bootstrap.css" />
<link rel="stylesheet" href="/css/jquery-ui/jquery-ui.css"> <?php //Datepicker ?>
<script src="/js/jquery/jquery.min.js"></script>
		<script src="/js/jquery-ui/jquery-ui.js"></script>

<!------------------------------------------------------------------------->
<!--main-->
<div class="container" id="main">
	<div class="row">   
	<!-- BREADCRUMB UNTUK MEMPERLIHATKAN SEDANG BERADA DI MENU APA -->
			<ol class="breadcrumb">
			<li><a href="#">Home</a></li>
			<li><a href="#">Laporan</a></li>
			<li class="active">Rekap Penjualan Produk Netto</li>
			</ol>
	<!------------------------------------------------------------------------->
     <div class="col-md-12 col-sm-12"> <!-- lg = large, md = medium, sm = small untuk melihat width nya, ada pada bootstrap.css -->
    	<div class="panelblue">
			<div id="view_data">
           		<div class="panel-heading" style="margin-bottom: 0px;"> <h3>Rekap Penjualan Produk Netto
					<!--<a href="#" class="tambah_data" style="font-size:15px;"><b style="background-color: #eee;padding: 5px 5px;border-radius: 14px;">+ Tambah data </b></a>
					<a href="#tambah_jpot" style="font-size:15px;" data-toggle="modal"><strong>+ Tambah data</strong></a>--></h3></div>
   					<div class="panel-body" style="margin-bottom: 0px;">
						<div class="panel panel-default" style="margin-bottom: 0px;">
								<div class="panel-body">
								<form method="GET" action="l_r_penjproduknetto.php" >
								<!--<div class="input-group"style="padding-bottom:5px;border-bottom:1px solid #dadada;">-->
								<div style="padding-bottom:5px;border-bottom:1px solid #dadada;">
									<table class="table table-condensed table-hover borderless tableedit" id="biodata">
										<tbody>
											<tr>
												<td class="col-md-4" colspan="4"><strong>Search:</strong></td>
											</tr>
											<tr>
												<td><label><input name="tanggal" type="radio" value="1" <?php echo $checkedT1; ?>> Tanggal</label></td>
												<td><input type="text" placeholder="YYYY/MM/DD" data-date-format="yyyy-mm-dd" data-date-viewmode="years" id="tanggal1" name="date1" class="form-control date-picker input-sm tanggal_lahir" value="<?php echo $date1; ?>" autocomplete="off"><?php //Datepicker ?>
			</td>
												<td align="center">sampai</td>
												<td><input type="text" placeholder="YYYY/MM/DD" data-date-format="yyyy-mm-dd" data-date-viewmode="years" id="tanggal2" name="date2" class="form-control date-picker input-sm tanggal_lahir" value="<?php echo $date2; ?>" autocomplete="off"><?php //Datepicker ?></td>
											</tr>
											<tr>
												<td colspan="4"><label><input name="tanggal" type="radio" value="2" <?php echo $checkedT2; ?>> All</label></td>
											</tr>
											<tr>
												<td colspan="4"><button class="btn btn-default btn-primary" type="submit"><i class="glyphicon glyphicon-search"></i> SEARCH</button> </td>
											</tr>
										</tbody>
									</table>
								</div>
								</form>
									<div class="table-responsive">
									<table class="table table-striped" id="table_custom">
										<thead>
											<tr style="background-color: #4B8DF8;font-size: 12px;">											
												<th class="center" valign="middle"><div align="center"><font color= "white">Kelompok</font></div></th>								
												<th class="center" valign="middle"><div align="center"><font color= "white">Jenis barang</font></div></th>								
												<th class="center" valign="middle"><div align="center"><font color= "white">Total Karung</font></div></th>						
												<th class="center" valign="middle"><div align="center"><font color= "white">Total lusin</font></div></th>	
											</tr>
										</thead>
										<tbody>
										</tbody>
									</table>
									</div>
								</div>
							</div>	
    					</div><!--playground-->
    					<br>
    					<div class="clearfix"></div>
   					</div>
					<div id="add_data" style="display:none;">
           				<div class="panel-heading"> <h3>Data Jenis Potongan <a href="#" class="lihat_data" style="font-size:15px;"><b style="background-color: #eee;padding: 5px 5px;border-radius: 14px;">&nbsp; Lihat data </b></a></h3></div>
                      	<div class="panel-body" style="margin-bottom: 0px;">
							<div class="col-sm-5">
								<form action="#" role="form" id="data_bank" >
									<input type="hidden" class="form-control" id="id" name="id" autocomplete="off">
									
									<div class="form-group">
										<label>Kode Jenis Potongan</label>
										<input type="text" class="form-control" id="nama_bank" name="nama_bank" placeholder="Nama bank" autocomplete="off">
										<font color = "red"><div class="notification_nama_bank" style="display:none;"></div></font>
									</div>										
									<div class="form-group">
										<label>Jenis Potongan</label>
									  	<input type="text" class="form-control" id="keterangan" name="keterangan" placeholder="Keterangan" autocomplete="off" required>
										<font color = "red"><div class="notification_keterangan" style="display:none;"></div></font>
									</div>
									<div class="form-group select_aktif" style="display:none;">
										<label>Status</label>
										<div class="form-group">
											<div class="row">
												<div class="col-sm-3">		
													<select class="form-control " id="aktif" name="aktif" >
													<option value="Y">AKTIF</option>
													<option value="T">TIDAK AKTIF</option>
													</select>
												</div>
											</div>
										</div>
									</div>
									<div class="form-group">
										<div class="col-sm-12">
											<label class="col-sm-3">&nbsp;</label>
											<div class="row">
												<div class="pull-right">		
									  				<button class="btn btn-primary btn-md" type="button" id="submit_bank">Simpan</button>
													<button class="btn btn-danger btn-md lihat_data" type="button">Kembali</button>
									  			</div>
									 		</div>
									 	</div>
									</div>
								</form>
							</div>
    					</div><!--playground-->
  					<br>
    				<div class="clearfix"></div>
    			</div>
  			</div>
		</div>
	</div>
	<?php include "../_template/navbar_footer.php"; ?>
</div><!--/main-->
<!-- javascript yang dibutuhkan untuk halaman ini saja -->
<script type="text/javascript" src="/js/DataTables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/js/DataTables/media/js/DT_bootstrap.js"></script>
<!-- ----------------------------------------------------------------------------- -->
		
<!-- ajax untuk insert data menggunakan _modal-and-datatable.js -->
<script  src="/js/ajax/_modal-and-datatable.js"></script>
<!--script>
var submit = <?php //echo "'"."button#".$submit."'" ;?>;
var data_form = <?php //echo "'"."#".$data_form."'" ;?>;
var act = 'ADD_ASURANSI';
var peringatan = 'Penyimpanan gagal, nama kode / jenis sudah ada';
</script-->
	
<!-- ----------------------------------------------------------------------------- -->
		
<script>
	
	$(function() {
	
	$( "#tanggal1" ).datepicker({changeMonth: true, changeYear: true, yearRange: "-60:+10", dateFormat:"yy-mm-dd"});
	$( "#tanggal2" ).datepicker({changeMonth: true, changeYear: true, yearRange: "-60:+10", dateFormat:"yy-mm-dd"});
	
	$('#table_custom').dataTable({"bSort": false,"bFilter": false,"bInfo": false,"bLengthChange": false,"bPaginate": false,
});
	//untuk menampilkan div view_data saat awal load
	//$('#view_data').show();
	act = '';
	action = '';
	peringatan = '';
	 //untuk menampilkan div add_data
	/*$(".tambah_data").click(function(){
	$('#view_data').hide();
	$('.select_aktif').hide();
	$('#add_data').show();
	$('#data_bank')[0].reset();
	act = 'BANK';
	action = 'ADD';

	peringatan = 'Update gagal, nama sudah ada';
	$('.notification_nama_bank').hide();
	$('.notification_keterangan').hide();		
	});
	
	$('#data_table').on('click','.edit_data',function (){
	$('#view_data').hide();
	$('#add_data').show();
	$('.select_aktif').show();
	act = 'BANK';
	action = 'UPDATE';
	peringatan = 'Update gagal, nama sudah ada';
	$('.notification_nama_bank').hide();
	$('.notification_keterangan').hide();	 
	});
	*/
	$('#data_table').on('click','.hapus_data',function (){
		var del_id= $(this).attr('id');
		act = 'BANK';
		action = 'DELETE';
		if (confirm('Anda yakin ?')) {
		   	jQuery.ajax({
			
    		   	type: "POST",
			url: "_ADM/crud_master.php?act="+act+"&action="+action,
			 data:{'id':del_id},
        		success: function(msg){
 	          		 location.reload();
 		        },
			error: function(){
				alert("failure");
				
				}
      			});
		} else {
		return false;
		}			
	});
	
	 $(".lihat_data").click(function(){
		   $('#view_data').show();
		   $('#add_data').hide();
		    $(data_form)[0].reset();
	});	

	/** Ajax untuk ADD, DELETE DAN UPDATE DATA **/
	
	$('#submit_bank').click(function(){
			
			//Validasi field			
			if($("#nama_bank").val()=="") {
						//alert('Field Asuransi tidak boleh kosong!');
						$('.notification_nama_bank').show();
						$('.notification_nama_bank').html('Field Nama bank tidak boleh kosong');
						$("#nama_bank").focus();
						return false;
					}
			else{
			$('.notification_nama_bank').hide();
			}	
			//
			
			//Validasi field
			if($("#keterangan").val()=="") {
						//alert('Field keterangan tidak boleh kosong!');
						$('.notification_keterangan').show();
						$('.notification_keterangan').html('Field Keterangan tidak boleh kosong');
						$("#keterangan").focus();
						return false;
					}
			else{
			$('.notification_keterangan').hide();
			}	
			//
			
			if (confirm('Anda yakin ?')) {
		   	$.ajax({
    		   	type: "POST",
			url: "_ADM/crud_master.php?act="+act+"&action="+action,
			data: $('#data_bank').serialize(),
        		success: function(msg){
 		        	if(msg==1){
					alert(peringatan);
					$('.notification_nama_bank').show();
					$('.notification_nama_bank').html('Update gagal, nama sudah ada');
					}else{
					window.location.replace(msg);
					
					}
					
 		        },
			error: function(){
				alert("koneksi bermasalah, silahkan reload halaman");
				
				}
      			});
    
			} else {
		return false;
		}
	
		});
});
</script>
</body>
</html>
<?php
}
else{
header('location:/lock.php?logingagal');
}
?>