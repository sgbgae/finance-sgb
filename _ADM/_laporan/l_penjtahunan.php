<?php
session_start();

if(!empty($_SESSION['username'])){

?>
<?php 
include_once '_conn/query.php';
include_once 'modul/class_paging.php';

?>
<!DOCTYPE html>
<html lang="en">
	<?php include '../_template/head.php';?>
	<body id="page-top">
<?php include '../_template/navbar_head.php';?>
<?php //include '../_template/navbar_sub.php';?>

<?PHP
/*
//Query
 select
      concat(MASTERGUDANG.GRUP.NAMA_GRUP
			, ' ' 
			, MASTERGUDANG.MASTER_BARANG.NAMA_BARANG) as BARANG
      , sum(FINANCE.DETAIL_SJ.KARUNG * MASTERGUDANG.SATUAN.ISI) AS ISI
      , month(FINANCE.SJ.TANGGAL) AS BULAN
      , year(FINANCE.SJ.TANGGAL) AS TAHUN
			, GBJ.MERK.MERK AS MERK
    from
     FINANCE.SJ
		, FINANCE.DETAIL_SJ
		, MASTERGUDANG.MASTER_BARANG
		, MASTERGUDANG.JENIS
    , MASTERGUDANG.GRUP
		,  MASTERGUDANG.SATUAN    
	  , GBJ.PRODUK_VARIAN
	  , GBJ.PRODUK_ARTIKEL
	  , GBJ.MERK
			
    where 1=1
      and FINANCE.DETAIL_SJ.SJ_ID = FINANCE.SJ.SJ_ID 
      and MASTERGUDANG.MASTER_BARANG.MASTERBARANG_ID = FINANCE.DETAIL_SJ.PRODUK_ID
      and MASTERGUDANG.MASTER_BARANG.JENIS_ID = MASTERGUDANG.JENIS.JENIS_ID
      and MASTERGUDANG.JENIS.GRUP_ID = MASTERGUDANG.GRUP.GRUP_ID
			and MASTERGUDANG.MASTER_BARANG.SATUAN_ID = MASTERGUDANG.SATUAN.SATUAN_ID
			AND GBJ.PRODUK_VARIAN.MASTERBARANG_ID = MASTERGUDANG.MASTER_BARANG.MASTERBARANG_ID
			AND GBJ.PRODUK_ARTIKEL.PRODUKARTIKEL_ID  = GBJ.PRODUK_VARIAN.PRODUKARTIKEL_ID
			AND GBJ.PRODUK_ARTIKEL.MERK_ID  = GBJ.MERK.MERK_ID 		
    group by TAHUN,BULAN,BARANG,MERK order by TAHUN,BULAN,BARANG,MERK 
*/
$query1 = "select MASTERGUDANG.MASTER_BARANG.NAMA_BARANG as BARANG ,  FORMAT(sum(FINANCE.DETAIL_SJ.KARUNG *GBJ.PRODUK_VARIAN.ISI/ MASTERGUDANG.SATUAN.ISI),0) AS ISI , month(FINANCE.SJ.TANGGAL) AS BULAN , year(FINANCE.SJ.TANGGAL) AS TAHUN, GBJ.MERK.MERK AS MERK from FINANCE.SJ , FINANCE.DETAIL_SJ , MASTERGUDANG.MASTER_BARANG , MASTERGUDANG.JENIS, MASTERGUDANG.GRUP ,  MASTERGUDANG.SATUAN , GBJ.PRODUK_VARIAN , GBJ.PRODUK_ARTIKEL , GBJ.MERK where 1=1 and FINANCE.DETAIL_SJ.SJ_ID = FINANCE.SJ.SJ_ID  and MASTERGUDANG.MASTER_BARANG.MASTERBARANG_ID = FINANCE.DETAIL_SJ.PRODUK_ID and MASTERGUDANG.MASTER_BARANG.JENIS_ID = MASTERGUDANG.JENIS.JENIS_ID and MASTERGUDANG.JENIS.GRUP_ID = MASTERGUDANG.GRUP.GRUP_ID and MASTERGUDANG.MASTER_BARANG.SATUANHITUNG_ID = MASTERGUDANG.SATUAN.SATUAN_ID AND GBJ.PRODUK_VARIAN.MASTERBARANG_ID = MASTERGUDANG.MASTER_BARANG.MASTERBARANG_ID AND GBJ.PRODUK_ARTIKEL.PRODUKARTIKEL_ID  = GBJ.PRODUK_VARIAN.PRODUKARTIKEL_ID AND GBJ.PRODUK_ARTIKEL.MERK_ID  = GBJ.MERK.MERK_ID ";

$query2 = " AND year(FINANCE.SJ.TANGGAL) = '".date("Y")."' ";

$query3 = " group by TAHUN,BULAN,BARANG,MERK  union all select 'zTotal Akhir' as BARANG ,  FORMAT(sum(FINANCE.DETAIL_SJ.KARUNG *GBJ.PRODUK_VARIAN.ISI/ MASTERGUDANG.SATUAN.ISI),0) AS ISI , month(FINANCE.SJ.TANGGAL) as  BULAN , '' as  TAHUN, '' AS MERK from FINANCE.SJ , FINANCE.DETAIL_SJ , MASTERGUDANG.MASTER_BARANG , MASTERGUDANG.JENIS, MASTERGUDANG.GRUP ,  MASTERGUDANG.SATUAN , GBJ.PRODUK_VARIAN , GBJ.PRODUK_ARTIKEL , GBJ.MERK where 1=1 and FINANCE.DETAIL_SJ.SJ_ID = FINANCE.SJ.SJ_ID  and MASTERGUDANG.MASTER_BARANG.MASTERBARANG_ID = FINANCE.DETAIL_SJ.PRODUK_ID and MASTERGUDANG.MASTER_BARANG.JENIS_ID = MASTERGUDANG.JENIS.JENIS_ID and MASTERGUDANG.JENIS.GRUP_ID = MASTERGUDANG.GRUP.GRUP_ID and MASTERGUDANG.MASTER_BARANG.SATUANHITUNG_ID = MASTERGUDANG.SATUAN.SATUAN_ID AND GBJ.PRODUK_VARIAN.MASTERBARANG_ID = MASTERGUDANG.MASTER_BARANG.MASTERBARANG_ID AND GBJ.PRODUK_ARTIKEL.PRODUKARTIKEL_ID  = GBJ.PRODUK_VARIAN.PRODUKARTIKEL_ID AND GBJ.PRODUK_ARTIKEL.MERK_ID  = GBJ.MERK.MERK_ID  ";

$query3b = ' group by BULAN order by BARANG ';

$setrow = ',rows: ["BARANG"]';
$setcols = ',cols: ["BULAN"]';
$SETJUDUL = "<p style=\"size:16px;\"><b>Laporan Penjualan Tahunan </b></p>";
$SETJUDUL2 = '<p><a href="http://finance.saligadingbersama.com/l_penjtahunan.php" title="Kembali"  >Kembali ke halaman sebelum nya</a></p>';
?>

<!-- CSS YANG DIBUTUHKAN DI PAGE INI SAJA -->
<link rel="stylesheet" href="/js/DataTables/media/css/DT_bootstrap.css" />
<link rel="stylesheet" href="/css/jquery-ui/jquery-ui.css">
<script src="/js/jquery/jquery.min.js"></script>
		<script src="/js/jquery-ui/jquery-ui.js"></script>
		

<!------------------------------------------------------------------------->


<!--main-->

<div class="navbar">
	
	<div class="row">   
	<br><br>
	<!-- BREADCRUMB UNTUK MEMPERLIHATKAN SEDANG BERADA DI MENU APA -->
			<!--<ol class="breadcrumb">
			<li><a href="/index_ADM.php">Home</a></li>
			<li class="">Transaksi</li>
			<li class="active">View Penerimaan</li>
			</ol>-->
	<!------------------------------------------------------------------------->
	
	
     <div class="col-md-12 col-sm-12"> <!-- lg = large, md = medium, sm = small untuk melihat width nya, ada pada bootstrap.css -->

    	<div class="panelblue">
			<span style="float:left;"><a href="/index_ADM.php">Home</a>
			> Laporan
			> <b>Penjualan Tahunan </b></span>
		
		
		<!--<div id="view_data" style="display:none;">
           <div class="panel-heading" style="margin-bottom: 0px;"> -->
		   <font size="4"><br><b>Laporan Penjualan Tahunan <!--
		    <a href="#tambah_data" class="tambah_data" style="font-size:15px;" data-toggle="modal"
			onClick="document.getElementById('judul').innerHTML='Tambah Penerimaan';"
			>
			-->
		
			
		    </b></font>
		    <!--</div>
		  
   			<div class="panel-body" style="margin-bottom: 0px;">
                     
					  
						<div class="panel panel-default" style="margin-bottom: 0px;">
								
								<div class="panel-body">-->
									<!--form method="GET" action="http://test.rpt-sgb.appspot.com/gchartcrud"-->
									<FORM METHOD=post ACTION=http://test.rpt-sgb.appspot.com/gchartcrud enctype="multipart/form-data" id="<?php echo $data_form =  'data_upload';?>" >
			
								<div style="padding-bottom:5px;border-bottom:1px solid #dadada;">
								<!--<h4><b>Master Penerimaan</b></h4>-->
								<?php   //echo $search;?>
								</div>
								<!--<div class="input-group"style="padding-bottom:5px;border-bottom:1px solid #dadada;">-->
								
								<div class="input-group" style="padding-bottom:5px;border-bottom:1px solid #dadada;">
									<input type="text" class="form-control hint" style="z-index:1;" placeholder="Pencarian .......... " name="search" id="search" data-container="body" data-placement="bottom" data-content="Untuk pencarian data Pelanggan berdasarkan Nama" autocomplete="off">
								
								
								
								
									
									<div class="input-group-btn" >
										<!--<button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>-->
										<button class="btn btn-default" type="submit"  name="kirim"/><i class="glyphicon glyphicon-search"></i> </button>
										<a href="#" class="btn btn-default" id="up" title="Menampilkan Pencarian" style="display:none"><i class="fa fa-caret-up"></i></a>
										<a href="#" class="btn btn-default" id="down" title="Menampilkan Pencarian" ><i class="fa fa-caret-down"></i></a>

									</div>
								</div>
								<center>
								
								
								</center><br>
								<div id="searchoption" style="padding-bottom:5px;border-bottom:1px solid #dadada;">
									<table class="table table-condensed table-hover borderless tableedit" id="biodata" >
										<tbody>
											<tr>
												<td width = "15%">Tahun</td>
												<td width = "85%">
												<?php $tahun = '';
												for($i=-5;$i<2;$i++){
													$now = date("Y");
													$date =date("Y",strtotime($i.' years'));
													$selected = '';
													if($now == $date){ $selected = ' selected="selected" ';}
													$tahun .= '<option '.$selected.'>'.$date.'</option>';
												}
												?>
												
												<select id="tahun" name="tahun">
													<?php echo $tahun;?>
												</select>
												<textarea name="SQLSELECT" id="SQLSELECT" cols="20" rows="10"  style="display:none"><?PHP echo $query1.$query2.$query3.$query2.$query3b; ?></textarea>
													<textarea name="SETROW" cols="20" rows="10"  style="display:none"><?PHP echo $setrow; ?></textarea>
													<textarea name="SETCOLS" cols="20" rows="10"  style="display:none"><?PHP echo $setcols; ?></textarea>
													<textarea name="SETJUDUL" id="SETJUDUL" cols="20" rows="10" style="display:none"><?PHP echo $SETJUDUL.$SETJUDUL2; ?></textarea>
			</td>	
											</tr>
											<tr>
												<td colspan="2" align="left">
													<button class="btn btn-default" type="submit"  name="kirim"/><i class="glyphicon glyphicon-search"></i> Filter</button></td>
											</tr>
										</tbody>
									</table>
									
									<div class="input-group-btn">
								
									</div>
								</div>
								
						
								</form>
						
									
									<br><br><br>
									
									<br><br>
								<center>
									
									 
									  <?php
						
									 ?>
								</center>
								
	   </div>
						
</div>
</div>

<?php include "../_template/navbar_footer.php"; ?>
</div><!--/main-->



	<!-- javascript yang dibutuhkan untuk halaman ini saja -->
		
		
		
		<script type="text/javascript" src="/js/DataTables/media/js/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="/js/DataTables/media/js/DT_bootstrap.js"></script>
		
		
		
		<!-- ----------------------------------------------------------------------------- -->
		
		
		<!-- ajax untuk insert data menggunakan _modal-and-datatable.js -->
		
		
		
		<script  src="/js/ajax/_modal-and-datatable.js"></script>
		<!--script>
		var submit = <?php //echo "'"."button#".$submit."'" ;?>;
		var data_form = <?php //echo "'"."#".$data_form."'" ;?>;
		var act = 'ADD_ASURANSI';
		var peringatan = 'Penyimpanan gagal, nama kode / jenis sudah ada';
		</script-->
		
		<!-- ----------------------------------------------------------------------------- -->
		

		
		
		<script>
	window.test = function(e){
		
        //alert(e.value);
		//IF(e.value != ''){
		if(sj.value == 'nf'){
			window.location.href = 'pdffinnonfaktur?mulai='+tanggal1.value+'&selesai='+tanggal2.value;
		} else {
			window.location.href = 'pdffinfaktur?mulai='+tanggal1.value+'&selesai='+tanggal2.value;
		}
		//}
}
	$(function() {
	$('#table_custom').dataTable({"bSort": false,"bFilter": false,"bInfo": false,"bLengthChange": false,"bPaginate": false,
});

$( "#tanggal1" ).datepicker({changeMonth: true, changeYear: true, yearRange: "-60:+10", dateFormat:"yy-mm-dd"});
$( "#tanggal2" ).datepicker({changeMonth: true, changeYear: true, yearRange: "-60:+10", dateFormat:"yy-mm-dd"});

var htmlobjek;
	$("#down").click(function(){
		$('#searchoption').show();
		$('#up').show();
		$('#down').hide();
	}); 
	$("#up").click(function(){
		$('#searchoption').hide();
		$('#up').hide();
		$('#down').show();
	}); 
	
	var htmlobjek;
			
	$('#tahun').change(function() {
		var query1 = "<?php echo $query1; ?>";
		var query2 = "";
		var tahun = $("#tahun").val();
		
		var judul2 = '';
		var query2 = query2+" AND year(FINANCE.SJ.TANGGAL) = '"+tahun+"' ";
		var judul2 = '<?php echo $SETJUDUL2; ?>';
		
		var query3 = "<?php echo $query3; ?>";
		var query3b = "<?php echo $query3b; ?>";
		$('#SQLSELECT').html(query1+query2+query3+query2+query3b);
		$('#SETJUDUL').html(SETJUDUL+" "+judul2);
	});
	/*	
	$('#view_data').show();
	act = '';
	action = '';
	peringatan = '';
	 //untuk menampilkan div add_data
	 
	$('#submit_as1').click(function(){
	 
	 var nik= $("#nik").val();
	 var validasi= $("#validasi").val();
	 
  	if(validasi=="") {
						//alert('Field Asuransi tidak boleh kosong!');
						$('.notification_validasi').show();
						$('.notification_validasi').html('Isi NIK Terlebih Dahulu');
						$("#validasi").focus();
						return false;
					}
			else{
			$('.notification_validasi').hide();
			}
			
	if(validasi != nik) {
						//alert('Field Asuransi tidak boleh kosong!');
						$('.notification_validasi').show();
						$('.notification_validasi').html('NIK tidak valid');
						$("#validasi").focus();
						return false;
					}
			else{
			$('.notification_validasi').hide();
			}		
			
	 
			if (confirm('Anda yakin ?')) {
		   	$.ajax({
    		   	type: "POST",
			url: "_ADM/crud_transaksi.php?act=PENERIMAAN&action=VALID",
			data: $('#data_as').serialize(),
        		success: function(msg){
 		        	if(msg==1){
					alert("Status Gagal Disimpan");
					}else{
					window.location.replace("t_viewpenerimaan.php?menu=transaksi");
					}},
			error: function(){
				alert("koneksi bermasalah, silahkan reload halaman");
				}
      			});
			} else {
		return false;
		}
		});

		
		$('#submit_as2').click(function(){
		
		var nik= $("#nik").val();
	 var validasi= $("#validasi").val();
	 
  	if(validasi=="") {
						//alert('Field Asuransi tidak boleh kosong!');
						$('.notification_validasi').show();
						$('.notification_validasi').html('Isi NIK Terlebih Dahulu');
						$("#validasi").focus();
						return false;
					}
			else{
			$('.notification_validasi').hide();
			}
			
	if(validasi != nik) {
						//alert('Field Asuransi tidak boleh kosong!');
						$('.notification_validasi').show();
						$('.notification_validasi').html('NIK tidak valid');
						$("#validasi").focus();
						return false;
					}
			else{
			$('.notification_validasi').hide();
			}		
		
		
			if (confirm('Anda yakin ?')) {
		   	$.ajax({
    		   	type: "POST",
			url: "_ADM/crud_transaksi.php?act=PENERIMAAN&action=DROP",
			data: $('#data_as').serialize(),
        		success: function(msg){
 		        	if(msg==1){
					alert("Status Gagal Disimpan");
					}else{
					window.location.replace("t_viewpenerimaan.php?menu=transaksi");
					}},
			error: function(){
				alert("koneksi bermasalah, silahkan reload halaman");
				}
      			});
			} else {
		return false;
		}
		});
	
	
	
	*/

	

	/** Ajax untuk ADD, DELETE DAN UPDATE DATA **/
	

		
	
	});
		</script>
		
	</body>
</html>
<?php
}
else{
header('location:/lock.php?logingagal');
}
?>