 $(document).ready(function() {
 
 
// Menjalankan plugin DataTables pada id contoh. id contoh merupakan tabel yang kita gunakan untuk menampilkan data
   $('#data_table').dataTable({"bSort": false,  
  });
// $.fn.editable.defaults = {
        /**
        Type of input. Can be <code>text|textarea|select|date|checklist</code> and more

        @property type 
        @type string
        @default 'text'
        **/
    /////   type: 'text',   
	/////	mode: 'inline',  		
        /**
        Sets disabled state of editable

        @property disabled 
        @type boolean
        @default false
        **/         
      /////  disabled: false,
        /**
        How to toggle editable. Can be <code>click|dblclick|mouseenter|manual</code>.   
        When set to <code>manual</code> you should manually call <code>show/hide</code> methods of editable.    
        **Note**: if you call <code>show</code> or <code>toggle</code> inside **click** handler of some DOM element, 
        you need to apply <code>e.stopPropagation()</code> because containers are being closed on any click on document.
        
        @example
        $('#edit-button').click(function(e) {
            e.stopPropagation();
            $('#username').editable('toggle');
        });

        @property toggle 
        @type string
        @default 'click'
        **/          
     /////   toggle: 'click',
        /**
        Text shown when element is empty.

        @property emptytext 
        @type string
        @default 'Empty'
        **/         
      /////  emptytext: '-'
       
       
/////    };	
	//untuk menghilankan sorting semua kolom
	//"bSort": false
	
	// untuk menghilankan sorting beberapa kolom
	//"aoColumnDefs": [
       // { 'bSortable': false, 'aTargets': [ -1 ] }
    //]

////////////////// untuk menghilangkan popover ketika klik body html

$('body').on('click', function (e) {
    $('.validation').each(function () {
        //the 'is' for buttons that trigger popups
        //the 'has' for icons within a button that triggers a popup
        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
            $(this).popover('destroy');
        }
    });
	$('.hint').each(function () {
        //the 'is' for buttons that trigger popups
        //the 'has' for icons within a button that triggers a popup
        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
            $(this).popover('destroy');
        }
    });
});

//////////////////////////////////////////////////////

 $('#select_jenis').change(function () {
    var val = $(this).val();

    if (val === 'LAINNYA') {
      $('#input_jenis_training').show();
	  $("#jenis_training").focus();
     

    }
	else{
	$('#input_jenis_training').hide();
	document.getElementById('jenis_training').value = "";
	document.getElementById('keterangan_training').value = "";
	}
    
  });
		
$('#select_penyelenggara').change(function () {
    var val = $(this).val();

    if (val === 'LAINNYA') {
      $('#input_penyelenggara').show();
	  $("#penyelenggara_name").focus();
     

    }
	else{
	$('#input_penyelenggara').hide();
	document.getElementById('penyelenggara_name').value = "";
	document.getElementById('keterangan_penyelenggara').value = "";
	}
    
  });
  $('#select_ikatan').change(function () {
    var val = $(this).val();

    if (val === 'Y') {
      $('#input_ikatan').show();
	  $("#ikatan_mulai").focus();
     

    }
	else{
	$('#input_ikatan').hide();
	document.getElementById('ikatan_mulai').value = "";
	document.getElementById('ikatan_selesai').value = "";
	}
    
  });
  $('#select_lokasi').change(function () {
    var val = $(this).val();

    if (val === 'LAINNYA') {
      $('#input_lokasi').show();
	  $("#ikatan_mulai").focus();
     

    }
	else{
	$('#input_lokasi').hide();
	document.getElementById('lokasi_training').value = "";
	document.getElementById('keterangan_lokasi').value = "";
	}
    
  });
  /*
 $(submit).click(function(){
				
			if($("#nama_asuransi").val()=="") {
						alert('Field Asuransi tidak boleh kosong!');
						$("#nama_asuransi").focus();
						return false;
					}
			if($("#bank_name").val()=="") {
						alert('Field Bank tidak boleh kosong!');
						$("#bank_name").focus();
						return false;
					}
			if($("#negara_name").val()=="") {
						alert('Field Negara tidak boleh kosong!');
						$("#negara_name").focus();
						return false;
					}
			if($("#select_negara").val()=="") {
						alert('Pilihan negara tidak boleh kosong!');
						$("#select_negara").focus();
						return false;
					}			
			if($("#prov_name").val()=="") {
						alert('Field Provinsi tidak boleh kosong!');
						$("#prov_name").focus();
						return false;
					}
			if($("#select_provinsi").val()=="") {
						alert('Pilihan provinsi tidak boleh kosong!');
						$("#select_provinsi").focus();
						return false;
					}			
			if($("#kota_name").val()=="") {
						alert('Field Kota tidak boleh kosong!');
						$("#kota_name").focus();
						return false;
					}
			if($("#jenis_alamat_name").val()=="") {
						alert('Field Jenis Alamat tidak boleh kosong!');
						$("#jenis_alamat_name").focus();
						return false;
					}	
			if($("#tanggal_mulai").val()=="") {
						alert('Field Tanggal mulai tidak boleh kosong!');
						$("#tanggal_mulai").focus();
						return false;
					}		
			if($("#tanggal_selesai").val()=="") {
						alert('Field Tanggal selesai tidak boleh kosong!');
						$("#tanggal_selesai").focus();
						return false;
					}	
			if($("#select_status").val()=="") {
						alert('Pilihan status pekerja tidak boleh kosong!');
						$("#select_status").focus();
						return false;
					}
			if($("#holiday_information").val()=="") {
						alert('Field Keterangan libur tidak boleh kosong!');
						$("#holiday_information").focus();
						return false;
					}
			if($("#status_mutasi_name").val()=="") {
						alert('Field Status mutasi tidak boleh kosong!');
						$("#status_mutasi_name").focus();
						return false;
					}
			if($("#kode").val()=="") {
						alert('Field Kode tidak boleh kosong!');
						$("#kode").focus();
						return false;
					}	
			if($("#jabatan_name").val()=="") {
						alert('Field Jabatan tidak boleh kosong!');
						$("#jabatan_name").focus();
						return false;
					}
			if($("#tingkat").val()=="") {
						alert('Field Tingkat tidak boleh kosong!');
						$("#tingkat").focus();
						return false;
					}
			if($("#status_ijin_name").val()=="") {
						alert('Field Status ijin tidak boleh kosong!');
						$("#status_ijin_name").focus();
						return false;
					}	
			if($("#potongan").val()=="") {
						alert('Pilihan Potongan tidak boleh kosong!');
						$("#potongan").focus();
						return false;
					}	
			if($(".nik").val()=="") {
						alert('Field nik tidak boleh kosong!');
						$("#nik").focus();
						return false;
					}		
			if($(".nama").val()=="") {
						alert('Field nama tidak boleh kosong!');
						$("#nama").focus();
						return false;
					}	
			if($(".jenis_kelamin").val()=="") {
						alert('Pilihan jenis kelamin tidak boleh kosong!');
						$("#jenis_kelamin").focus();
						return false;
					}
			if($(".tempat_lahir").val()=="") {
						alert('Pilihan tempat lahir tidak boleh kosong!');
						$("#tempat_lahir").focus();
						return false;
					}
			if($(".tanggal_lahir").val()=="") {
						alert('Field tanggal lahir tidak boleh kosong!');
						$("#tanggal_lahir").focus();
						return false;
					}
			if($(".kebangsaan").val()=="") {
						alert('Field kebangsaan tidak boleh kosong!');
						$("#kebangsaan").focus();
						return false;
					}
			if($(".no_ktp").val()=="") {
						alert('Field no ktp tidak boleh kosong, jika tidak ada beri tanda - !');
						$("#no_ktp").focus();
						return false;
					}
			if($(".status_menikah").val()=="") {
						alert('Field status menikah tidak boleh kosong!');
						$("#status_menikah").focus();
						return false;
					}
			if($(".agama").val()=="") {
						alert('Field agama tidak boleh kosong!');
						$("#agama").focus();
						return false;
					}
			if($(".pend_terakhir").val()=="") {
						alert('Field pend terakhir tidak boleh kosong!');
						$("#pend_terakhir").focus();
						return false;
					}
			if($(".nama_ayah").val()=="") {
						alert('Field nama ayah tidak boleh kosong!');
						$("#nama_ayah").focus();
						return false;
					}
			if($(".nama_ibu").val()=="") {
						alert('Field nama ibu tidak boleh kosong!');
						$("#nama_ibu").focus();
						return false;
					}
			if($(".sp_kode").val()=="") {
						alert('Field kode tidak boleh kosong!');
						$("#sp_kode").focus();
						return false;
					}
			if($(".sp_nama").val()=="") {
						alert('Field status pekerja tidak boleh kosong!');
						$("#sp_nama").focus();
						return false;
					}
			if($(".lok_nama").val()=="") {
						alert('Field lokasi kerja tidak boleh kosong!');
						$("#lok_nama").focus();
						return false;
					}		
		   	$.ajax({
    		   	type: "POST",
			url: "_conn/crud.php?act="+act,
			data: $(data_form).serialize(),
        		success: function(msg){
 		        	if(msg==1){
					alert(peringatan);
					}else{
					window.location.replace(msg);
					//location.reload();
					}
					
 		        },
			error: function(){
				alert("koneksi bermasalah, silahkan reload halaman");
				
				}
      			});
    
	
	
		});
		*/
} );