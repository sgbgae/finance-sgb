<?php
date_default_timezone_set('Asia/Bangkok');
include('conn/conn.php');
include('link/link.php');
$ID = $_REQUEST['subid'];
$periode = $_REQUEST['periode'];
//$expenseid = $_REQUEST['expenseid'];
$hal = $_REQUEST['hal'];
if(isset($_REQUEST['print']) == 'Y'){
$hariini = date('Y-m-d H:i:s');
$adoQueryPrint = "
	update SUB_EXPENSE
	SET PRINT = 'Y',
		TGL_PRINT = '$hariini'
	WHERE SUBEXPENSE_ID = '$ID'
"; 
$objQueryPrint = mysql_query($adoQueryPrint) or die("Error Query[".$adoQueryPrint."]");
echo "<script language=\"JavaScript\">{ 
	location.href=\"http://".$link_url."/index.do?menu=1&expenseid=".$expenseid."&hal=".$hal."\"; 
	}</script>"; 
}
?><script language="javascript">
function printDiv(divName) {
	if(confirm("apakah anda yakin untuk print halaman ini??\nHalaman ini hanya bisa di print 1 kali saja")){		
		var printContents = document.getElementById(divName).innerHTML;
		var originalContents = document.body.innerHTML;
		document.body.innerHTML = printContents;
		window.print();
		document.body.innerHTML = originalContents;
		window.location = 'http://<?php echo $link_url; ?>/print.do?subid=<?php echo $ID; ?>&periode=<?php echo $periode; ?>&print=Y&expenseid=<?php echo $expenseid; ?>&hal=<?php echo $hal; ?>';
		return false
	 } 
}
</script>
<?php

$strQuerySub = "
	select M.NAMA, M.NIK, D.DEPARTEMENT AS BAGIAN
		, D.TGL_INPUT, D.NILAI, D.NILAI_APPROVAL, D.NIK_APPROVAL, ME.EXPENSE
		, MU.NAMA AS NAMA_APPROVAL, D.TGL_APPROVAL
	from SUB_EXPENSE S
		INNER JOIN EXPENSE E ON E.EXPENSE_ID = S.EXPENSE_ID
		INNER JOIN HRD.KARYAWAN M ON M.NIK = E.NIK		
		inner join HRD.DEPARTEMENT D ON D.DEPARTEMENT_ID = M.DEPARTEMENT_ID	
		INNER JOIN DETAIL_EXPENSE D ON D.SUBEXPENSE_ID = S.SUBEXPENSE_ID
		INNER JOIN MASTER_EXPENSE ME ON D.MASTEREXPENSE_ID = ME.MASTEREXPENSE_ID
		INNER JOIN HRD.KARYAWAN MU ON MU.NIK = D.NIK_APPROVAL
	WHERE S.SUBEXPENSE_ID = '$ID' 
"; //ECHO $strQuerySub;
$objQuerySub = mysql_query($strQuerySub) or die("Error Query[".$strQuerySub."]");
$num_QuerySub = mysql_num_rows($objQuerySub);
$nama = mysql_result($objQuerySub,0,"NAMA");
$nik = mysql_result($objQuerySub,0,"NIK");
$departement = mysql_result($objQuerySub,0,"BAGIAN");
?>
<div id="printableArea">
<table width="100%">
<tr>
	<td width="13%">NAMA</td>
	<td width="87%"><?php echo $nama;?></td>
</tr>
<tr>
	<td>NIK</td>
	<td><?php echo $nik;?></td>
</tr>
<tr>
	<td>DEPARTEMENT</td>
	<td><?php echo $departement;?></td>
</tr>
<tr>
	<td>EXPENSE PERIODE </td>
	<td><?php echo $periode;?></td>
</tr>
</table>
<br />
<strong>DETAIL EXPENSE</strong>
<table width="75%" border="1">
<tr>
	<td width="12%"><strong>TANGGAL<BR />
    PENGAJUAN</strong></td>
	<TD width="17%"><strong>POS</strong></TD>
	<TD width="21%"><strong>NILAI<BR />
    PENGAJUAN</strong></TD>
	<TD width="21%"><strong>NILAI<BR />
    APPROVAL</strong></TD>
	<TD width="29%"><strong>MENYETUJUI</strong></TD>
</tr>
<?php for($i=0;$i<$num_QuerySub;$i++){ ?>
<tr>
	<td width="12%"><?php echo date("d M Y", strtotime(mysql_result($objQuerySub,$i,"TGL_INPUT")));?></td>
	<TD width="17%"><?php echo mysql_result($objQuerySub,$i,"EXPENSE");?></TD>
	<TD width="21%"><?php echo number_format(mysql_result($objQuerySub,$i,"NILAI"), 2, ',', '.'); ?></TD>
	<TD width="21%"><?php echo number_format(mysql_result($objQuerySub,$i,"NILAI_APPROVAL"), 2, ',', '.');?></TD>
	<TD width="29%"><?php echo mysql_result($objQuerySub,$i,"NAMA_APPROVAL")."<BR>".date("d M Y H:i:s", strtotime(mysql_result($objQuerySub,$i,"TGL_APPROVAL"))); ?></TD>
</tr>
<?php } ?>
</table>     
</div>
<br />
<form>
<input type="button" name="print" onclick="printDiv('printableArea')" value="print!" />
</form>
