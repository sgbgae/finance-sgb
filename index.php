<?php
session_start();

if(!empty($_SESSION['dept_kode'])){
?>
<?php 

include_once '_conn/query.php'; //Class query

$action = ''; //Untuk identifikasi level user yang menggunakan aplikasi ini

//Mengambil dept kode dimulai, diambil 4 digit diawal
?>
<!DOCTYPE html>
<html lang="en">
	<?php include '_ADM/_template/head.php'; //TEMPLATE HEADER DAN BERISI CSS GENERAL ?>
	<body id="page-top">
<nav class="navbar navbar-fixed-top header">
 	<div class="col-md-12">
        <div class="navbar-header">
          <a href="#" class="navbar-brand"><img src="images/logo.png" width="25" height="25">&nbsp;<strong><font color="#4B8DF8">PT. Saligading Bersama</font></strong></a>
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse1">
          <i class="glyphicon glyphicon-search"></i>
          </button>
      
        </div>
        <div class="collapse navbar-collapse" id="navbar-collapse1">
          <ul class="nav navbar-nav navbar-right">
			<li>
                <a href="#" class="dropdown-toggle hide-minimize" data-toggle="dropdown"><i class="fa fa-th" style="padding:4px 0 0 0;"></i></a>
                <ul class="dropdown-menu arrow-right">
                  <div align="center" style="width:200px;" class="list-inline">
				
				<li><a href="https://mail.google.com" target="_blank"><img src="/images/gmail.png" width="50px" style="padding:5px;" ></a><br/>Mail</li>
				<li><a href="https://drive.google.com" target="_blank"><img src="/images/google_drive.png" width="50px" style="padding:5px;" ></a><br/>Drive</li>
				<li><a href="https://calendar.google.com" target="_blank"><img src="/images/google_calendar.png" width="50px" style="padding:5px;" ></a><br/>Calendar</i>
				</div>
                </ul>
             </li>
             <li>
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Help</a>
                <ul class="dropdown-menu arrow-right">
                  <li><a href="#">Help Center</li>
                  <li><a href="#">Send Feedback</a></li>
                </ul>
             </li>
			 
			 
             <li style="display:none"><a href="#" id="btnToggle"><i class="glyphicon glyphicon-th-large"></i></a></li>
             <li><a class = "dropdown-toggle" href = "#" data-toggle = "dropdown"><?php echo $_SESSION['alamat_email'];?><span class="caret"></span></a>
			  <ul class="nav dropdown-menu arrow-right">
   
    <div style = "padding: 5px 15px 10px 15px; width: auto;word-wrap: break-word;">
	
	<h6>Apakah anda akan keluar dari aplikasi ?</h6>
		<form action="/logout.php">
         <button type="submit" class="btn btn-sm btn-danger pull-right"><i class="fa fa-sign-out"></i>&nbsp;Sign Out</button>
		 </form>

		 <hr>
      </div>
	
	  </ul>
			 </li>
           </ul>
        </div>	
     </div>	
	
</nav>

<!-- CSS YANG DIBUTUHKAN DI PAGE INI SAJA -->


<!------------------------------------------------------------------------->


<!--main-->
<div class="container" id="main">
	<div class="row">   
     <div class="col-md-8"> <!-- lg = large, md = medium, sm = small untuk melihat width nya, ada pada bootstrap.css -->

    	<div class="panel-default">
		<?php //IF($_SESSION['action'] == 'SR' OR $_SESSION['action'] =='SUPERADMIN'){?>
		<div class="col-md-5">
      	 
          <div class="well"> 
             <form class="form">
              <h4 class="text-center">TAGIHAN</h4>
              <div class="input-group text-center">
              
                <span class="input-group-btn"><button class="btn btn-lg btn-primary" onClick="tagihan()"  type="button">MASUK</button></span>
              </div>
            </form>
          </div>
	
  </div><?php //} ?>
  <?php //IF($_SESSION['action'] == 'ADMCMPN' OR $_SESSION['action'] =='SUPERADMIN'){?>
  	<div class="col-md-5">
      	 
          <div class="well"> 
             <form class="form">
              <h4 class="text-center">EXPENSE</h4>
              <div class="input-group text-center">
         
                <span class="input-group-btn"><button class=" btn btn-lg btn-primary" onClick="expense()" type="button">MASUK</button></span>
              </div>
            </form>
          </div>
	
  </div>
  <?php //} ?>
</div>
</div>

<hr>
<?php include "_ADM/_template/navbar_footer.php"; ?>
<hr>
</div><!--/main-->
<script>
function expense(){
window.location = '/index.do'; //redirect ke index EXPENSE 
}
function tagihan(){
window.location = '/index_ADM.php'; //redirect ke index TAGIHAN
}
</script>		
	</body>
</html>
<?php
}
else{
header('location:/lock.php?logingagal'); //Jika session telah habis atau tidak ada maka akan di redirect ke halaman lock untuk login lagi menggunakan google
}
?>